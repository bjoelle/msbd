/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.distributions;

import java.util.List;
import java.util.Random;

import beast.base.inference.Distribution;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.inference.State;
import beast.base.inference.parameter.RealParameter;
import msbd.base.evolution.tree.MultiRateTree;

/**
 * Abstract class for creating multi-rate tree likelihood density functions.
 */
public class MultiRateTreeDistribution extends Distribution {

	public Input<MultiRateTree> mrTreeInput = new Input<MultiRateTree>("multiRateTree", "Multi-rate tree.", Validate.REQUIRED);

	public Input<Boolean> checkValidityInput = new Input<>("checkValidity", "Explicitly check validity of colouring. (Default false.) "
			+ "Useful if operators are in danger of proposing invalid trees.", false);
	public Input<Boolean> toThePresentInput = new Input<>("toThePresent", "Whether the tree goes up to the present (Default true.)  "
			+ "Useful because extinct and extant sampling is treated differently.", true);

	public Input<RealParameter> originInput = new Input<>("origin", "height of the origin of the tree, if no value is provided the tree will be considered without root edge");

	protected MultiRateTree mrTree;
	RealParameter origin;
	protected boolean checkValidity, toThePresent, mrca = false;	
	double rho, sigma, gamma;

	@Override
	public void initAndValidate() {
		mrTree = mrTreeInput.get();
		checkValidity = checkValidityInput.get();
		toThePresent = toThePresentInput.get();
		if (originInput.get() == null) mrca = true;
		else {
			origin = originInput.get();
			if (origin.getValue() < mrTree.getRoot().getHeight()) throw new IllegalArgumentException("Origin cannot be lower than the MRCA age of the tree."); 
		}
	}

	// Interface requirements:
	@Override
	public List<String> getArguments() {
		return null;
	}

	@Override
	public List<String> getConditions() {
		return null;
	}

	@Override
	public void sample(State state, Random random) {}

	/**
	 * @return whether distribution requires recalculation
	 */
	@Override
	public boolean requiresRecalculation() {
		return true;
	}

	/**
	 * Calculate the probability density of a lineage existing at time with the given parameters to disappear from the
	 * tree, knowing the initial condition p0(0) = 1-rho.
	 *
	 * @param lambda the lambda
	 * @param mu the mu
	 * @param psi the psi
	 * @param time the time
	 * @return the density
	 */
	protected double p0(double lambda, double mu, double psi, double time) {
		return p0(lambda, mu, psi, time, 0.0, 1 - rho);
	}

	/**
	 * Calculate the probability density of a lineage existing at time with the given parameters to disappear from the
	 * tree, knowing the initial condition p0(init_t) = init_v.
	 *
	 * @param lambda the lambda
	 * @param mu the mu
	 * @param psi the psi
	 * @param time the time
	 * @param init_t the initial time
	 * @param init_v the initial value
	 * @return the density
	 */
	protected double p0(double lambda, double mu, double psi, double time, double init_t, double init_v) {
		if (init_t < 1e-10) init_v = 1 - rho;

		double c = Math.sqrt(Math.pow(gamma + mu + lambda + psi, 2) - 4 * mu * lambda * (1 - sigma));
		double x1 = -(gamma + mu + lambda + psi + c) / 2;
		double x2 = -(gamma + mu + lambda + psi - c) / 2;
		double A1 = lambda * init_v + x1;
		double A2 = lambda * init_v + x2;
		double res = (A1 * x2 - A2 * x1 * Math.exp(-c * (time - init_t))) / (lambda * (A2 * Math.exp(-c * (time - init_t)) - A1));
		return res;
	}

	/**
	 * Calculates the log likelihood of a lineage existing at ti with the given parameters to show no event until time
	 * te, knowing the initial condition p0(0) = 1-rho.
	 *
	 * @param lambda the lambda
	 * @param mu the mu
	 * @param psi the psi
	 * @param ti the start of the interval
	 * @param te the end of the interval (te < ti)
	 * @return the log likelihood
	 */
	protected double logfratio(double lambda, double mu, double psi, double ti, double te) {
		return logfratio(lambda, mu, psi, ti, te, 0.0, 1 - rho);
	}

	/**
	 * Calculates the log likelihood of a lineage existing at ti with the given parameters to show no event until time
	 * te, knowing the initial condition p0(init_t) = init_v.
	 *
	 * @param lambda the lambda
	 * @param mu the mu
	 * @param psi the psi
	 * @param ti the start of the interval
	 * @param te the end of the interval (te < ti)
	 * @param init_t the initial time
	 * @param init_v the initial value
	 * @return the log likelihood
	 */
	protected double logfratio(double lambda, double mu, double psi, double ti, double te, double init_t, double init_v) {
		if (ti - te < 1e-10) return 0.0;

		double c = Math.sqrt(Math.pow(gamma + mu + lambda + psi, 2) - 4 * mu * lambda * (1 - sigma));
		double x1 = -(gamma + mu + lambda + psi + c) / 2;
		double x2 = -(gamma + mu + lambda + psi - c) / 2;
		double A1 = lambda * init_v + x1;
		double A2 = lambda * init_v + x2;
		double res = c * (te - ti) + 2 * Math.log(A2 * Math.exp(-c * (te - init_t)) - A1) - 2 * Math.log(A2 * Math.exp(-c * (ti - init_t)) - A1);
		return res;
	}

}
