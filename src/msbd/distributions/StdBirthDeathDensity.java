/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.distributions;

import org.apache.commons.math.special.Gamma;

import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.evolution.tree.Node;
import msbd.base.evolution.tree.MultiRateNode;
import msbd.evolution.tree.StateChangeModel;

/**
 * Tree likelihood for a StateChangeModel.
 */
public class StdBirthDeathDensity extends MultiRateTreeDistribution {

	public Input<StateChangeModel> stateChangeModelInput = new Input<>("stateChangeModel", "Model of transition between states.",
			Validate.REQUIRED);

	protected StateChangeModel stateChangeModel;

	/**
	 * Instantiates a new std birth death density.
	 */
	public StdBirthDeathDensity() {}

	@Override
	public void initAndValidate() {
		super.initAndValidate();
		stateChangeModel = stateChangeModelInput.get();
	}

	/**
	 * Calculate the log likelihood.
	 *
	 * @return logP
	 */
	@Override
	public double calculateLogP() {
		if (checkValidity && !mrTree.isValid()) return Double.NEGATIVE_INFINITY;
		if (stateChangeModel.getNstar() < stateChangeModel.getNStates()) return Double.NEGATIVE_INFINITY;
		
		gamma = stateChangeModel.getGamma();
		rho = stateChangeModel.getRho();
		sigma = stateChangeModel.getSigma();

		Node rootNode = mrTree.getRoot();
		logP = 0.0;

		if (!mrca) {
			double t0 = rootNode.getHeight();
			if (origin.getValue() < t0) return Double.NEGATIVE_INFINITY;
			int rootstate = ((MultiRateNode) rootNode).getNodeState();
			logP += logfratio(stateChangeModel.getLambda(rootstate), stateChangeModel.getMu(rootstate),
					stateChangeModel.getPsi(rootstate), origin.getValue(), t0); 
			if(!mrTree.getRoot().isFake()) logP += Math.log(stateChangeModel.getLambda(rootstate));
		}

		for (Node child : rootNode.getChildren()) {
			logP += logPNode(child);
		}

		// correction for unseen states : each unseen state m+1 added adds a (nstar - m) factor
		// so full correction is (nstar - 1)! / (nstar - n)!
		// NB: Gamma(x) = (x - 1)!
		logP += Gamma.logGamma(stateChangeModel.getNstar() - 1 + 1)
				- Gamma.logGamma(stateChangeModel.getNstar() - stateChangeModel.getNStates() + 1);
		
		// correction for oriented trees - this ensures that flipped configurations (i.e. where right and left subtree are reversed)
		// are treated as identical for the purposes of the likelihood
		// only matters for SA trees since it's constant otherwise
		int internalNodeCount = mrTree.getLeafNodeCount() - mrTree.getDirectAncestorNodeCount() - 1;
		logP += internalNodeCount*Math.log(2);

		return logP;
	}

	/**
	 * Calculate logP of the subtree defined by node n.
	 *
	 * @param n the node
	 * @return the logP of the subtree
	 */
	double logPNode(Node n) {
		MultiRateNode node = (MultiRateNode) n;
		double lambda, mu, psi, ti, te;
		int dwnState, state;
		double logPNode = 0;

		for (int i = node.getChangeCount() - 1; i > -2; i--) {
			if (i >= 0) te = node.getChangeTime(i);
			else te = node.getHeight();

			if (i >= 0) state = node.getChangeState(i);
			else state = node.getNodeState();

			if (i > 0) dwnState = node.getChangeState(i - 1);
			else dwnState = node.getNodeState();

			if (node.getChangeCount() > i + 1) ti = node.getChangeTime(i + 1);
			else ti = node.getParent().getHeight();
			
			if(te > ti) { 
				// this can happen due to non-MSBD operators which move nodes
				// without considering the attached shifts (e.g. tip dates sampler)
				return Double.NEGATIVE_INFINITY;
			}

			lambda = stateChangeModel.getLambda(state);
			mu = stateChangeModel.getMu(state);
			psi = stateChangeModel.getPsi(state);

			logPNode += logfratio(lambda, mu, psi, ti, te);

			if (i >= 0) {
				logPNode += Math.log(stateChangeModel.getRate(dwnState, state));
			} else {
				if (node.isLeaf()) {
					if (te > 1e-4 || !toThePresent) { // Extinct sample
						if(stateChangeModel.isSA()) {
							logPNode += Math.log(psi);
							if(node.isDirectAncestor() && ((MultiRateNode) node.getParent()).getNodeState() != dwnState) throw new IllegalStateException("mismatch");
							if(!node.isDirectAncestor()) logPNode += Math.log(p0(lambda, mu, psi, te));
						}
						else logPNode += Math.log(mu * sigma);
					} else {
						// Extant sample
						logPNode += Math.log(rho);
					}
				} else if(!node.isFake()) {					
					// Next event is a speciation
					logPNode += Math.log(lambda);
				} // if node is fake, nothing to be done - probability handled by the leaf
			}
		}

		for (Node child : node.getChildren()) {
			logPNode += logPNode(child);
		}
		return (logPNode);
	}

}
