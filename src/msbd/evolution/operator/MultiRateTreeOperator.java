/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.evolution.operator;

import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.inference.Operator;
import msbd.base.evolution.tree.MultiRateNode;
import msbd.base.evolution.tree.MultiRateTree;
import msbd.evolution.tree.StateChangeModel;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.Tree;

/**
 * Base class for operators on multi-rate trees.
 */
@Description("This operator generates proposals for a coloured beast tree.")
public abstract class MultiRateTreeOperator extends Operator {

	public Input<MultiRateTree> multiRateTreeInput = new Input<>("multiRateTree", "Multi-rate tree on which to operate.", Validate.REQUIRED);

	public Input<StateChangeModel> stateChangeModelInput = new Input<>("stateChangeModel", "Rate shift model for proposal distribution", Input.Validate.REQUIRED);

	protected MultiRateTree mrTree;
	protected StateChangeModel stateChangeModel;

	@Override
	public void initAndValidate() {
		mrTree = multiRateTreeInput.get();
		stateChangeModel = stateChangeModelInput.get();
	}

	/**
	 * Implement a state removal: check that the state is no longer present on the tree, adapt the tree state indexes
	 * and remove the state from the model.
	 *
	 * @param i the index
	 */
	public void removeState(int i) {
		for (Node node : mrTree.getNodesAsArray()) {
			MultiRateNode mtnode = (MultiRateNode) node;
			int nstate = mtnode.getNodeState();
			if (nstate == i) throw new IllegalStateException("State to remove is present on a node");
			if (nstate > i) mtnode.setNodeState(nstate - 1);

			for (int idx = mtnode.getChangeCount() - 1; idx >= 0; idx--) {
				int state = mtnode.getChangeState(idx);
				if (state == i) throw new IllegalStateException("State to remove is present on a change");
				if (state > i) mtnode.setChangeState(idx, state - 1);
			}
		}

		stateChangeModel.removeState(i);
	}

	/**
	 * Number of separate occurrences of each state in the tree.
	 *
	 * @return the vector of occurrences
	 */
	protected int[] occurrences() {
		int[] counts = new int[stateChangeModel.getNStates()];
		for (Node node2 : mrTree.getNodesAsArray()) {
			MultiRateNode node = (MultiRateNode) node2;
			if (node.isRoot()) counts[node.getNodeState()]++;
			if (node.getChangeCount() > 0) counts[node.getNodeState()]++;
			for (int i = 0; i < node.getChangeCount() - 1; i++) {
				counts[node.getChangeState(i)]++;
			}
		}
		return counts;
	}

	/*
	 * The following two methods are copied verbatim from TreeOperator.
	 */

	/**
	 * Obtain the sister of node "child" having parent "parent".
	 * 
	 * @param parent the parent
	 * @param child the child that you want the sister of
	 * @return the other child of the given parent.
	 */
	protected Node getOtherChild(Node parent, Node child) {
		if (parent.getLeft().getNr() == child.getNr()) {
			return parent.getRight();
		} else {
			return parent.getLeft();
		}
	}

	/**
	 * Replace node "child" with another node.
	 *
	 * @param node the node
	 * @param child the child
	 * @param replacement the replacement
	 */
	public void replace(Node node, Node child, Node replacement) {
		node.removeChild(child);
		node.addChild(replacement);
		node.makeDirty(Tree.IS_FILTHY);
		replacement.makeDirty(Tree.IS_FILTHY);
	}

	/* ********************************************************************* */
	/*
	 * The following are used only for Wilson-Balding at the moment but may be needed for future tree operators
	 */

	/**
	 * Disconnect edge <node,node.getParent()> by joining node's sister directly to node's grandmother and adding all
	 * colour changes previously on <node.getParent(),node.getParent().getParent()> to the new branch.
	 *
	 * @param node the node
	 */
	public void disconnectBranch(Node node) {

		// Check argument validity:
		Node parent = node.getParent();
		if (node.isRoot() || parent.isRoot()) throw new IllegalArgumentException("Illegal argument to " + "disconnectBranch().");

		Node sister = getOtherChild(parent, node);

		// Add colour changes originally attached to parent to those attached
		// to node's sister:
		for (int idx = 0; idx < ((MultiRateNode) parent).getChangeCount(); idx++) {
			int colour = ((MultiRateNode) parent).getChangeState(idx);
			double time = ((MultiRateNode) parent).getChangeTime(idx);
			((MultiRateNode) sister).addChange(colour, time);
		}

		// Implement topology change.
		replace(parent.getParent(), parent, sister);

		// Clear colour changes from parent:
		((MultiRateNode) parent).clearChanges();

		// Ensure BEAST knows to update affected likelihoods:
		parent.makeDirty(Tree.IS_FILTHY);
		sister.makeDirty(Tree.IS_FILTHY);
		node.makeDirty(Tree.IS_FILTHY);
	}

	/**
	 * Disconnect node from root, discarding all colouring on <node,root> and <node's sister,root>.
	 *
	 * @param node the node
	 */
	public void disconnectBranchFromRoot(Node node) {

		// Check argument validity:
		if (node.isRoot() || !node.getParent().isRoot()) throw new IllegalArgumentException("Illegal argument to" + " disconnectBranchFromRoot().");

		// Implement topology change:
		Node parent = node.getParent();
		Node sister = getOtherChild(parent, node);
		sister.setParent(null);
		parent.removeChild(sister);

		// Clear colour changes on new root:
		((MultiRateNode) sister).clearChanges();

		// Ensure BEAST knows to update affected likelihoods:
		parent.makeDirty(Tree.IS_FILTHY);
		sister.makeDirty(Tree.IS_FILTHY);
		node.makeDirty(Tree.IS_FILTHY);
	}

	/**
	 * Creates a new branch between node and a new node at time destTime between destBranchBase and its parent. Colour
	 * changes are divided between the two new branches created by the split.
	 *
	 * @param node the node
	 * @param destBranchBase the dest branch base
	 * @param destTime the dest time
	 */
	public void connectBranch(Node node, Node destBranchBase, double destTime) {

		// Check argument validity:
		if (node.isRoot() || destBranchBase.isRoot()) throw new IllegalArgumentException("Illegal argument to " + "connectBranch().");

		// Obtain existing parent of node and set new time:
		Node parent = node.getParent();
		parent.setHeight(destTime);

		MultiRateNode mtParent = (MultiRateNode) parent;
		MultiRateNode mtDestBranchBase = (MultiRateNode) destBranchBase;

		// Determine where the split comes in the list of colour changes
		// attached to destBranchBase:
		int split;
		for (split = 0; split < mtDestBranchBase.getChangeCount(); split++)
			if (mtDestBranchBase.getChangeTime(split) > destTime) break;

		// Divide colour changes between new branches:
		mtParent.clearChanges();
		for (int idx = split; idx < mtDestBranchBase.getChangeCount(); idx++)
			mtParent.addChange(mtDestBranchBase.getChangeState(idx), mtDestBranchBase.getChangeTime(idx));

		mtDestBranchBase.truncateChanges(split);

		// Set colour at split:
		mtParent.setNodeState(mtDestBranchBase.getFinalState());

		// Implement topology changes:
		replace(destBranchBase.getParent(), destBranchBase, parent);
		destBranchBase.setParent(parent);

		if (parent.getLeft() == node) parent.setRight(destBranchBase);
		else if (parent.getRight() == node) parent.setLeft(destBranchBase);

		// Ensure BEAST knows to update affected likelihoods:
		node.makeDirty(Tree.IS_FILTHY);
		parent.makeDirty(Tree.IS_FILTHY);
		destBranchBase.makeDirty(Tree.IS_FILTHY);
	}

	/**
	 * Set up node's parent as the new root with a height of destTime, with oldRoot as node's new sister.
	 *
	 * @param node the node
	 * @param oldRoot the old root
	 * @param destTime the dest time
	 */
	public void connectBranchToRoot(Node node, Node oldRoot, double destTime) {

		// Check argument validity:
		if (node.isRoot() || !oldRoot.isRoot()) throw new IllegalArgumentException("Illegal argument " + "to connectBranchToRoot().");

		// Obtain existing parent of node and set new time:
		Node newRoot = node.getParent();
		newRoot.setHeight(destTime);

		// Implement topology changes:

		newRoot.setParent(null);

		if (newRoot.getLeft() == node) newRoot.setRight(oldRoot);
		else if (newRoot.getRight() == node) newRoot.setLeft(oldRoot);

		oldRoot.setParent(newRoot);

		// Ensure BEAST knows to recalculate affected likelihood:
		newRoot.makeDirty(Tree.IS_FILTHY);
		oldRoot.makeDirty(Tree.IS_FILTHY);
		node.makeDirty(Tree.IS_FILTHY);
	}
}