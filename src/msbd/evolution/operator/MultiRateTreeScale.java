/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.evolution.operator;

import java.util.ArrayList;
import java.util.List;

import beast.base.core.Input;
import beast.base.inference.parameter.BooleanParameter;
import beast.base.inference.parameter.RealParameter;
import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;
import msbd.base.evolution.tree.MultiRateNode;

/**
 * Scaling operator for a multi-rate tree. Can scale parameters with or inversely with the tree. (adapted from the
 * MultiTypeTree package)
 * 
 * @author Tim Vaughan <tgvaughan@gmail.com>
 */
public class MultiRateTreeScale extends MultiRateTreeOperator {

	public Input<List<RealParameter>> parametersInput = new Input<>("parameter", "Scale this scalar parameter by the same amount as tree.", new ArrayList<RealParameter>());

	public Input<List<BooleanParameter>> indicatorsInput = new Input<>("indicator", "If provided, used to specify a subset of parameter elements to scale.",
			new ArrayList<BooleanParameter>());

	public Input<List<RealParameter>> parametersInverseInput = new Input<>("parameterInverse", "Scale this scalar parameter inversely.", new ArrayList<RealParameter>());

	public Input<List<BooleanParameter>> indicatorsInverseInput = new Input<>("indicatorInverse", "If provided, used to specify a subset of parameter elements to scale "
			+ "inversely.", new ArrayList<BooleanParameter>());

	public Input<Double> scaleFactorInput = new Input<>("scaleFactor", "Scaling is restricted to the range [1/scaleFactor, scaleFactor]");

	boolean indicatorsUsed, indicatorsInverseUsed;

	@Override
	public void initAndValidate() {

		super.initAndValidate();

		if (indicatorsInput.get().size() > 0) {
			if (indicatorsInput.get().size() != parametersInput.get().size())
				throw new IllegalArgumentException("If an indicator element " + "exists, the number of such elements must equal " + "the number of parameter elements.");

			for (int pidx = 0; pidx < parametersInput.get().size(); pidx++) {
				if (parametersInput.get().get(pidx).getDimension() != indicatorsInput.get().get(pidx).getDimension()) {
					throw new IllegalArgumentException("The number of boolean " + "values in indicator element " + String.valueOf(pidx + 1)
					+ " doesn't match the dimension of the " + "corresponding parameter element.");
				}
			}
			indicatorsUsed = true;
		} else indicatorsUsed = false;

		if (indicatorsInverseInput.get().size() > 0) {
			if (indicatorsInverseInput.get().size() != parametersInverseInput.get().size())
				throw new IllegalArgumentException("If an indicatorInverse element " + "exists, the number of such elements must equal "
						+ "the number of parameterInverse elements.");

			for (int pidx = 0; pidx < parametersInverseInput.get().size(); pidx++) {
				if (parametersInverseInput.get().get(pidx).getDimension() != indicatorsInverseInput.get().get(pidx).getDimension()) {
					throw new IllegalArgumentException("The number of boolean " + "values in indicatorInverse element " + String.valueOf(pidx + 1)
					+ " doesn't match the dimension of the " + "corresponding parameterInverse element.");
				}
			}
			indicatorsInverseUsed = true;
		} else indicatorsInverseUsed = false;
	}

	/**
	 * Propose a move.
	 *
	 * @return the logHR
	 */
	@Override
	public double proposal() {

		// Choose scale factor:
		double u = Randomizer.nextDouble();
		double f = u * scaleFactorInput.get() + (1.0 - u) / scaleFactorInput.get();

		// Keep track of Hastings ratio:
		double logf = Math.log(f);
		double logHR = -2 * logf;

		// Scale colour change times on branches with bottom height fixed (leaf or fake):
		for (Node node : mrTree.getNodesAsArray()) {			
			if(!node.isLeaf() && !node.isFake()) continue;
			// nothing to be done if parent height not changing either
			if(node.isDirectAncestor() || node.isRoot() || node.getParent().isFake()) continue;

			double lold = node.getParent().getHeight() - node.getHeight();
			double lnew = f * node.getParent().getHeight() - node.getHeight();

			// Rejecting invalid scalings
			if(lnew < 0) return Double.NEGATIVE_INFINITY;

			for (int c = 0; c < ((MultiRateNode) node).getChangeCount(); c++) {
				double oldTime = ((MultiRateNode) node).getChangeTime(c);
				double newTime = node.getHeight() + (oldTime - node.getHeight()) * lnew / lold;
				((MultiRateNode) node).setChangeTime(c, newTime);
			}

			logHR += ((MultiRateNode) node).getChangeCount() * Math.log(lnew / lold);
		}

		// Scale internal node heights and colour change times:
		for (Node node : mrTree.getNodesAsArray()) {
			if(node.isLeaf() || node.isFake()) continue;

			node.setHeight(node.getHeight() * f);
			logHR += logf;

			if(!node.isRoot() && node.getParent().isFake()) {
				double lold = node.getParent().getHeight() - node.getHeight() / f;
				double lnew = node.getParent().getHeight() - node.getHeight();

				// Rejecting invalid scalings
				if(lnew < 0) return Double.NEGATIVE_INFINITY;

				for (int c = 0; c < ((MultiRateNode) node).getChangeCount(); c++) {
					double oldTime = ((MultiRateNode) node).getChangeTime(c);
					double newTime = node.getParent().getHeight() + (oldTime - node.getParent().getHeight()) * lnew / lold;
					((MultiRateNode) node).setChangeTime(c, newTime);
				}

				logHR += ((MultiRateNode) node).getChangeCount() * Math.log(lnew / lold);

			} else {
				for (int c = 0; c < ((MultiRateNode) node).getChangeCount(); c++) {
					double oldTime = ((MultiRateNode) node).getChangeTime(c);
					((MultiRateNode) node).setChangeTime(c, f * oldTime);
					logHR += logf;
				}
			}
		}

		// Reject invalid tree scalings:
		for (Node node : mrTree.getNodesAsArray()) {
			if (((MultiRateNode) node).getChangeCount() > 0 && ((MultiRateNode) node).getChangeTime(0) < node.getHeight()) {
				throw new IllegalStateException("Scaled colour change time " + "has dipped below age of leaf - this should never happen!");
			}
		}

		// Scale parameters:
		for (int pidx = 0; pidx < parametersInput.get().size(); pidx++) {
			RealParameter param = parametersInput.get().get(pidx);
			for (int i = 0; i < param.getDimension(); i++) {
				if (!indicatorsUsed || indicatorsInput.get().get(pidx).getValue(i)) {
					double oldValue = param.getValue(i);
					double newValue = oldValue * f;
					if (newValue < param.getLower() || newValue > param.getUpper()) return Double.NEGATIVE_INFINITY;

					param.setValue(i, newValue);
					logHR += logf;
				}
			}
		}

		// Scale parameters inversely:
		for (int pidx = 0; pidx < parametersInverseInput.get().size(); pidx++) {
			RealParameter param = parametersInverseInput.get().get(pidx);
			for (int i = 0; i < param.getDimension(); i++) {
				if (!indicatorsInverseUsed || indicatorsInverseInput.get().get(pidx).getValue(i)) {
					double oldValue = param.getValue(i);
					double newValue = oldValue / f;
					if (newValue < param.getLower() || newValue > param.getUpper()) return Double.NEGATIVE_INFINITY;

					param.setValue(i, newValue);
					logHR -= logf;
				}
			}
		}

		// Return Hastings ratio:
		return logHR;
	}

}