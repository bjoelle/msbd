package msbd.evolution.operator;

import java.util.ArrayList;
import java.util.List;

import beast.base.core.Input;
import beast.base.evolution.operator.TipDatesRandomWalker;
import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;
import msbd.base.evolution.tree.MultiRateNode;
import msbd.base.evolution.tree.MultiRateTree;
import msbd.imports.SamplingDate;

/**
 * Operator for sampling the age of a tip, adapted for multi-rate trees with potential SAs.
 */
public class MRTSampleDatesRandomWalker extends TipDatesRandomWalker {
	
	public Input<List<SamplingDate>> samplingDatesInput = new Input<>("samplingDates", "List of sampling dates", new ArrayList<SamplingDate>());
	
	List<String> samplingDateTaxonNames = new ArrayList<>();

	public void initAndValidate() {
		if(!(treeInput.get() instanceof MultiRateTree)) throw new IllegalArgumentException("MRTSampleDatesRandomWalker operator"
				+ "is only valid for multi-rate trees from the MSBD package, use SampledNodeDateRandomWalker or TipDatesRandomWalker"
				+ "for regular trees");
		
		for (SamplingDate taxon:samplingDatesInput.get()) {
            samplingDateTaxonNames.add(taxon.taxonInput.get().getID());
        }
		
		super.initAndValidate();
	}
	
	/**
	 * Propose a move.
	 *
	 * @return the logHR
	 */
	public double proposal() {
		 // randomly select leaf node
        int i = Randomizer.nextInt(taxonIndices.length);
        Node node = treeInput.get().getNode(taxonIndices[i]);

        double value = node.getHeight();
        double newValue = value;

        boolean drawFromDistribution = samplingDateTaxonNames.contains(node.getID());
        if (drawFromDistribution) {
            SamplingDate taxonSamplingDate = samplingDatesInput.get().get(samplingDateTaxonNames.indexOf(node.getID()));
            double range = taxonSamplingDate.getUpper() - taxonSamplingDate.getLower();
            newValue = taxonSamplingDate.getLower() + Randomizer.nextDouble() * range;
        }  else {
            if (useGaussian) {
                newValue += Randomizer.nextGaussian() * windowSize;
            } else {
                newValue += Randomizer.nextDouble() * 2 * windowSize - windowSize;
            }
        }

        double lower, upper;
        if (node.isDirectAncestor()) {
            Node fake = node.getParent();
            lower = getOtherChild(fake, node).getHeight();
            if (fake.getParent() != null) {
                upper = fake.getParent().getHeight();
            } else upper = Double.POSITIVE_INFINITY;
        } else {
            lower = 0.0;
            upper = node.getParent().getHeight();
        }

        if (newValue < lower || newValue > upper || newValue == value) {
            // this saves calculating the posterior
            return Double.NEGATIVE_INFINITY;
        }

        if (node.isDirectAncestor()) {
        	node.getParent().setHeight(newValue);
        }
        else {
        	MultiRateNode mtnode = (MultiRateNode) node;
        	double factor = (mtnode.getParent().getHeight() - newValue) / (mtnode.getParent().getHeight() - value);
        	for(int sc = 0; sc < mtnode.getChangeCount(); sc++) {
        		double newtime = newValue + factor * (mtnode.getChangeTime(sc) - value);
        		mtnode.setChangeTime(sc, newtime);
        	}
        }
        node.setHeight(newValue);
        
        return 0.0;
	}
}
