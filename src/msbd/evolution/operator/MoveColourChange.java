/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.evolution.operator;

import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;
import msbd.base.evolution.tree.MultiRateNode;

/**
 * Operator to move a colour change on its sub-edge (constrained by the nearest rate shift/node above and below).
 */
public class MoveColourChange extends MultiRateTreeOperator {

	/**
	 * Propose a move.
	 *
	 * @return the logHR
	 */
	@Override
	public double proposal() {
		int tot = mrTree.getTotalNumberOfChanges();
		if (tot == 0) return Double.NEGATIVE_INFINITY;
		int k = Randomizer.nextInt(tot);

		MultiRateNode node = null;
		for (Node node2 : mrTree.getNodesAsArray()) {
			if (node2.isRoot()) continue;
			node = (MultiRateNode) node2;
			if (k < node.getChangeCount()) {
				break;
			}
			k -= node.getChangeCount();
		}

		double[] t = node.getTimeRangeAround(k);
		double time = t[0] + (t[1] - t[0]) * Randomizer.nextDouble();
		node.setChangeTime(k, time);

		return 0;
	}

}
