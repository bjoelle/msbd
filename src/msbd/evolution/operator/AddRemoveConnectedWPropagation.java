/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.evolution.operator;

import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;
import msbd.base.evolution.tree.MultiRateNode;

/**
 * Operator to add/remove state changes from the tree. The model/number of states will not be modified.
 */
public class AddRemoveConnectedWPropagation extends RandomPropagationRetypeOperator {

	/**
	 * Propose a move.
	 *
	 * @return the logHR
	 */
	@Override
	public double proposal() {
		double logHR;
		boolean add = Randomizer.nextDouble() < 0.5;
		if (add) logHR = proposal_add();
		else logHR = proposal_remove();

		return logHR;
	}

	/**
	 * Propose a state change removal.
	 *
	 * @return the logHR of the move
	 */
	private double proposal_remove() {
		double logHR = 0.0;

		int tot = mrTree.getTotalNumberOfChanges(); // #colourings = tot+1 (root colouring not considered here)
		if (tot == 0) return Double.NEGATIVE_INFINITY;
		if (tot == stateChangeModel.getNStates() - 1) return Double.NEGATIVE_INFINITY;

		int k = Randomizer.nextInt(tot);
		logHR += Math.log(tot);

		MultiRateNode node = null;
		for (Node node2 : mrTree.getNodesAsArray()) {
			if (node2.isRoot() || node2.isDirectAncestor()) continue;
			node = (MultiRateNode) node2;
			if (k < node.getChangeCount()) {
				break;
			}
			k -= node.getChangeCount();
		}

		int oldstate;
		if (k == 0) oldstate = node.getNodeState();
		else oldstate = node.getChangeState(k - 1);

		logHR += retypeBranchForRemove(node, k);

		// check if color was removed
		boolean flag = true;
		for (Node node2 : mrTree.getNodesAsArray()) {
			node = (MultiRateNode) node2;
			if (node.getNodeState() == oldstate) flag = false;
			for (int i = 0; i < node.getChangeCount() - 1; i++) {
				if (node.getChangeState(i) == oldstate) {
					flag = false;
					break;
				}
			}
			if (!flag) break;
		}
		if (flag) return Double.NEGATIVE_INFINITY; // entire color removal is not allowed

		int n = mrTree.getLeafNodeCount();
		int m = mrTree.getTotalNumberOfChanges();
		int nSA = mrTree.getDirectAncestorNodeCount();
		if (m == 0) {
			throw new IllegalStateException("No shifts present and NStates > 1");
		}
		logHR -= Math.log(2 * n - 2 + m - nSA) + Math.log(stateChangeModel.getNStates() - 1);

		return logHR;
	}

	/**
	 * Propose a state change addition.
	 *
	 * @return the logHR of the move
	 */
	private double proposal_add() {
		if (stateChangeModel.getNStates() == 1) return Double.NEGATIVE_INFINITY;
		double logHR = 0.0;

		int n = mrTree.getLeafNodeCount();
		int m = mrTree.getTotalNumberOfChanges();
		int nSA = mrTree.getDirectAncestorNodeCount(); //zero-edges cannot have changes added
		if (m == 0) {
			throw new IllegalStateException("No shifts present and NStates > 1");
		}

		// Select sub-edge at random:
		int k = Randomizer.nextInt(2 * n - 2 + m - nSA); 
		logHR += Math.log(2 * n - 2 + m - nSA);

		MultiRateNode node = null;
		for (Node node2 : mrTree.getNodesAsArray()) {
			if (node2.isRoot() || node2.isDirectAncestor()) continue;
			node = (MultiRateNode) node2;
			if (k < node.getChangeCount() + 1) {
				break;
			}
			k -= node.getChangeCount() + 1;
		}

		double[] t = node.getTimeRangeBelow(k);
		int oldstate;
		if (k == 0) oldstate = node.getNodeState();
		else oldstate = node.getChangeState(k - 1);
		double changet = t[0] + (t[1] - t[0]) * Randomizer.nextDouble();
		node.insertChange(k, oldstate, changet);

		int newstate;
		do {
			newstate = Randomizer.nextInt(stateChangeModel.getNStates());
		} while (newstate == oldstate);
		logHR += retypeBranchForAdd(node, k, newstate, true) + Math.log(t[1] - t[0]); // HR

		logHR += Math.log(stateChangeModel.getNStates() - 1) - Math.log(mrTree.getTotalNumberOfChanges());

		return logHR;
	}

}
