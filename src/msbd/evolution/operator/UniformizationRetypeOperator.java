/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.evolution.operator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

import beast.base.core.Input;
import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;
import msbd.base.evolution.tree.MultiRateNode;

/**
 * Abstract class of operators on MultiRateTrees which use the Fearnhead-Sherlock uniformization/forward-backward
 * algorithm for branch retyping (adapted from the MultiTypeTree package).
 *
 * @author Tim Vaughan <tgvaughan@gmail.com>
 */
public abstract class UniformizationRetypeOperator extends MultiRateTreeOperator {

	public Input<Boolean> useSymmetrizedRatesInput = new Input<>("useSymmetrizedRates", "Use symmetrized rate matrix to propose shift paths.", false);

	/**
	 * Exception used to signal non-existence of allowed state sequence between node states.
	 */
	protected class NoValidPathException extends Exception {

		private static final long serialVersionUID = 1L;

		@Override
		public String getMessage() {
			return "No valid valid state sequence exists between chosen nodes.";
		}
	}

	private double unif, unifSym;
	private DoubleMatrix Q, R;
	private DoubleMatrix Qsym, Rsym;
	private List<DoubleMatrix> RpowN, RsymPowN;
	private DoubleMatrix RpowMax, RsymPowMax;
	private boolean RpowSteady, RsymPowSteady;

	@Override
	public void initAndValidate() {
		super.initAndValidate();
		// Initialise caching array for powers of uniformized
		// transition matrix:
		RpowN = new ArrayList<>();
		RsymPowN = new ArrayList<>();
		updateMatrices();
	}

	/**
	 * Sample the number of virtual events to occur along branch.
	 * 
	 * General strategy here is to: 1. Draw u from Unif(0,1), 2. Starting from zero, evaluate P(n leq 0|a,b) up until
	 * n=thresh or P(n leq 0|a,b)>u. 3. If P(n leq 0|a,b) has exceeded u, use that n. If not, use rejection sampling to
	 * draw conditional on n being >= thresh.
	 *
	 * @param dwnState State at end (bottom) of branch
	 * @param upState State at start (top) of branch
	 * @param muL Expected unconditioned number of virtual events
	 * @param Pba Probability of final state given start state
	 * @param sym the sym
	 * @return number of virtual events.
	 */
	private int drawEventCount(int dwnState, int upState, double muL, double Pba, boolean sym) {

		int nVirt = 0;

		double u = Randomizer.nextDouble();
		double P_low_given_ab = 0.0;
		double acc = -muL - Math.log(Pba);
		double log_muL = Math.log(muL);

		do {
			// double offset = acc + nVirt*log_muL - Gamma.logGamma(nVirt+1);
			P_low_given_ab += Math.exp(Math.log(getRpowN(nVirt, sym).get(dwnState, upState)) + acc);

			if (P_low_given_ab > u) return nVirt;

			nVirt += 1;
			acc += log_muL - Math.log(nVirt);

		} while (RpowSteadyN(sym) < 0 || nVirt < RpowSteadyN(sym));

		int thresh = nVirt;

		// P_n_given_ab constant for n>= thresh: only need
		// to sample P(n|n>=thresh)
		do {
			nVirt = (int) Randomizer.nextPoisson(muL);
		} while (nVirt < thresh);

		return nVirt;
	}

	/**
	 * Retype branch between srcNode and its parent. Uses the combined uniformization/forward-backward approach of
	 * Fearnhead and Sherlock (2006) to condition on both the beginning and end states.
	 *
	 * @param srcNode the source node
	 * @return Probability of new state.
	 * @throws NoValidPathException the no valid path exception
	 */
	protected double retypeBranch(Node srcNode) throws NoValidPathException {
		
		if(srcNode.isDirectAncestor()) {
			((MultiRateNode) srcNode).clearChanges();
			return 0.0;
		}

		boolean sym = useSymmetrizedRatesInput.get();

		Node srcNodeP = srcNode.getParent();
		double t_srcNode = srcNode.getHeight();
		double t_srcNodeP = srcNodeP.getHeight();

		double L = t_srcNodeP - t_srcNode;

		int state_srcNode = ((MultiRateNode) srcNode).getNodeState();
		int state_srcNodeP = ((MultiRateNode) srcNodeP).getNodeState();

		// Pre-calculate some stuff:
		double muL = getUnif(sym) * L;

		double Pba = MatrixFunctions.expm(getQ(sym).mul(L)).get(state_srcNode, state_srcNodeP);

		// Abort if transition is impossible.
		if (Pba == 0.0) throw new NoValidPathException();

		// Catch for numerical errors
		if (Pba > 1.0 || Pba < 0.0) {
			System.err.println("Warning: matrix exponentiation resulted in rubbish.  Aborting move.");
			return Double.NEGATIVE_INFINITY;
		}

		// Select number of virtual events:
		int nVirt = drawEventCount(state_srcNode, state_srcNodeP, muL, Pba, sym);

		if (nVirt < 0) return Double.NEGATIVE_INFINITY;

		// Select times of virtual events:
		double[] times = new double[nVirt];
		for (int i = 0; i < nVirt; i++)
			times[i] = Randomizer.nextDouble() * L + t_srcNode;
		Arrays.sort(times);

		// Sample state changes along branch using FB algorithm:
		int[] states = new int[nVirt];
		int prevState = state_srcNode;

		for (int i = 1; i <= nVirt; i++) {

			double u2 = Randomizer.nextDouble() * getRpowN(nVirt - i + 1, sym).get(prevState, state_srcNodeP);
			int c;
			boolean fellThrough = true;
			for (c = 0; c < stateChangeModel.getNStates(); c++) {
				u2 -= getR(sym).get(prevState, c) * getRpowN(nVirt - i, sym).get(c, state_srcNodeP);
				if (u2 < 0.0) {
					fellThrough = false;
					break;
				}
			}

			// Check for FB algorithm error:
			if (fellThrough) {

				double sum1 = getRpowN(nVirt - i + 1, sym).get(prevState, state_srcNodeP);
				double sum2 = sum1;
				for (c = 0; c < stateChangeModel.getNStates(); c++) {
					sum2 += getR(sym).get(prevState, c) * getRpowN(nVirt - i, sym).get(c, state_srcNodeP);
				}

				System.err.println("Warning: FB algorithm failure.  Aborting move. " + sum2);
				return Double.NEGATIVE_INFINITY;
			}

			states[i - 1] = c;
			prevState = c;
		}

		double logProb = 0.0;

		// Add non-virtual state changes to branch, calculating probability
		// of path conditional on start state:
		((MultiRateNode) srcNode).clearChanges();
		prevState = state_srcNode;
		double prevTime = t_srcNode;
		for (int i = 0; i < nVirt; i++) {

			if (states[i] != prevState) {

				// Add change to branch:
				((MultiRateNode) srcNode).addChange(states[i], times[i]);

				// Add probability contribution:
				logProb += getQ(sym).get(prevState, prevState) * (times[i] - prevTime) + Math.log(getQ(sym).get(prevState, states[i]));

				prevState = states[i];
				prevTime = times[i];
			}
		}
		logProb += getQ(sym).get(prevState, prevState) * (t_srcNodeP - prevTime);

		// Adjust probability to account for end condition:
		logProb -= Math.log(Pba);

		// Return probability of path given boundary conditions:
		return logProb;
	}

	/**
	 * Obtain probability of the current migratory path above srcNode.
	 *
	 * @param srcNode the source node
	 * @return Path probability.
	 */
	protected double getBranchStatesProb(Node srcNode) {
		
		if(srcNode.isDirectAncestor()) {
			if(((MultiRateNode) srcNode).getChangeCount() > 0) throw new IllegalStateException("State changes on zero-edge, this should never happen");
			return 0.0;
		}

		stateChangeModel = stateChangeModelInput.get();
		boolean sym = useSymmetrizedRatesInput.get();

		double logProb = 0.0;

		Node srcNodeP = srcNode.getParent();
		double t_srcNode = srcNode.getHeight();
		double t_srcNodeP = srcNodeP.getHeight();
		double L = t_srcNodeP - t_srcNode;
		int col_srcNode = ((MultiRateNode) srcNode).getNodeState();
		int col_srcNodeP = ((MultiRateNode) srcNodeP).getNodeState();

		// Probability of branch conditional on start state:
		double lastTime = t_srcNode;
		int lastCol = col_srcNode;
		for (int i = 0; i < ((MultiRateNode) srcNode).getChangeCount(); i++) {
			double thisTime = ((MultiRateNode) srcNode).getChangeTime(i);
			int thisCol = ((MultiRateNode) srcNode).getChangeState(i);

			logProb += (thisTime - lastTime) * getQ(sym).get(lastCol, lastCol) + Math.log(getQ(sym).get(lastCol, thisCol));

			lastTime = thisTime;
			lastCol = thisCol;
		}
		logProb += (t_srcNodeP - lastTime) * getQ(sym).get(lastCol, lastCol);

		// Adjust to account for end condition of path:
		double Pba = MatrixFunctions.expm(getQ(sym).mul(L)).get(col_srcNode, col_srcNodeP);

		// Catch for numerical errors:
		if (Pba > 1.0 || Pba < 0.0) {
			System.err.println("Warning: matrix exponentiation resulted in rubbish.  Aborting move.");
			return Double.NEGATIVE_INFINITY;
		}

		logProb -= Math.log(Pba);

		return logProb;
	}

	/**
	 * Update matrices.
	 */
	public void updateMatrices() {

		stateChangeModel = stateChangeModelInput.get();
		int nStates = stateChangeModel.getNStates();

		unif = 0.0;
		unifSym = 0.0;
		Q = new DoubleMatrix(nStates, nStates);
		Qsym = new DoubleMatrix(nStates, nStates);

		// Set up backward transition rate matrix Q and symmetrized backward
		// transition rate matrix Qsym:
		for (int i = 0; i < nStates; i++) {
			Q.put(i, i, 0.0);
			Qsym.put(i, i, 0.0);
			for (int j = 0; j < nStates; j++) {
				if (i != j) {
					Q.put(i, j, stateChangeModel.getRate(i, j));
					Q.put(i, i, Q.get(i, i) - Q.get(i, j));

					Qsym.put(i, j, 0.5 * (stateChangeModel.getRate(i, j) + stateChangeModel.getRate(j, i)));
					Qsym.put(i, i, Qsym.get(i, i) - Qsym.get(i, j));
				}
			}

			if (-Q.get(i, i) > unif) unif = -Q.get(i, i);

			if (-Qsym.get(i, i) > unifSym) unifSym = -Qsym.get(i, i);
		}

		// Set up uniformized backward transition rate matrices R and Rsym:
		R = Q.mul(1.0 / unif).add(DoubleMatrix.eye(nStates));
		Rsym = Qsym.mul(1.0 / unifSym).add(DoubleMatrix.eye(nStates));

		// Clear cached powers of R and Rsym and steady state flag:
		RpowN.clear();
		RsymPowN.clear();

		RpowSteady = false;
		RsymPowSteady = false;

		// Power sequences initially contain R^0 = I
		RpowN.add(DoubleMatrix.eye(nStates));
		RsymPowN.add(DoubleMatrix.eye(nStates));

		RpowMax = DoubleMatrix.eye(nStates);
		RsymPowMax = DoubleMatrix.eye(nStates);
	}

	/**
	 * Gets the matrix R to the power of n.
	 *
	 * @param n the power
	 * @param symmetric whether to use the symmetric matrix
	 * @return R^n
	 */
	public DoubleMatrix getRpowN(int n, boolean symmetric) {
		updateMatrices();

		List<DoubleMatrix> matPowerList;
		DoubleMatrix mat, matPowerMax;
		if (symmetric) {
			matPowerList = RsymPowN;
			mat = Rsym;
			matPowerMax = RsymPowMax;
		} else {
			matPowerList = RpowN;
			mat = R;
			matPowerMax = RpowMax;
		}

		if (n >= matPowerList.size()) {

			// Steady state of matrix iteration already reached
			if ((symmetric && RsymPowSteady) || (!symmetric && RpowSteady)) {
				// System.out.println("Assuming R SS.");
				return matPowerList.get(matPowerList.size() - 1);
			}

			int startN = matPowerList.size();
			for (int i = startN; i <= n; i++) {
				matPowerList.add(matPowerList.get(i - 1).mmul(mat));

				matPowerMax.maxi(matPowerList.get(i));

				// Occasionally check whether matrix iteration has reached steady state
				if (i % 10 == 0) {
					double maxDiff = 0.0;
					for (double el : matPowerList.get(i).sub(matPowerList.get(i - 1)).toArray())
						maxDiff = Math.max(maxDiff, Math.abs(el));

					if (!(maxDiff > 0)) {
						if (symmetric) RsymPowSteady = true;
						else RpowSteady = true;

						return matPowerList.get(i);
					}
				}
			}
		}
		return matPowerList.get(n);
	}

	/**
	 * Power above which R is known to be steady.
	 *
	 * @param symmetric whether to use the symmetric matrix
	 * @return index of first known steady element.
	 */
	public int RpowSteadyN(boolean symmetric) {
		if (symmetric) {
			if (RsymPowSteady) return RsymPowN.size();
			else return -1;
		} else {
			if (RpowSteady) return RpowN.size();
			else return -1;
		}
	}

	/**
	 * Return matrix containing upper bounds on elements from the powers of R if known. Returns a matrix of ones if
	 * steady state has not yet been reached.
	 *
	 * @param symmetric whether to use the symmetric matrix
	 * @return Matrix of upper bounds.
	 */
	public DoubleMatrix getRpowMax(boolean symmetric) {
		int nStates = stateChangeModel.getNStates();
		if (symmetric) {
			if (RsymPowSteady) return RsymPowMax;
			else return DoubleMatrix.ones(nStates, nStates);
		} else {
			if (RpowSteady) return RpowMax;
			else return DoubleMatrix.ones(nStates, nStates);
		}
	}

	/**
	 * Gets the R matrix.
	 *
	 * @param symmetric whether to use the symmetric matrix
	 * @return R
	 */
	public DoubleMatrix getR(boolean symmetric) {
		updateMatrices();
		if (symmetric) return Rsym;
		else return R;
	}

	/**
	 * Gets the Q matrix.
	 *
	 * @param symmetric whether to use the symmetric matrix
	 * @return Q
	 */
	public DoubleMatrix getQ(boolean symmetric) {
		updateMatrices();
		if (symmetric) return Qsym;
		else return Q;
	}

	/**
	 * Gets unif.
	 *
	 * @param symmetric whether to use the symmetric rate
	 * @return unif
	 */
	public double getUnif(boolean symmetric) {
		updateMatrices();
		if (symmetric) return unifSym;
		else return unif;
	}
}
