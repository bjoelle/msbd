/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.evolution.operator;

import beast.base.core.Input;
import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;
import msbd.base.evolution.tree.MultiRateNode;

/**
 * Base class for propagation operator. Propagation will retype nodes and branches recursively towards the tips, it will
 * either stop at the next rate shift, or stop early by picking a time uniformly at random.
 */
public abstract class RandomPropagationRetypeOperator extends MultiRateTreeOperator {

	public Input<Double> propagationPInput = new Input<Double>("propagationP", "probability of propagation of new state for each branch, default 0.95", 0.95);

	double propagationP, logProp, logNoProp;

	@Override
	public void initAndValidate() {
		super.initAndValidate();
		propagationP = propagationPInput.get();
		logProp = Math.log(propagationP);
		logNoProp = Math.log(1 - propagationP);
	}

	/**
	 * Retype branch after removing a rate shift.
	 *
	 * @param node the starting node
	 * @param k the index of the rate shift being removed
	 * @return the logHR of the move
	 */
	protected double retypeBranchForRemove(MultiRateNode node, int k) {
		// this is not used by the color removal, so reverse is always true
		double logHR = 0.0;
		boolean propagation = Randomizer.nextDouble() < propagationP;
		double[] tk = node.getTimeRangeAround(k);

		if (propagation) {
			int newstate = node.getChangeState(k);
			if (k > 0) {
				if ((k > 1 && node.getChangeState(k - 2) == newstate) || (k == 1 && node.getNodeState() == newstate)) {
					// propagation leads to joining 2 components with the same state
					double[] t = node.getTimeRangeAround(k - 1);
					node.removeChange(k);
					logHR -= logProp // forward
							- logNoProp + Math.log(t[1] - t[0]); // reverse
					tk[0] = t[0];
					node.removeChange(k - 1);
				} else {
					node.setChangeState(k - 1, newstate); // forward and reverse HR cancel out
					node.removeChange(k);
				}
			} else {
				node.removeChange(k);
				node.setNodeState(newstate); // forward and reverse HR cancel out
				logHR += propagateRetype(node, true);
			}
		} else {
			double[] t = node.getTimeRangeBelow(k);
			double time = t[0] + (t[1] - t[0]) * Randomizer.nextDouble();
			node.setChangeTime(k, time);
			logHR -= logNoProp - Math.log(t[1] - t[0]) // forward
					- logProp; // reverse
		}

		logHR -= Math.log(tk[1] - tk[0]);
		return logHR;
	}

	/**
	 * Retype branch after adding a rate shift.
	 *
	 * @param node the starting node
	 * @param k the index of the added rate shift
	 * @param newstate the state of the new rate shift
	 * @param reverse whether to account for partial propagation of the reverse move in the HR (false if the reverse
	 *        move uses NoStop propagation)
	 * @return the logHR
	 */
	protected double retypeBranchForAdd(MultiRateNode node, int k, int newstate, boolean reverse) {
		double logHR = 0.0;
		boolean propagation = Randomizer.nextDouble() < propagationP;

		if (propagation) {
			if (!reverse) logHR -= logProp; // if no reverse, no cancel out
			if (k > 0) {
				if ((k > 1 && node.getChangeState(k - 2) == newstate) || (k == 1 && node.getNodeState() == newstate)) {
					// if no reverse, this does not happen (because it is an add colour move)
					double[] t = node.getTimeRangeAround(k - 1);
					logHR -= logProp // forward
							- logNoProp + Math.log(t[1] - t[0]); // reverse
					node.removeChange(k - 1);
				} else {
					node.setChangeState(k - 1, newstate);
				} // forward and reverse HR cancel out
			} else {
				node.setNodeState(newstate); // forward and reverse HR cancel out
				logHR += propagateRetype(node, reverse);
			}
		} else {
			double[] t = node.getTimeRangeBelow(k);
			double time = t[0] + (t[1] - t[0]) * Randomizer.nextDouble();
			node.insertChange(k, newstate, time);
			logHR -= logNoProp - Math.log(t[1] - t[0]); // forward
			if (reverse) logHR += logProp; // reverse
		}

		return logHR;
	}

	/**
	 * Retype branch after removing a rate shift, without possibility of stopping early. This is used for removing a
	 * state from the tree entirely.
	 *
	 * @param node the starting node
	 * @param k the index of the rate shift being removed
	 * @return the logHR of the move
	 */
	protected double retypeBranchForRemoveNoStop(MultiRateNode node, int k) {
		double logHR = 0.0;
		double[] tk = node.getTimeRangeAround(k);

		int newstate = node.getChangeState(k);
		if (k > 0) { // not checking for redundant change ?
			if ((k > 1 && node.getChangeState(k - 2) == newstate) || (k == 1 && node.getNodeState() == newstate)) {
				double[] t = node.getTimeRangeAround(k - 1);
				node.removeChange(k);
				logHR += logNoProp - Math.log(t[1] - t[0]); // reverse
				tk[0] = t[0];
				node.removeChange(k - 1);
			} else {
				node.setChangeState(k - 1, newstate);
				node.removeChange(k);
				logHR += logProp; // contribution of reverse move
			}
		} else {
			node.setNodeState(newstate);
			node.removeChange(k);
			logHR += logProp + propagateRetypeNoStop(node); // contribution of reverse move
		}

		logHR -= Math.log(tk[1] - tk[0]);
		return logHR;
	}
	
	// the functions above are never used on a direct ancestor 
	// because adding/removing changes on zero-edges is disallowed
	// the propagation functions on the other hand need to account for direct ancestors
	// propagation is always on for zero-edges

	/**
	 * Propagate retype towards the tips.
	 *
	 * @param node the starting node
	 * @param reverse whether to account for a reverse move in the HR
	 * @return the logHR of the retype
	 */
	protected double propagateRetype(MultiRateNode node, boolean reverse) {
		double logHR = 0.0;
		int state = node.getNodeState();
		for (Node ch : node.getChildren()) {
			MultiRateNode child = (MultiRateNode) ch;
			boolean propagation = child.isDirectAncestor() || Randomizer.nextDouble() < propagationP;
			int n = child.getChangeCount();
			if (propagation) {
				if (!reverse && !child.isDirectAncestor()) logHR -= logProp; // if no reverse, no cancel out
				if (n > 0) {
					if ((n > 1 && child.getChangeState(n - 2) == state) || (n == 1 && child.getNodeState() == state)) {
						// if no reverse, this does not happen (because it is an add colour move)
						double[] t = child.getTimeRangeAround(n - 1);
						logHR -= logProp // forward
								- logNoProp + Math.log(t[1] - t[0]); // reverse
						child.removeChange(n - 1);
					} else {
						child.setChangeState(n - 1, state);
					} // forward and reverse HR cancel out
				} else {
					child.setNodeState(state);
					logHR += propagateRetype(child, reverse); // forward and reverse HR cancel out
				}
			} else {
				double[] t = child.getTimeRangeBelow(child.getChangeCount());
				double time = t[0] + (t[1] - t[0]) * Randomizer.nextDouble();
				child.addChange(state, time);
				logHR -= logNoProp - Math.log(t[1] - t[0]); // forward
				if (reverse) logHR += logProp; // reverse
			}
		}
		return logHR;
	}

	/**
	 * Propagate retype towards the tips, without possibility of stopping early.
	 *
	 * @param node the starting node
	 * @return the logHR of the retype
	 */
	protected double propagateRetypeNoStop(MultiRateNode node) {
		double logHR = 0.0;
		int state = node.getNodeState();
		for (Node ch : node.getChildren()) {
			MultiRateNode child = (MultiRateNode) ch;
			int n = child.getChangeCount();
			if (n > 0) {
				if ((n > 1 && child.getChangeState(n - 2) == state) || (n == 1 && child.getNodeState() == state)) {
					double[] t = child.getTimeRangeAround(n - 1);
					child.removeChange(n - 1);
					logHR += logNoProp - Math.log(t[1] - t[0]); // HR contribution of reverse move
				} else {
					child.setChangeState(n - 1, state);
					logHR += logProp; // HR contribution of reverse move
				}
			} else {
				child.setNodeState(state);
				if(!child.isDirectAncestor()) logHR += logProp;
				logHR += propagateRetypeNoStop(child); // HR contribution of reverse move
			}
		}
		return logHR;
	}

}
