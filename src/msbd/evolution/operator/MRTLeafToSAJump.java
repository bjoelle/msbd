package msbd.evolution.operator;

import beast.base.core.Input;
import beast.base.inference.parameter.IntegerParameter;
import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;
import msbd.base.evolution.tree.MultiRateNode;

/**
 * Operator for switching a fossil tip between sampled ancestor and leaf, adapted for multi-rate trees.
 */
public class MRTLeafToSAJump extends UniformizationRetypeOperator {

	public Input<IntegerParameter> categoriesInput = new Input<IntegerParameter>("rateCategories", "rate category per branch");

	/**
	 * Propose a move.
	 *
	 * @return the logHR
	 */
	@Override
	public double proposal() {

		double logHR = 0.0;

		int categoryCount = 1;
		if (categoriesInput.get() != null) {
			categoryCount = categoriesInput.get().getUpper() - categoriesInput.get().getLower() +1;
		}

		int leafNodeCount = mrTree.getLeafNodeCount();
		Node leaf = mrTree.getNode(Randomizer.nextInt(leafNodeCount));
		Node parent = leaf.getParent();

		if (leaf.isDirectAncestor()) {
			logHR = proposal_SAtoLeaf(leaf, parent, categoryCount);
		} else {
			logHR = proposal_LeaftoSA(leaf, parent, categoryCount);
		}

		return logHR;
	}

	/**
	 * Propose switch from SA to leaf.
	 *
	 * @param SA the node to switch
	 * @param parent the parent node
	 * @param categoryCount the category count of the clock model
	 * @return the logHR
	 */
	double proposal_SAtoLeaf(Node SA, Node parent, int categoryCount) {

		double newHeight, logHR = 0.0;

		if (parent.isRoot()) {
			final double randomNumber = Randomizer.nextExponential(1);
			newHeight = parent.getHeight() + randomNumber;
			logHR += randomNumber;
		} else {
			double newRange;
			if(((MultiRateNode) parent).getChangeCount() == 0) newRange = parent.getParent().getHeight() - parent.getHeight();
			else newRange = ((MultiRateNode) parent).getChangeTime(0) - parent.getHeight();
			newHeight = parent.getHeight() + Randomizer.nextDouble() * newRange;
			logHR += Math.log(newRange);
		}

		if (categoriesInput.get() != null) {
			int index = SA.getNr();
			int newValue = Randomizer.nextInt(categoryCount) + categoriesInput.get().getLower(); // from 0 to n-1, n must > 0,
			categoriesInput.get().setValue(index, newValue);
			logHR += Math.log(categoryCount);
		}
		
		parent.setHeight(newHeight);		
		((MultiRateNode) SA).setNodeState(Randomizer.nextInt(stateChangeModel.getNStates()));
		logHR += Math.log(stateChangeModel.getNStates());
		
		try {
			logHR -= retypeBranch(SA);
		} catch (NoValidPathException e) {
			return Double.NEGATIVE_INFINITY;
		}
		
		// Rejecting the move if it creates a ghost colour, i.e one that is in the model but not the tree
		int[] counts = occurrences();
		for (int x : counts) {
			if (x == 0) return Double.NEGATIVE_INFINITY;
		}

		return logHR;
	}

	/**
	 * Propose switch from leaf to SA.
	 *
	 * @param leaf the node to switch
	 * @param parent the parent node
	 * @param categoryCount the category count of the clock model
	 * @return the logHR
	 */
	double proposal_LeaftoSA(Node leaf, Node parent, int categoryCount) {

		//make sure that the branch where a new sampled node to appear is not above that sampled node
		if (getOtherChild(parent, leaf).getHeight() >= leaf.getHeight())  {
			return Double.NEGATIVE_INFINITY;
		}

		double oldRange, logHR = 0.0;

		if (parent.isRoot()) {
			oldRange = Math.exp(parent.getHeight() - leaf.getHeight());
		} else {
			oldRange = parent.getParent().getHeight() - leaf.getHeight();
		}
		logHR -= Math.log(oldRange);

		if  (categoriesInput.get() != null) {
			int index = leaf.getNr();
			categoriesInput.get().setValue(index, -1);
			logHR -= Math.log(categoryCount);
		}

		logHR += getBranchStatesProb(leaf);

		((MultiRateNode) leaf).clearChanges();
		parent.setHeight(leaf.getHeight());
		
		((MultiRateNode) leaf).setNodeState(((MultiRateNode) parent).getNodeState());
		logHR -= Math.log(stateChangeModel.getNStates());
		
		// Rejecting the move if it creates a ghost colour, i.e one that is in the model but not the tree
		int[] counts = occurrences();
		for (int x : counts) {
			if (x == 0) return Double.NEGATIVE_INFINITY;
		}

		return logHR;
	}

}
