/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.evolution.operator;

import beast.base.core.Input;
import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;
import msbd.base.evolution.tree.MultiRateNode;

/**
 * Randomly selects internal tree event (i.e. node or state change, by default not the root) 
 * and moves node height uniformly in interval restricted
 * by the event's parent and children. (adapted from the MultiTypeTree package)
 * 
 * @author Denise Kuhnert
 */
public class MultiRateTreeUniform extends MultiRateTreeOperator {

	public Input<Boolean> includeRootInput = new Input<>("includeRoot", "Allow modification of root node.", false);

	public Input<Double> rootScaleFactorInput = new Input<>("rootScaleFactor", "Root scale factor.", 0.9);

	/**
	 * Propose a move.
	 *
	 * @return the logHR
	 */
	@Override
	public double proposal() {
		// Randomly select event on tree (except for SA nodes):
		int totalEvents = mrTree.getInternalNodeCount() + mrTree.getTotalNumberOfChanges() - mrTree.getDirectAncestorNodeCount();
		if(!includeRootInput.get()) totalEvents--;
		if(totalEvents < 1) return Double.NEGATIVE_INFINITY;
		int event = Randomizer.nextInt(totalEvents);

		MultiRateNode node = null;
		int changeIdx = -1, nevents;
		
		for (Node node_tmp : mrTree.getNodesAsArray()) {
			if (!includeRootInput.get() && node_tmp.isRoot()) continue;
			
			// if leaf or fake, not counting the node itself (SAs should not have changes either)
			if(node_tmp.isLeaf() || node_tmp.isFake()) nevents = ((MultiRateNode) node_tmp).getChangeCount(); 
			else nevents = ((MultiRateNode) node_tmp).getChangeCount() + 1;
			if (event < nevents) {
				node = (MultiRateNode) node_tmp;
				changeIdx = (node_tmp.isLeaf() || node_tmp.isFake()) ? event : event - 1;
				break;
			}
			event -= nevents;
		}

		if (node == null) throw new IllegalStateException("Event selection loop fell through!");

		if (changeIdx == -1) { // scaling node itself
			if (node.isRoot()) {
				// Scale distance root and closest event
				double tmin = Math.max(((MultiRateNode) node.getLeft()).getFinalChangeTime(), ((MultiRateNode) node.getRight()).getFinalChangeTime());

				double u = Randomizer.nextDouble();
				double f = u * rootScaleFactorInput.get() + (1 - u) / rootScaleFactorInput.get();

				double tnew = tmin + f * (node.getHeight() - tmin);

				node.setHeight(tnew);
				return -Math.log(f);
			} else {
				// Reposition node randomly between closest events
				double tmin = Math.max(((MultiRateNode) node.getLeft()).getFinalChangeTime(), ((MultiRateNode) node.getRight()).getFinalChangeTime());

				double tmax = node.getChangeCount() > 0 ? node.getChangeTime(0) : node.getParent().getHeight();

				double u = Randomizer.nextDouble();
				double tnew = u * tmin + (1.0 - u) * tmax;

				node.setHeight(tnew);
				return 0.0;
			}
		} else { // scaling change on node
			double tmin, tmax;
			if (changeIdx + 1 < node.getChangeCount()) tmax = node.getChangeTime(changeIdx + 1);
			else tmax = node.getParent().getHeight();

			if (changeIdx - 1 < 0) tmin = node.getHeight();
			else tmin = node.getChangeTime(changeIdx - 1);

			double u = Randomizer.nextDouble();
			double tnew = u * tmin + (1 - u) * tmax;

			node.setChangeTime(changeIdx, tnew);
			return 0.0;
		}
	}
}
