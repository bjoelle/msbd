/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.evolution.operator;

import beast.base.core.Input;
import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;

/**
 * Subtree exchange operator for multi-rate trees (adapted from the MultiTypeTree package).
 *
 * @author Tim Vaughan <tgvaughan@gmail.com>
 */
public class MRTSubtreeExchange extends UniformizationRetypeOperator {

	public Input<Boolean> isNarrowInput = new Input<Boolean>("isNarrow", "Whether or not to use narrow exchange. (Default true.)", true);

	/**
	 * Propose a move.
	 *
	 * @return the logHR
	 */
	@Override
	public double proposal() {
		double logHR = 0.0;
		
		if(mrTree.getNodeCount() == 3 && mrTree.getRoot().isFake()) return Double.NEGATIVE_INFINITY;

		// Select source and destination nodes:
		Node srcNode, srcNodeParent, destNode, destNodeParent;
		if (isNarrowInput.get()) {

			// Narrow exchange selection:
			do {
				srcNode = mrTree.getNode(Randomizer.nextInt(mrTree.getNodeCount()));
			} while (srcNode.isRoot() || srcNode.getParent().isRoot() || srcNode.isDirectAncestor());
			srcNodeParent = srcNode.getParent();
			destNode = getOtherChild(srcNodeParent.getParent(), srcNodeParent);
			destNodeParent = destNode.getParent();

		} else {

			// Wide exchange selection:
			do {
				srcNode = mrTree.getNode(Randomizer.nextInt(mrTree.getNodeCount()));
			} while (srcNode.isRoot() || srcNode.isDirectAncestor());
			srcNodeParent = srcNode.getParent();

			do {
				destNode = mrTree.getNode(Randomizer.nextInt(mrTree.getNodeCount()));
			} while (destNode == srcNode || destNode.isRoot() || destNode.isDirectAncestor() || destNode.getParent() == srcNode.getParent());
			destNodeParent = destNode.getParent();
		}

		// Reject if substitution would result in negative branch lengths:
		if (destNode.getHeight() >= srcNodeParent.getHeight() || srcNode.getHeight() >= destNodeParent.getHeight()) return Double.NEGATIVE_INFINITY;

		// Record probability of old states:
		logHR += getBranchStatesProb(srcNode) + getBranchStatesProb(destNode);

		// Make changes to tree topology:
		replace(srcNodeParent, srcNode, destNode);
		replace(destNodeParent, destNode, srcNode);

		// Recolour branches involved:
		try {
			logHR -= retypeBranch(srcNode) + retypeBranch(destNode);
		} catch (NoValidPathException e) {
			return Double.NEGATIVE_INFINITY;
		}

		// Rejecting the move if it creates a ghost colour, i.e one that is in the model but not the tree
		int[] counts = occurrences();
		for (int x : counts) {
			if (x == 0) return Double.NEGATIVE_INFINITY;
		}

		return logHR;
	}

}
