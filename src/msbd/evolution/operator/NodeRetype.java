/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.evolution.operator;

import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;
import msbd.base.evolution.tree.MultiRateNode;

/**
 * Change the state of a random node and retype all edges connected to that node, using the uniformization retype method
 * (adapted from the MultiTypeTree package).
 *
 * @author Tim Vaughan <tgvaughan@gmail.com>
 */
public class NodeRetype extends UniformizationRetypeOperator {

	/**
	 * Propose a move.
	 *
	 * @return the logHR
	 */
	@Override
	public double proposal() {
		double logHR = 0.0;
		int n = stateChangeModel.getNStates();
		if (n == 1) return Double.NEGATIVE_INFINITY;

		// Select node:
		Node node;
		do node = mrTree.getNode(Randomizer.nextInt(mrTree.getNodeCount()));
		while(node.isDirectAncestor());

		// Record probability of current states along attached branches:
		if (!node.isRoot()) logHR += getBranchStatesProb(node);
		if (!node.isLeaf()) logHR += getBranchStatesProb(node.getLeft()) + getBranchStatesProb(node.getRight());

		// Select new node state:
		int newstate = Randomizer.nextInt(n);
		((MultiRateNode) node).setNodeState(newstate);
		if(node.isFake()) ((MultiRateNode) node.getDirectAncestorChild()).setNodeState(newstate);

		// Retype attached branches:
		try {
			if (!node.isRoot()) logHR -= retypeBranch(node);
			if (!node.isLeaf()) logHR -= retypeBranch(node.getLeft()) + retypeBranch(node.getRight());
		} catch (NoValidPathException e) {
			return Double.NEGATIVE_INFINITY;
		}

		// Rejecting the move if it creates a ghost colour, i.e one that is in the model but not the tree
		int[] counts = occurrences();
		for (int x : counts) {
			if (x == 0) return Double.NEGATIVE_INFINITY;
		}

		return logHR;
	}
}
