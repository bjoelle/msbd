/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.evolution.tree;

import java.util.Arrays;

import beast.base.inference.CalculationNode;
import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.inference.parameter.IntegerParameter;
import beast.base.inference.parameter.RealParameter;
import msbd.base.inference.parameter.ExtendableRealParameter;

/**
 * Basic state change model.
 */
@Description("Model allowing changes between multiple states")
public class StateChangeModel extends CalculationNode {

	public Input<RealParameter> gammaInput = new Input<>("gamma", "Transition rate", Validate.REQUIRED);
	public Input<ExtendableRealParameter> lambdasInput = new Input<>("lambdas", "Birth rates, at least one is required", Validate.REQUIRED);
	public Input<ExtendableRealParameter> musInput = new Input<>("mus", "Death rates, at least one is required", Validate.REQUIRED);
	
	public Input<Boolean> isSAInput = new Input<>("isSA", "Does the model include sampled ancestors", false);
	public Input<ExtendableRealParameter> psisInput = new Input<>("psis", "Sampling rate for SA trees");
	
	public Input<RealParameter> rhoInput = new Input<>("rho", "Sampling proportion at present, default 1");
	public Input<RealParameter> sigmaInput = new Input<>("sigma", "Past sampling probability on recovery/extinction (non-SA trees), default 0");

	public Input<Boolean> isLambdaFixed = new Input<>("isLambdaFixed", "Whether to use a constant lambda for all states, default false", false);
	public Input<Boolean> isMuFixed = new Input<>("isMuFixed", "Whether to use a constant mu for all states, default false", false);
	public Input<Boolean> isPsiFixed = new Input<>("isPsiFixed", "Whether to use a constant psi for all states, default false", false);

	public Input<IntegerParameter> nstarInput = new Input<>("nstar",
			"Total number of states in the model (some of these states may not be present in the tree)");

	// used by Beauti for activating operators
	public Input<Boolean> estimateNStatesInput = new Input<>("estimateNStates", "Is the number of states allowed to change", true);

	private ExtendableRealParameter lambdas, mus, psis;
	private RealParameter gamma, rho, sigma;
	private IntegerParameter nstar;
	protected int nStates;

	private boolean uniformTransitions = true;

	/**
	 * Instantiates a new state change model.
	 */
	public StateChangeModel() {}

	/**
	 * Initialize and check inputs.
	 */
	@Override
	public void initAndValidate() {
		lambdas = lambdasInput.get();
		lambdas.setLower(0.0);
		if (isLambdaFixed() && lambdas.getDimension() > 1) lambdas.setDimension(1); // accommodate weird inputs

		mus = musInput.get();
		mus.setLower(0.0);
		if (isMuFixed() && mus.getDimension() > 1) mus.setDimension(1); // accommodate weird inputs

		gamma = gammaInput.get();
		gamma.setLower(0.0);

		// sampling validity checks
		rho = rhoInput.get() != null ? rhoInput.get() : new RealParameter(new Double[] {1.0});
		rho.setBounds(0.0, 1.0);
		if (rho.getValue() < 0 || rho.getValue() > 1) throw new IllegalArgumentException("Extant sampling proportion must be between 0 and 1");
		
		if(!isSA()) {
			sigma = sigmaInput.get() != null ? sigmaInput.get() : new RealParameter(new Double[] {0.0});
			sigma.setBounds(0.0, 1.0);
			if (sigma.getValue() < 0 || sigma.getValue() > 1) throw new IllegalArgumentException("Extinct sampling probability must be between 0 and 1");
		}
		else if(psisInput.get() == null) throw new IllegalArgumentException("Sampling rate(s) must be specified for SA tree");
		else {
			psis = psisInput.get();
			psis.setLower(0.0);
			if (Arrays.stream(psis.getValues()).anyMatch(x -> x < 0)) throw new IllegalArgumentException("All sampling rates should be positive");
		}
		
		// rates validity checks (NB: setLower does NOT perform any actual checks)
		if (Arrays.stream(lambdas.getValues()).anyMatch(x -> x < 0)) throw new IllegalArgumentException("All birth rates should be positive");
		if (Arrays.stream(mus.getValues()).anyMatch(x -> x < 0)) throw new IllegalArgumentException("All death rates should be positive");
		
		if (gamma.getDimension() > 1) {
			System.err.println("Several state change rates supplied, only the first one will be used");
			gamma.setDimension(1);
			gammaInput.setValue(gamma, null);
		}
		if (gamma.getValue() < 0) throw new IllegalArgumentException("State change rate should be positive");

		nStates = 1;
		checkSetNStates();

		if (nstarInput.get() != null) {
			nstar = nstarInput.get();
			if (nstar.getValue() < nStates)
				throw new IllegalArgumentException("True number of states cannot be inferior to number of observed states.");
			if (nstar.getValue() < 2)
				throw new IllegalArgumentException("True number of states should be at least 2.");
		} else {
			Integer[] nstarInit = { nStates + 1 };
			nstar = new IntegerParameter(nstarInit);
		}
	}

	/**
	 * Gets the migration rate from state i to j. Rates are backward in time, which does not matter AS LONG as they are
	 * symmetric (otherwise, careful).
	 *
	 * @param i the from state
	 * @param j the to state
	 * @return the rate
	 */
	public double getRate(int i, int j) { //
		if (i == j || nstar.getValue() < 2) return 0;
		if (uniformTransitions) return gamma.getValue() / (nstar.getValue() - 1);
		return 0; // TODO only uniform transitions are implemented
	}

	/**
	 * Sets the lambda for state i.
	 *
	 * @param i the state
	 * @param lambda the new lambda
	 */
	public void setLambda(int i, double lambda) {
		if (isLambdaFixed()) lambdas.setValue(lambda);
		else lambdas.setValue(i, lambda);
	}

	/**
	 * Sets the lambdas.
	 *
	 * @param newvalues the new lambdas
	 */
	public void setLambdas(Double[] newvalues) {
		if (isLambdaFixed()) {
			if (newvalues.length == 1) lambdas.setValue(newvalues[0]);
			else throw new UnsupportedOperationException("Cannot assign multiple values to fixed lambda");
		} else {
			lambdas.setValue(0, newvalues[0]); // this is a hack -- setDimension does not set the hasStartedEditing flag
												// but this does, preserving the ACTUAL original state
			int n = newvalues.length;
			lambdas.setDimension(n);
			for (int i = 1; i < n; i++) {
				lambdas.setValue(i, newvalues[i]);
			}
		}
	}

	/**
	 * Gets the lambda for state i.
	 *
	 * @param i the state
	 * @return the lambda
	 */
	public double getLambda(int i) {
		if (isLambdaFixed()) return lambdas.getValue();
		return lambdas.getValue(i);
	}

	/**
	 * Gets the lambdas.
	 *
	 * @return the lambdas
	 */
	public Double[] getLambdas() {
		return lambdas.getValues();
	}

	/**
	 * Sets gamma.
	 *
	 * @param g the new gamma
	 */
	public void setGamma(double g) {
		gamma.setValue(g);
	}

	/**
	 * Gets effective gamma (set to zero when nstar=1).
	 *
	 * @return effective gamma
	 */
	public double getGamma() {
		if (getNstar() >= 2) return gamma.getValue();
		else return 0.0;
	}

	/**
	 * Gets the number of states.
	 *
	 * @return the number of states
	 */
	public int getNStates() {
		checkSetNStates();
		return nStates;
	}

	/**
	 * Sets the number of states.
	 *
	 * @param n the new number of states
	 */
	public void setNStates(int n) {
		nStates = n;
		checkSetNStates();
	}

	/**
	 * Checks the compatibility of the dimensions of lambda, mu and (if SA) psi and sets NStates to the correct value if compatible.
	 */
	protected void checkSetNStates() {
		boolean set = false;
		if (!isLambdaFixed() && lambdas.getDimension() != nStates) {
			nStates = lambdas.getDimension();
			set = true;
		}
		if (!isMuFixed() && mus.getDimension() != nStates) {
			if (set) throw new IllegalStateException("Value of NStates is inconsistent with lambdas or mus");
			nStates = mus.getDimension();
			set = true;
		}
		if (isSA() && !isPsiFixed() && psis.getDimension() != nStates) {
			if (set) throw new IllegalStateException("Value of NStates is inconsistent with some of lambdas or mus or psis");
			else nStates = psis.getDimension();
		}
	}

	/**
	 * Sets the mu for state i.
	 *
	 * @param i the state
	 * @param mu the new mu
	 */
	public void setMu(int i, double mu) {
		if (isMuFixed()) mus.setValue(mu);
		else mus.setValue(i, mu);
	}

	/**
	 * Sets the mus.
	 *
	 * @param newvalues the new mus
	 */
	public void setMus(Double[] newvalues) {
		if (isMuFixed()) {
			if (newvalues.length == 1) mus.setValue(newvalues[0]);
			else throw new UnsupportedOperationException("Cannot assign multiple values to fixed mu");
		} else {
			mus.setValue(0, newvalues[0]); // this is a hack -- setDimension does not set the hasStartedEditing flag but
											// this does, preserving the ACTUAL original state
			int n = newvalues.length;
			mus.setDimension(n);
			for (int i = 1; i < n; i++) {
				mus.setValue(i, newvalues[i]);
			}
		}
	}

	/**
	 * Gets the mu for state i.
	 *
	 * @param i the state
	 * @return the mu
	 */
	public double getMu(int i) {
		if (isMuFixed()) return mus.getValue();
		return mus.getValue(i);
	}

	/**
	 * Gets the mus.
	 *
	 * @return the mus
	 */
	public Double[] getMus() {
		return mus.getValues();
	}

	/*
	 * CalculationNode implementations.
	 */

	/* (non-Javadoc)
	 * @see beast.base.inference.CalculationNode#requiresRecalculation()
	 */
	@Override
	protected boolean requiresRecalculation() {
		return true;
	}

	/* (non-Javadoc)
	 * @see beast.base.inference.CalculationNode#restore()
	 */
	@Override
	protected void restore() {
		super.restore();
		checkSetNStates();
	}

	/**
	 * Gets the true number of states.
	 *
	 * @return nstar
	 */
	public int getNstar() {
		return (nstar.getValue());
	}

	/**
	 * Removes the state and associated parameters.
	 *
	 * @param i the index of the state to remove
	 */
	public void removeState(int i) {
		int n = getNStates();
		if (!isLambdaFixed()) {
			Double[] newvaluesl = new Double[n - 1];
			Double[] oldl = getLambdas();
			if (i > 0) {
				System.arraycopy(oldl, 0, newvaluesl, 0, i);
			}
			if (i < n - 1) {
				System.arraycopy(oldl, i + 1, newvaluesl, i, n - 1 - i);
			}
			setLambdas(newvaluesl);
		}

		if (!isMuFixed()) {
			Double[] newvaluesm = new Double[n - 1];
			Double[] oldm = getMus();
			if (i > 0) {
				System.arraycopy(oldm, 0, newvaluesm, 0, i);
			}
			if (i < n - 1) {
				System.arraycopy(oldm, i + 1, newvaluesm, i, n - 1 - i);
			}
			setMus(newvaluesm);
		}
		if(isSA() && !isPsiFixed()) {
			Double[] newvaluesp = new Double[n - 1];
			Double[] oldp = getPsis();
			if (i > 0) {
				System.arraycopy(oldp, 0, newvaluesp, 0, i);
			}
			if (i < n - 1) {
				System.arraycopy(oldp, i + 1, newvaluesp, i, n - 1 - i);
			}
			setPsis(newvaluesp);
		}
		setNStates(n - 1);
	}

	/**
	 * Adds a new state with associated parameters.
	 *
	 * @param newLambda the new lambda
	 * @param newMu the new mu
	 */
	public void addState(double newLambda, double newMu) {
		int n = getNStates();
		if (!isLambdaFixed()) {
			Double[] newvaluesl = new Double[n + 1];
			Double[] oldl = getLambdas();
			System.arraycopy(oldl, 0, newvaluesl, 0, n);
			newvaluesl[n] = newLambda;
			setLambdas(newvaluesl);
		}

		if (!isMuFixed()) {
			Double[] newvaluesm = new Double[n + 1];
			Double[] oldm = getMus();
			System.arraycopy(oldm, 0, newvaluesm, 0, n);
			newvaluesm[n] = newMu;
			setMus(newvaluesm);
		}
		
		if(!isSA()) setNStates(n + 1); //if SA this will be done by calling code
	}
	
	/**
	 * Adds a new state with associated parameters.
	 *
	 * @param newLambda the new lambda
	 * @param newMu the new mu
	 * @param newPsi the new psi
	 */
	public void addState(double newLambda, double newMu, double newPsi) {
		int n = getNStates();
		addState(newLambda, newMu);
				
		if (isSA() && !isPsiFixed()) {
			Double[] newvaluesp = new Double[n + 1];
			Double[] oldp = getPsis();
			System.arraycopy(oldp, 0, newvaluesp, 0, n);
			newvaluesp[n] = newPsi;
			setPsis(newvaluesp);
		}
		setNStates(n + 1);
	}

	/**
	 * Checks if lambda is constant across the tree.
	 *
	 * @return true, if constant
	 */
	public boolean isLambdaFixed() {
		return isLambdaFixed.get();
	}

	/**
	 * Checks if mu is  constant across the tree.
	 *
	 * @return true, if constant
	 */
	public boolean isMuFixed() {
		return isMuFixed.get();
	}

	/**
	 * Gets rho (extant sampling proportion).
	 *
	 * @return rho
	 */
	public double getRho() {
		return rho.getValue();
	}

	/**
	 * Gets sigma (extinct sampling probability upon extinction/recovery) 
	 * if the model is NOT configured for an SA tree, 0 otherwise.
	 *
	 * @return sigma
	 */
	public double getSigma() {
		if(isSA()) return 0.0;
		else return sigma.getValue();
	}

	/**
	 * Is the model configured for an SA tree, i.e. has a SA sampling rate ?
	 * 
	 * @return true if configured for SA
	 */
	public boolean isSA() {
		return isSAInput.get();
	}

	/**
	 * Gets the sampling rate (fossil/past) for state i
	 * if the model is configured for an SA tree, 0 otherwise.
	 *
	 * @param i the state index
	 * @return psi or 0
	 */
	public double getPsi(int i) {
		if(!isSA()) return 0.0;
		if(isPsiFixed()) return psis.getValue();
		return psis.getValue(i); 
	}

	/**
	 * Checks if is psi is constant across the tree..
	 *
	 * @return true, if constant
	 */
	public boolean isPsiFixed() {
		return isPsiFixed.get();
	}
	
	/**
	 * Sets the psis.
	 *
	 * @param newvalues the new psis
	 */
	private void setPsis(Double[] newvalues) {
		if(!isSA()) throw new UnsupportedOperationException("Cannot set psis on non-SA tree");
		if (isPsiFixed()) {
			if (newvalues.length == 1) psis.setValue(newvalues[0]);
			else throw new UnsupportedOperationException("Cannot assign multiple values to fixed psi");
		} else {
			psis.setValue(0, newvalues[0]); // this is a hack -- setDimension does not set the hasStartedEditing flag but
											// this does, preserving the ACTUAL original state
			int n = newvalues.length;
			psis.setDimension(n);
			for (int i = 1; i < n; i++) {
				psis.setValue(i, newvalues[i]);
			}
		}
	}

	/**
	 * Gets the psis.
	 *
	 * @return the psis
	 */
	private Double[] getPsis() {
		return psis.getValues();
	}

}
