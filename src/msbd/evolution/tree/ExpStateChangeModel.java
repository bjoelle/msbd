/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.evolution.tree;

import beast.base.core.Input;
import msbd.base.inference.parameter.ExtendableRealParameter;

/**
 * State change model with a time-dependent exponential decay on lambda and/or mu.
 */
public class ExpStateChangeModel extends StateChangeModel {

	public Input<ExtendableRealParameter> lambdaRatesInput = new Input<>("lambdaRates", "Birth exponential decay rates (positive number), no decay for lambda if left blank");
	public Input<ExtendableRealParameter> muRatesInput = new Input<>("muRates", "Death exponential decay rates (positive number), no decay for mu if left blank");

	public Input<Boolean> isLambdaRateFixed = new Input<>("isLambdaRateFixed", "Whether to use a constant rate for lambda for all states, default false", false);
	public Input<Boolean> isMuRateFixed = new Input<>("isMuRateFixed", "Whether to use a constant rate for mu for all states, default false", false);

	public Input<Boolean> isLambdaDecay = new Input<>("isLambdaDecay", "Whether to use exponential decay on birth rates", true);
	public Input<Boolean> isMuDecay = new Input<>("isMuDecay", "Whether to use exponential decay on death rates", true);

	private ExtendableRealParameter lambdaRates, muRates;
	private boolean lbdaRatefix, muRatefix;
	private boolean lbdaDecay = true, muDecay = true;

	/**
	 * Instantiates a new exponential state change model.
	 */
	public ExpStateChangeModel() {}

	/**
	 * {@inheritDoc}
	 * <p>
	 * Will print a warning if no decay is activated, but keep going.
	 */
	@Override
	public void initAndValidate() {
		lbdaRatefix = isLambdaRateFixed.get();
		muRatefix = isMuRateFixed.get();

		if (lambdaRatesInput.get() == null || !isLambdaDecay.get()) {
			if (isLambdaDecay.get()) System.out.println("No rates for lambda found, defaulting to no decay");
			lbdaRatefix = true;
			lbdaDecay = false;
		} else {
			lbdaDecay = true;
			lambdaRates = lambdaRatesInput.get();
			lambdaRates.setLower(0.0);
		}

		if (muRatesInput.get() == null || !isMuDecay.get()) {
			if (isMuDecay.get()) System.out.println("No rates for mu found, defaulting to no decay");
			muRatefix = true;
			muDecay = false;
		} else {
			muDecay = true;
			muRates = muRatesInput.get();
			muRates.setLower(0.0);
		}

		if (!muDecay && !lbdaDecay) System.err.println("Warning: no decay activated, consider using StateChangeModel instead");

		super.initAndValidate();
		checkSetNStates();
	}

	/**
	 * Sets the lambda rate for a state.
	 *
	 * @param i the index of the state
	 * @param rate the rate
	 */
	public void setLambdaRate(int i, double rate) {
		if (lbdaRatefix) lambdaRates.setValue(rate);
		else lambdaRates.setValue(i, rate);
	}

	/**
	 * Sets the lambda rates.
	 *
	 * @param newvalues the new lambda rates
	 */
	public void setLambdaRates(Double[] newvalues) {
		if (lbdaRatefix) {
			if (newvalues.length == 1) lambdaRates.setValue(newvalues[0]);
			else throw new UnsupportedOperationException("Cannot assign multiple values to fixed lambda rate");
		} else {
			lambdaRates.setValue(0, newvalues[0]); // this is a hack -- setDimension does not set the hasStartedEditing
													// flag but this does, preserving the ACTUAL original state
			int n = newvalues.length;
			lambdaRates.setDimension(n);
			for (int i = 1; i < n; i++) {
				lambdaRates.setValue(i, newvalues[i]);
			}
		}
	}

	/**
	 * Gets the lambda rate for a state.
	 *
	 * @param i the index of the state
	 * @return the lambda rate
	 */
	public double getLambdaRate(int i) {
		if (!lbdaDecay) return 0;
		if (lbdaRatefix) return lambdaRates.getValue();
		return lambdaRates.getValue(i);
	}

	/**
	 * Gets the lambda rates.
	 *
	 * @return the lambda rates
	 */
	public Double[] getLambdaRates() {
		if (!lbdaDecay) return new Double[1];
		return lambdaRates.getValues();
	}

	/**
	 * Sets the mu rate for a state.
	 *
	 * @param i the index of the state
	 * @param rate the rate
	 */
	public void setMuRate(int i, double rate) {
		if (muRatefix) muRates.setValue(rate);
		else muRates.setValue(i, rate);
	}

	/**
	 * Sets the mu rates.
	 *
	 * @param newvalues the new mu rates
	 */
	public void setMuRates(Double[] newvalues) {
		if (muRatefix) {
			if (newvalues.length == 1) muRates.setValue(newvalues[0]);
			else throw new UnsupportedOperationException("Cannot assign multiple values to fixed mu rate");
		} else {
			muRates.setValue(0, newvalues[0]); // this is a hack -- setDimension does not set the hasStartedEditing flag
												// but this does, preserving the ACTUAL original state
			int n = newvalues.length;
			muRates.setDimension(n);
			for (int i = 1; i < n; i++) {
				muRates.setValue(i, newvalues[i]);
			}
		}
	}

	/**
	 * Gets the mu rate for a state.
	 *
	 * @param i the index of the state
	 * @return the mu rate
	 */
	public double getMuRate(int i) {
		if (!muDecay) return 0;
		if (muRatefix) return muRates.getValue();
		return muRates.getValue(i);
	}

	/**
	 * Gets the mu rates.
	 *
	 * @return the mu rates
	 */
	public Double[] getMuRates() {
		if (!muDecay) return new Double[1];
		return muRates.getValues();
	}

	/**
	 * Removes the specified state.
	 *
	 * @param i the state index
	 */
	@Override
	public void removeState(int i) {
		int n = getNStates();
		super.removeState(i);

		if (!lbdaRatefix) {
			Double[] newvalueslr = new Double[n - 1];
			Double[] oldlr = this.getLambdaRates();
			if (i > 0) {
				System.arraycopy(oldlr, 0, newvalueslr, 0, i);
			}
			if (i < n - 1) {
				System.arraycopy(oldlr, i + 1, newvalueslr, i, n - 1 - i);
			}
			setLambdaRates(newvalueslr);
		}

		if (!muRatefix) {
			Double[] newvaluesmr = new Double[n - 1];
			Double[] oldmr = this.getMuRates();
			if (i > 0) {
				System.arraycopy(oldmr, 0, newvaluesmr, 0, i);
			}
			if (i < n - 1) {
				System.arraycopy(oldmr, i + 1, newvaluesmr, i, n - 1 - i);
			}
			setMuRates(newvaluesmr);
		}
		setNStates(n - 1);
	}

	/**
	 * Adds a new state with associated parameters.
	 *
	 * @param newLambda the new lambda
	 * @param newMu the new mu
	 * @param newLR the new lambda decay rate
	 * @param newMR the new mu decay rate
	 */
	public void addState(double newLambda, double newMu, double newLR, double newMR) {
		int n = getNStates();
		super.addState(newLambda, newMu);

		if (!lbdaRatefix) {
			Double[] newvalueslr = new Double[n + 1];
			Double[] oldlr = this.getLambdaRates();
			System.arraycopy(oldlr, 0, newvalueslr, 0, n);
			newvalueslr[n] = newLR;
			setLambdaRates(newvalueslr);
		}

		if (!muRatefix) {
			Double[] newvaluesmr = new Double[n + 1];
			Double[] oldmr = this.getMuRates();
			System.arraycopy(oldmr, 0, newvaluesmr, 0, n);
			newvaluesmr[n] = newMR;
			setMuRates(newvaluesmr);
		}

		setNStates(n + 1);
	}
	
	/**
	 * Adds a new state with associated parameters.
	 *
	 * @param newLambda the new lambda
	 * @param newMu the new mu
	 * @param newPsi the new psi
	 * @param newLR the new lambda decay rate
	 * @param newMR the new mu decay rate
	 */
	public void addState(double newLambda, double newMu, double newPsi, double newLR, double newMR) {
		int n = getNStates();
		super.addState(newLambda, newMu, newPsi);

		if (!lbdaRatefix) {
			Double[] newvalueslr = new Double[n + 1];
			Double[] oldlr = this.getLambdaRates();
			System.arraycopy(oldlr, 0, newvalueslr, 0, n);
			newvalueslr[n] = newLR;
			setLambdaRates(newvalueslr);
		}

		if (!muRatefix) {
			Double[] newvaluesmr = new Double[n + 1];
			Double[] oldmr = this.getMuRates();
			System.arraycopy(oldmr, 0, newvaluesmr, 0, n);
			newvaluesmr[n] = newMR;
			setMuRates(newvaluesmr);
		}

		setNStates(n + 1);
	}

	/**
	 * Checks the compatibility of the dimensions of lambda rates and mu rates and sets NStates to the correct value if
	 * compatible.
	 *
	 * @see msbd.evolution.tree.StateChangeModel#checkSetNStates()
	 */
	@Override
	protected void checkSetNStates() {
		int save = nStates;
		super.checkSetNStates();
		boolean set = (nStates != save); // has the super function modified sth already
		if (!lbdaRatefix && lambdaRates.getDimension() != nStates) {
			if (set) throw new IllegalStateException("Value of NStates is inconsistent with lambda rates");
			else {
				nStates = lambdaRates.getDimension();
				set = true;
			}
		}
		if (!muRatefix && muRates.getDimension() != nStates) {
			if (set) throw new IllegalStateException("Value of NStates is inconsistent with lambda rates or mu rates or both");
			else nStates = muRates.getDimension();
		}
	}

	/**
	 * Checks if the lambda decay rate is constant across the tree.
	 *
	 * @return true, if constant
	 */
	public boolean isLambdaRateFixed() {
		return lbdaRatefix;
	}

	/**
	 * Checks if the mu decay rate is constant across the tree.
	 *
	 * @return true, if constant
	 */
	public boolean isMuRateFixed() {
		return muRatefix;
	}

	/**
	 * Checks if decay is active for lambda.
	 *
	 * @return true, if decay is active
	 */
	public boolean isLambdaDecay() {
		return lbdaDecay;
	}

	/**
	 * Checks if decay is active for mu.
	 *
	 * @return true, if decay is active
	 */
	public boolean isMuDecay() {
		return muDecay;
	}

}
