/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.app.beauti;

import beastfx.app.inputeditor.BeautiDoc;
import msbd.base.evolution.tree.MultiRateTree;
import msbd.base.evolution.tree.MultiRateTreeFromBDModel;
import msbd.distributions.ExpBirthDeathDensity;
import msbd.distributions.MultiRateTreeDistribution;
import msbd.distributions.StdBirthDeathDensity;
import msbd.evolution.tree.StateChangeModel;
import beast.base.core.BEASTInterface;
import beast.base.evolution.likelihood.GenericTreeLikelihood;

/**
 * This connector avoids a problem with circular linkage scModel->initial tree->tree which led to issues with
 * storing/restoring the tree state.
 */
public class InitModelConnector {
	
	// this will be modified by the MRTDistributionInputEditor if we switch models
	public static String latest = "stdBDMRT";

	/**
	 * Custom connector.
	 *
	 * @param doc the doc
	 * @return true, if successful
	 */
	public static boolean customConnector(BeautiDoc doc) {
		for (BEASTInterface p : doc.getPartitions("Tree")) {
			GenericTreeLikelihood treeLikelihood = (GenericTreeLikelihood) p;
			MultiRateTree tree = (MultiRateTree) treeLikelihood.treeInput.get();

			if (!(tree.m_initial.get() instanceof MultiRateTreeFromBDModel)) return true;
			
			String pID = BeautiDoc.parsePartition(tree.getID());
			StateChangeModel initModel;
			MultiRateTreeDistribution distr = (MultiRateTreeDistribution) doc.pluginmap.get(latest + ".t:" + pID);
			if (distr instanceof StdBirthDeathDensity) initModel = ((StdBirthDeathDensity) distr).stateChangeModelInput.get();
			else initModel = ((ExpBirthDeathDensity) distr).stateChangeModelInput.get();

			tree.m_initial.get().setInputValue("lambda", initModel.getLambda(0));
		}

		return true;
	}
}
