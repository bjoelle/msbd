/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.app.beauti;

import java.util.ArrayList;
import java.util.List;

import beastfx.app.inputeditor.BeautiDoc;
import beastfx.app.inputeditor.InputEditor;
import beastfx.app.inputeditor.ParameterInputEditor;
import beastfx.app.util.FXUtils;
import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import msbd.evolution.tree.StateChangeModel;
import beast.base.core.BEASTInterface;
import beast.base.core.Input;
import beast.base.core.Log;

/**
 * Shows and allows editing of a StateChangeModel in Beauti.
 */
public class StateChangeModelInputEditor extends InputEditor.Base {

	List<TextField> lambdasList, musList, psisList;
	GridPane lGrid, mGrid, pGrid;
	StateChangeModel scModel;
	Spinner<Integer> nStatesSpinner;

	CheckBox lambdaEstCheckBox, muEstCheckBox, nStatesEstCheckBox, lambdaFixedCheckBox, muFixedCheckBox;
	CheckBox isSACheckBox, psiEstCheckBox, psiFixedCheckBox;
	InputEditor.Base gammaInputEditor, rhoInputEditor, sigmaInputEditor, nstarInputEditor;	

	/**
	 * Instantiates a new state change model input editor.
	 *
	 * @param doc the beauti doc
	 */
	public StateChangeModelInputEditor(BeautiDoc doc) {
		super(doc);
		gammaInputEditor = new ParameterInputEditor(doc);
		rhoInputEditor = new ParameterInputEditor(doc);
		sigmaInputEditor = new ParameterInputEditor(doc);
		nstarInputEditor = new ParameterInputEditor(doc);
	}

	/**
	 * @return the class of objects that can be edited by this input editor
	 */
	@Override
	public Class<?> type() {
		return StateChangeModel.class;
	}

	/**
	 * Inits the editor.
	 *
	 * @param input the input (state change model)
	 * @param plugin the plugin (parent object)
	 * @param itemNr the item nr
	 * @param bExpandOption the expand option
	 * @param bAddButtons the add buttons option
	 */
	@Override
	public void init(Input<?> input, BEASTInterface beastObject, int itemNr, ExpandOption isExpandOption, boolean addButtons) {
		// Set up fields
		m_bAddButtons = addButtons;
		m_input = input;
		m_beastObject = beastObject;
		this.itemNr = itemNr;

		pane = FXUtils.newVBox();

		// Adds label to left of input editor
		addInputLabel();

		initCommonFields();

		loadFromStateChangeModel();

		addGammaNSTField(isExpandOption, addButtons);

		addLMPFields();

		addEndFields(isExpandOption, addButtons);

		// Event handlers
		nStatesSpinner.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (!scModel.isLambdaFixed.get()) {
				changeGridDimension(lGrid, lambdasList, newValue);
			}

			if (!scModel.isMuFixed.get()) {
				changeGridDimension(mGrid, musList, newValue);
			}

			if (!scModel.isPsiFixed.get()) {
				changeGridDimension(pGrid, psisList, newValue);
			}

			saveToStateChangeModel();
		});

		getChildren().add(pane);

		addEventListeners();
		addValidationLabel();
	}

	void initCommonFields() {
		// Create component models and fill them with data from input
		scModel = (StateChangeModel) m_input.get();

		nStatesSpinner = new Spinner<Integer>(1, Short.MAX_VALUE, 2, 1);

		lambdaEstCheckBox = setUpCheckBox(scModel.lambdasInput.get().isEstimatedInput, null);
		muEstCheckBox = setUpCheckBox(scModel.musInput.get().isEstimatedInput, null);
		psiEstCheckBox = setUpCheckBox(scModel.psisInput.get().isEstimatedInput, null);
		nStatesEstCheckBox = setUpCheckBox(scModel.estimateNStatesInput, "estimate");

		lambdaFixedCheckBox = setUpCheckBox(scModel.isLambdaFixed, "identical across states");
		muFixedCheckBox = setUpCheckBox(scModel.isMuFixed, "identical across states");
		psiFixedCheckBox = setUpCheckBox(scModel.isPsiFixed, "identical across states");

		isSACheckBox = setUpCheckBox(scModel.isSAInput, "Use Sampled Ancestors");

		lambdasList = new ArrayList<TextField>();
		lGrid = new GridPane();
		changeGridDimension(lGrid, lambdasList, 2);

		musList = new ArrayList<TextField>();
		mGrid = new GridPane();
		changeGridDimension(mGrid, musList, 2);

		psisList = new ArrayList<TextField>();
		pGrid = new GridPane();
		changeGridDimension(pGrid, psisList, 2);
	}

	/**
	 * Load data from the state change model.
	 */
	public void loadFromStateChangeModel() {
		int n = scModel.getNStates();
		nStatesSpinner.getValueFactory().setValue(n);

		int dim = scModel.isLambdaFixed.get() ? 1 : n;
		changeGridDimension(lGrid, lambdasList, n);
		for (int i = 0; i < dim; i++) lambdasList.get(i).setText(Double.toString(scModel.getLambda(i)));

		dim = scModel.isMuFixed.get() ? 1 : n;
		changeGridDimension(mGrid, musList, n);
		for (int i = 0; i < dim; i++) musList.get(i).setText(Double.toString(scModel.getMu(i)));

		dim = scModel.isPsiFixed.get() ? 1 : n;
		changeGridDimension(pGrid, psisList, n);
		for (int i = 0; i < dim; i++) psisList.get(i).setText(Double.toString(scModel.getPsi(i)));

		lambdaEstCheckBox.setSelected(scModel.lambdasInput.get().isEstimatedInput.get());
		muEstCheckBox.setSelected(scModel.musInput.get().isEstimatedInput.get());
		lambdaFixedCheckBox.setSelected(scModel.isLambdaFixed.get());
		muFixedCheckBox.setSelected(scModel.isMuFixed.get());
		psiEstCheckBox.setSelected(scModel.psisInput.get().isEstimatedInput.get());
		psiFixedCheckBox.setSelected(scModel.isPsiFixed.get());

		isSACheckBox.setSelected(scModel.isSA());
		nStatesEstCheckBox.setSelected(scModel.estimateNStatesInput.get());
	}

	/**
	 * Save data to the rate shift model.
	 */
	public void saveToStateChangeModel() {
		scModel.isLambdaFixed.setValue(lambdaFixedCheckBox.isSelected(), scModel);
		scModel.isMuFixed.setValue(muFixedCheckBox.isSelected(), scModel);

		StringBuilder sblambdas = new StringBuilder();
		int lc = (scModel.isLambdaFixed.get()) ? 1 : lGrid.getColumnCount();
		for (int i = 0; i < lc; i++) {
			if (i > 0) sblambdas.append(" ");
			if (i < lambdasList.size()) sblambdas.append(Double.parseDouble(lambdasList.get(i).getText()));
			else sblambdas.append("0.0");
		}
		scModel.lambdasInput.get().setDimension(lc);
		scModel.lambdasInput.get().valuesInput.setValue(sblambdas.toString(), scModel.lambdasInput.get());
		scModel.lambdasInput.get().isEstimatedInput.setValue(lambdaEstCheckBox.isSelected(), scModel.lambdasInput.get());

		int mc = (scModel.isMuFixed.get()) ? 1 : mGrid.getColumnCount();
		StringBuilder sbmus = new StringBuilder();
		for (int i = 0; i < mc; i++) {
			if (i > 0) sbmus.append(" ");
			if (i < musList.size()) sbmus.append(Double.parseDouble(musList.get(i).getText()));
			else sbmus.append("0.0");
		}
		scModel.musInput.get().setDimension(mc);
		scModel.musInput.get().valuesInput.setValue(sbmus.toString(), scModel.musInput.get());
		scModel.musInput.get().isEstimatedInput.setValue(muEstCheckBox.isSelected(), scModel.musInput.get());

		scModel.isSAInput.setValue(isSACheckBox.isSelected(), scModel);
		scModel.isPsiFixed.setValue(psiFixedCheckBox.isSelected(), scModel);

		StringBuilder sbpsis = new StringBuilder();
		int pc = (scModel.isPsiFixed.get()) ? 1 : pGrid.getColumnCount();
		for (int i = 0; i < pc; i++) {
			if (i > 0) sbpsis.append(" ");
			if (i < psisList.size()) sbpsis.append(Double.parseDouble(psisList.get(i).getText()));
			else sbpsis.append("0.0");
		}
		scModel.psisInput.get().setDimension(pc);
		scModel.psisInput.get().valuesInput.setValue(sbpsis.toString(), scModel.psisInput.get());
		scModel.psisInput.get().isEstimatedInput.setValue(psiEstCheckBox.isSelected(), scModel.psisInput.get());

		validateInput();
		refreshPanel();
	}

	/**
	 * Adds the input label.
	 *
	 * @param horBox the box where to add
	 * @param input the input
	 * @return the updated box
	 */
	protected HBox addInputLabel(HBox horBox, Input<?> input) {
		if (m_bAddButtons) {
			String sName = formatName(input.getName());
			String sTipText = input.getHTMLTipText();
			Label inputLabel = setUpLabel(sName, sTipText);
			horBox.getChildren().add(inputLabel);
		}
		return horBox;
	}

	/**
	 * Adds the gamma and nstates fields.
	 *
	 * @param bExpandOption the expand option
	 * @param bAddButtons the add buttons option
	 */
	protected void addGammaNSTField(ExpandOption bExpandOption, boolean bAddButtons) {
		gammaInputEditor.init(scModel.gammaInput, scModel, itemNr, bExpandOption, bAddButtons);
		pane.getChildren().add(gammaInputEditor);
		gammaInputEditor.addValidationListener(this);

		// nstates
		HBox horBox = FXUtils.newHBox();

		Label tmp = setUpLabel("Number of states", "Number of different evolutionary regimes or rates");
		horBox.getChildren().add(tmp);

		nStatesSpinner.setMaxSize(Short.MAX_VALUE, 100);
		horBox.getChildren().add(nStatesSpinner);

		nStatesEstCheckBox.setSelected(scModel.estimateNStatesInput.get());
		horBox.getChildren().add(nStatesEstCheckBox);
		pane.getChildren().add(horBox);
	}

	/**
	 * Adds the end fields (rho, sigma, nstar).
	 *
	 * @param bExpandOption the expand option
	 * @param bAddButtons the b add buttons option
	 */
	protected void addEndFields(ExpandOption bExpandOption, boolean bAddButtons) {
		rhoInputEditor.init(scModel.rhoInput, scModel, itemNr, bExpandOption, bAddButtons);
		rhoInputEditor.addValidationListener(this);
		pane.getChildren().add(rhoInputEditor);
		if(!scModel.isSA()) {
			sigmaInputEditor.init(scModel.sigmaInput, scModel, itemNr, bExpandOption, bAddButtons);
			sigmaInputEditor.addValidationListener(this);
			pane.getChildren().add(sigmaInputEditor);
		}
		nstarInputEditor.init(scModel.nstarInput, scModel, itemNr, bExpandOption, bAddButtons);
		pane.getChildren().add(nstarInputEditor);
		nstarInputEditor.addValidationListener(this);

		pane.getChildren().add(isSACheckBox);
	}

	/**
	 * Adds the lambda, mu and psi fields.
	 */
	protected void addLMPFields() {
		// Lambdas table
		HBox horBox = FXUtils.newHBox();
		horBox = addInputLabel(horBox, scModel.lambdasInput);
		horBox.getChildren().add(lGrid);
		lambdaEstCheckBox.setSelected(scModel.lambdasInput.get().isEstimatedInput.get());
		horBox.getChildren().add(lambdaEstCheckBox);
		pane.getChildren().add(horBox);

		horBox = FXUtils.newHBox();
		lambdaFixedCheckBox.setSelected(scModel.isLambdaFixed.get());
		horBox.getChildren().add(lambdaFixedCheckBox);
		pane.getChildren().add(horBox);

		// Mus table
		horBox = FXUtils.newHBox();
		horBox = addInputLabel(horBox, scModel.musInput);
		horBox.getChildren().add(mGrid);
		muEstCheckBox.setSelected(scModel.musInput.get().isEstimatedInput.get());
		horBox.getChildren().add(muEstCheckBox);
		pane.getChildren().add(horBox);

		horBox = FXUtils.newHBox();
		muFixedCheckBox.setSelected(scModel.isMuFixed.get());
		horBox.getChildren().add(muFixedCheckBox);
		pane.getChildren().add(horBox);

		// Psis table
		if(scModel.isSA()) {
			horBox = FXUtils.newHBox();
			horBox = addInputLabel(horBox, scModel.psisInput);
			horBox.getChildren().add(pGrid);
			psiEstCheckBox.setSelected(scModel.psisInput.get().isEstimatedInput.get());
			horBox.getChildren().add(psiEstCheckBox);
			pane.getChildren().add(horBox);

			horBox = FXUtils.newHBox();
			psiFixedCheckBox.setSelected(scModel.isPsiFixed.get());
			horBox.getChildren().add(psiFixedCheckBox);
			pane.getChildren().add(horBox);
		}
	}

	/**
	 * Adds the event listeners.
	 */
	protected void addEventListeners() {
		lambdaEstCheckBox.setOnAction(e -> {
			saveToStateChangeModel();
		});

		lambdaFixedCheckBox.setOnAction(e -> {
			int dim = lambdaFixedCheckBox.isSelected() ? 1 : nStatesSpinner.getValue();
			changeGridDimension(lGrid, lambdasList, dim);
			saveToStateChangeModel();
		});

		muEstCheckBox.setOnAction(e -> {
			saveToStateChangeModel();
		});

		muFixedCheckBox.setOnAction(e -> {
			int dim = muFixedCheckBox.isSelected() ? 1 : nStatesSpinner.getValue();
			changeGridDimension(mGrid, musList, dim);
			saveToStateChangeModel();
		});

		psiEstCheckBox.setOnAction(e -> {
			saveToStateChangeModel();
		});

		psiFixedCheckBox.setOnAction(e -> {
			int dim = psiFixedCheckBox.isSelected() ? 1 : nStatesSpinner.getValue();
			changeGridDimension(pGrid, psisList, dim);
			saveToStateChangeModel();
		});

		nStatesEstCheckBox.setOnAction(e -> {
			scModel.estimateNStatesInput.setValue(nStatesEstCheckBox.isSelected(), scModel);
			sync();
			refreshPanel();
		});

		isSACheckBox.setOnAction(e -> {
			scModel.isSAInput.setValue(isSACheckBox.isSelected(), scModel);
			scModel.initAndValidate();
			sync();
			refreshPanel();
		});
	}

	/**
	 * Validate input.
	 */
	@Override
	public void validateInput() {
		try {
			scModel.musInput.get().initAndValidate();
			scModel.lambdasInput.get().initAndValidate();
			if(scModel.isSA()) scModel.psisInput.get().initAndValidate();
			scModel.initAndValidate();
			if (m_validateLabel != null) {
				m_validateLabel.setVisible(false);
			}
		} catch (Exception e) {
			Log.err.println("Validation message: " + e.getMessage());
			if (m_validateLabel != null) {
				m_validateLabel.setTooltip(e.getMessage());
				m_validateLabel.setColor("red");
				m_validateLabel.setVisible(true);
			}
			notifyValidationListeners(ValidationStatus.IS_INVALID);
		}
		repaint();
	}

	CheckBox setUpCheckBox(Input<Boolean> chInput, String label) {
		CheckBox chBox;
		if(label == null) chBox = new CheckBox(doc.beautiConfig.getInputLabel(scModel, chInput.getName()));
		else chBox = new CheckBox(label);
		chBox.setId(m_input.getName() + ".isEstimated");
		chBox.setSelected(chInput.get());
		chBox.setTooltip(new Tooltip(chInput.getTipText()));
		return chBox;
	}

	Label setUpLabel(String name, String tipText) {
		Label label = new Label(name);
		label.setTooltip(new Tooltip(tipText));
		label.setTextAlignment(TextAlignment.RIGHT);
		label.setPrefSize(LABEL_SIZE.getWidth(), LABEL_SIZE.getHeight());            
		label.setPadding(new Insets(5,0,0,0));
		return label;
	}

	void changeGridDimension(GridPane grid, List<TextField> fields, int newDim) {
		if(grid != null && grid.getColumnCount() == newDim) return;

		grid.getChildren().clear();
		for(int i = 0; i < newDim; i++) {
			if(i < fields.size()) grid.add(fields.get(i), i, 0);
			else {
				TextField tf = new TextField("0.0");
				tf.setOnKeyReleased(e -> {
					saveToStateChangeModel();
				});
				tf.setMaxWidth(MAX_SIZE.getWidth());
				tf.setPrefWidth(PREFERRED_SIZE.getWidth());

				fields.add(tf);
				grid.add(tf, i , 0);
			}
		}
	}
}
