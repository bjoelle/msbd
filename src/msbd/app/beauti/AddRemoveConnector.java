/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.app.beauti;

import beastfx.app.inputeditor.BeautiDoc;
import msbd.base.evolution.tree.MultiRateTree;
import msbd.evolution.operator.AddRemoveColourWPropagation;
import beast.base.core.BEASTInterface;
import beast.base.evolution.likelihood.GenericTreeLikelihood;
import beast.base.inference.distribution.Prior;

/**
 * Custom connector for inputting prior distributions to the AddREmoveColour operator.
 */
public class AddRemoveConnector {
	// set here for cross reference from other input connectors
	public final static String lambdaID = "l0";
	public final static String lambdaRID = "lr0";
	public final static String muID = "m0";
	public final static String muRID = "mr0";
	public final static String psiID = "p0";

	/**
	 * Custom connector.
	 *
	 * @param doc the beauti doc
	 * @return true, if successful
	 */
	public static boolean customConnector(BeautiDoc doc) {

		for (BEASTInterface p : doc.getPartitions("Tree")) {
			GenericTreeLikelihood treeLikelihood = (GenericTreeLikelihood) p;
			MultiRateTree tree = (MultiRateTree) treeLikelihood.treeInput.get();

			String pID = BeautiDoc.parsePartition(tree.getID());

			Prior lambdaP = (Prior) doc.pluginmap.get("lambdasPrior.t:" + pID);
			Prior muP = (Prior) doc.pluginmap.get("musPrior.t:" + pID);
			Prior lambdaRP = (Prior) doc.pluginmap.get("lambdaRatesPrior.t:" + pID);
			Prior muRP = (Prior) doc.pluginmap.get("muRatesPrior.t:" + pID);
			Prior stdpsiP = (Prior) doc.pluginmap.get("stdpsisPrior.t:" + pID);
			Prior exppsiP = (Prior) doc.pluginmap.get("exppsisPrior.t:" + pID);

			// this is a hack (because of course it is) to get around the renumbering of things with identical ids that
			// is done when saving to xml and will break the connection
			// this works because as far as I can tell the renumbering will never use letters but the rest of the thing
			// doesn't care
			if (lambdaP != null) lambdaP.distInput.get().setID(lambdaP.distInput.get().getID().substring(0, lambdaP.distInput.get().getID().indexOf('.')) + "." + lambdaID);
			if (lambdaRP != null) lambdaRP.distInput.get().setID(lambdaRP.distInput.get().getID().substring(0, lambdaRP.distInput.get().getID().indexOf('.')) + "." + lambdaRID);
			if (muP != null) muP.distInput.get().setID(muP.distInput.get().getID().substring(0, muP.distInput.get().getID().indexOf('.')) + "." + muID);
			if (muRP != null) muRP.distInput.get().setID(muRP.distInput.get().getID().substring(0, muRP.distInput.get().getID().indexOf('.')) + "." + muRID);
			if (stdpsiP != null) stdpsiP.distInput.get().setID(stdpsiP.distInput.get().getID().substring(0, stdpsiP.distInput.get().getID().indexOf('.')) + "." + psiID);
			if (exppsiP != null) exppsiP.distInput.get().setID(exppsiP.distInput.get().getID().substring(0, exppsiP.distInput.get().getID().indexOf('.')) + "." + psiID);

			AddRemoveColourWPropagation operator = (AddRemoveColourWPropagation) doc.pluginmap.get("stdAddRemoveStates.t:" + pID);

			if (lambdaP != null) operator.lambdaDistributionInput.setValue(lambdaP.distInput.get(), operator);
			if (lambdaRP != null) operator.lambdaRDistributionInput.setValue(lambdaRP.distInput.get(), operator);
			if (muP != null) operator.muDistributionInput.setValue(muP.distInput.get(), operator);
			if (muRP != null) operator.muRDistributionInput.setValue(muRP.distInput.get(), operator);
			if (stdpsiP != null) operator.psiDistributionInput.setValue(stdpsiP.distInput.get(), operator);

			try {
				operator.initAndValidate();
			} catch (Exception ex) {
				System.err.println("Error configuring add/remove states operator.");
			}

			operator = (AddRemoveColourWPropagation) doc.pluginmap.get("expAddRemoveStates.t:" + pID);
			if (operator == null) continue;

			if (lambdaP != null) operator.lambdaDistributionInput.setValue(lambdaP.distInput.get(), operator);
			if (lambdaRP != null) operator.lambdaRDistributionInput.setValue(lambdaRP.distInput.get(), operator);
			if (muP != null) operator.muDistributionInput.setValue(muP.distInput.get(), operator);
			if (muRP != null) operator.muRDistributionInput.setValue(muRP.distInput.get(), operator);
			if (exppsiP != null) operator.psiDistributionInput.setValue(exppsiP.distInput.get(), operator);

			try {
				operator.initAndValidate();
			} catch (Exception ex) {
				System.err.println("Error configuring add/remove states operator.");
			}
		}

		return true;
	}
}
