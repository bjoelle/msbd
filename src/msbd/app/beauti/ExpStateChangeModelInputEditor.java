/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.app.beauti;

import java.util.ArrayList;
import java.util.List;

import beastfx.app.inputeditor.BeautiDoc;
import beastfx.app.util.FXUtils;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import msbd.evolution.tree.ExpStateChangeModel;
import beast.base.core.BEASTInterface;
import beast.base.core.Input;
import beast.base.core.Log;

/**
 * Shows and allows editing of an ExpStateChangeModel in Beauti.
 */
public class ExpStateChangeModelInputEditor extends StateChangeModelInputEditor {
	ExpStateChangeModel expscModel;
	CheckBox lambdaDecayCheckBox, muDecayCheckBox, lambdaREstCheckBox, muREstCheckBox, lambdaRFixedCheckBox, muRFixedCheckBox;
	GridPane lrGrid, mrGrid;
	List<TextField> lambdasRList, musRList;

	/**
	 * Instantiates a new exp rate shift model input editor.
	 *
	 * @param doc the beauti doc
	 */
	public ExpStateChangeModelInputEditor(BeautiDoc doc) {
		super(doc);
	}

	/**
	 * @return the class that can be edited by this input editor
	 */
	@Override
	public Class<?> type() {
		return ExpStateChangeModel.class;
	}

	/**
	 * Inits the editor.
	 *
	 * @param input the input (state change model)
	 * @param plugin the plugin (parent object)
	 * @param itemNr the item nr
	 * @param bExpandOption the expand option
	 * @param bAddButtons the add buttons option
	 */
	@Override
	public void init(Input<?> input, BEASTInterface beastObject, int itemNr, ExpandOption isExpandOption, boolean addButtons) {
		// Set up fields
		m_bAddButtons = addButtons;
		m_input = input;
		m_beastObject = beastObject;
		this.itemNr = itemNr;

		pane = FXUtils.newVBox();

		// Adds label to left of input editor
		addInputLabel();

		// Create component models and fill them with data from input
		expscModel = (ExpStateChangeModel) input.get();

		initCommonFields();

		lrGrid = new GridPane();
		lambdasRList = new ArrayList<TextField>();
		changeGridDimension(lrGrid, lambdasRList, 2);

		mrGrid = new GridPane();
		musRList = new ArrayList<TextField>();
		changeGridDimension(mrGrid, musRList, 2);

		lambdaDecayCheckBox = setUpCheckBox(expscModel.isLambdaDecay, "Activate birth rate decay");
		lambdaDecayCheckBox.setTooltip(new Tooltip("Use exponential decay on birth rates"));

		muDecayCheckBox = setUpCheckBox(expscModel.isMuDecay, "Activate death rate decay");
		muDecayCheckBox.setTooltip(new Tooltip("Use exponential decay on death rates"));

		lambdaREstCheckBox = setUpCheckBox(expscModel.lambdaRatesInput.get().isEstimatedInput, null);
		muREstCheckBox = setUpCheckBox(expscModel.muRatesInput.get().isEstimatedInput, null);
		lambdaRFixedCheckBox = setUpCheckBox(expscModel.isLambdaRateFixed, "identical across states");
		muRFixedCheckBox = setUpCheckBox(expscModel.isMuRateFixed, "identical across states");

		loadFromStateChangeModel();

		addGammaNSTField(isExpandOption, addButtons);

		addLMPFields();

		addEndFields(isExpandOption, addButtons);

		// Spinner handler
		nStatesSpinner.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (!expscModel.isLambdaFixed.get()) {
				changeGridDimension(lGrid, lambdasList, newValue);
			}

			if (!expscModel.isMuFixed.get()) {
				changeGridDimension(mGrid, musList, newValue);
			}

			if (!expscModel.isPsiFixed.get()) {
				changeGridDimension(pGrid, psisList, newValue);
			}

			if (!expscModel.isLambdaRateFixed.get()) {
				changeGridDimension(lrGrid, lambdasRList, newValue);
			}

			if (!expscModel.isMuRateFixed.get()) {
				changeGridDimension(mrGrid, musRList, newValue);
			}

			saveToStateChangeModel();
		});

		getChildren().add(pane);

		addEventListeners();
		addValidationLabel();
	}

	/**
	 * Load inputs from state change model.
	 */
	@Override
	public void loadFromStateChangeModel() {
		int n;
		try {
			n = expscModel.getNStates();
		} catch (Exception ex) { // workaround if nstates gets out of sync
			expscModel.isLambdaRateFixed.setValue(true, expscModel);
			expscModel.isMuRateFixed.setValue(true, expscModel);
			try {
				expscModel.initAndValidate();
			} catch (Exception e) {
				e.printStackTrace();
			}
			n = expscModel.getNStates();
		}

		nStatesSpinner.getValueFactory().setValue(n);

		int dim = expscModel.isLambdaFixed.get() ? 1 : n;
		changeGridDimension(lGrid, lambdasList, n);
		for (int i = 0; i < dim; i++) lambdasList.get(i).setText(Double.toString(expscModel.getLambda(i)));

		dim = expscModel.isMuFixed.get() ? 1 : n;
		changeGridDimension(mGrid, musList, n);
		for (int i = 0; i < dim; i++) musList.get(i).setText(Double.toString(expscModel.getMu(i)));

		dim = expscModel.isPsiFixed.get() ? 1 : n;
		changeGridDimension(pGrid, psisList, n);
		for (int i = 0; i < dim; i++) psisList.get(i).setText(Double.toString(expscModel.getPsi(i)));

		lambdaEstCheckBox.setSelected(expscModel.lambdasInput.get().isEstimatedInput.get());
		muEstCheckBox.setSelected(expscModel.musInput.get().isEstimatedInput.get());
		lambdaFixedCheckBox.setSelected(expscModel.isLambdaFixed.get());
		muFixedCheckBox.setSelected(expscModel.isMuFixed.get());
		psiEstCheckBox.setSelected(expscModel.psisInput.get().isEstimatedInput.get());
		psiFixedCheckBox.setSelected(expscModel.isPsiFixed.get());

		isSACheckBox.setSelected(expscModel.isSA());
		nStatesEstCheckBox.setSelected(expscModel.estimateNStatesInput.get());

		lambdaDecayCheckBox.setSelected(expscModel.isLambdaDecay.get());
		muDecayCheckBox.setSelected(expscModel.isMuDecay.get());

		if (lambdaDecayCheckBox.isSelected()) {
			dim = expscModel.isLambdaRateFixed.get() ? 1 : n;
			changeGridDimension(lrGrid, lambdasRList, n);
			for (int i = 0; i < dim; i++) lambdasRList.get(i).setText(Double.toString(expscModel.getLambdaRate(i)));
			lambdaREstCheckBox.setSelected(expscModel.lambdaRatesInput.get().isEstimatedInput.get());
			lambdaRFixedCheckBox.setSelected(expscModel.isLambdaRateFixed.get());
		}

		if (muDecayCheckBox.isSelected()) {
			dim = expscModel.isMuRateFixed.get() ? 1 : n;
			changeGridDimension(mrGrid, musRList, n);
			for (int i = 0; i < dim; i++) musRList.get(i).setText(Double.toString(expscModel.getMuRate(i)));
			muREstCheckBox.setSelected(expscModel.muRatesInput.get().isEstimatedInput.get());
			muRFixedCheckBox.setSelected(expscModel.isMuRateFixed.get());
		}
	}

	/**
	 * Save inputs to state change model.
	 */
	@Override
	public void saveToStateChangeModel() {
		expscModel.isLambdaFixed.setValue(lambdaFixedCheckBox.isSelected(), expscModel);
		expscModel.isMuFixed.setValue(muFixedCheckBox.isSelected(), expscModel);

		StringBuilder sblambdas = new StringBuilder();
		int lc = (expscModel.isLambdaFixed.get()) ? 1 : lGrid.getColumnCount();
		for (int i = 0; i < lc; i++) {
			if (i > 0) sblambdas.append(" ");
			if (i < lambdasList.size()) sblambdas.append(Double.parseDouble(lambdasList.get(i).getText()));
			else sblambdas.append("0.0");
		}
		expscModel.lambdasInput.get().setDimension(lc);
		expscModel.lambdasInput.get().valuesInput.setValue(sblambdas.toString(), expscModel.lambdasInput.get());
		expscModel.lambdasInput.get().isEstimatedInput.setValue(lambdaEstCheckBox.isSelected(), expscModel.lambdasInput.get());

		int mc = (expscModel.isMuFixed.get()) ? 1 : mGrid.getColumnCount();
		StringBuilder sbmus = new StringBuilder();
		for (int i = 0; i < mc; i++) {
			if (i > 0) sbmus.append(" ");
			if (i < musList.size()) sbmus.append(Double.parseDouble(musList.get(i).getText()));
			else sbmus.append("0.0");
		}
		expscModel.musInput.get().setDimension(mc);
		expscModel.musInput.get().valuesInput.setValue(sbmus.toString(), expscModel.musInput.get());
		expscModel.musInput.get().isEstimatedInput.setValue(muEstCheckBox.isSelected(), expscModel.musInput.get());

		expscModel.isSAInput.setValue(isSACheckBox.isSelected(), expscModel);
		expscModel.isPsiFixed.setValue(psiFixedCheckBox.isSelected(), expscModel);

		StringBuilder sbpsis = new StringBuilder();
		int pc = (expscModel.isPsiFixed.get()) ? 1 : pGrid.getColumnCount();
		for (int i = 0; i < pc; i++) {
			if (i > 0) sbpsis.append(" ");
			if (i < psisList.size()) sbpsis.append(Double.parseDouble(psisList.get(i).getText()));
			else sbpsis.append("0.0");
		}
		expscModel.psisInput.get().setDimension(pc);
		expscModel.psisInput.get().valuesInput.setValue(sbpsis.toString(), expscModel.psisInput.get());
		expscModel.psisInput.get().isEstimatedInput.setValue(psiEstCheckBox.isSelected(), expscModel.psisInput.get());

		expscModel.isLambdaDecay.setValue(lambdaDecayCheckBox.isSelected(), expscModel);
		expscModel.lambdaRatesInput.get().isEstimatedInput.setValue(lambdaREstCheckBox.isSelected(), expscModel.lambdaRatesInput.get());

		if (lambdaDecayCheckBox.isSelected()) {
			expscModel.isLambdaRateFixed.setValue(lambdaRFixedCheckBox.isSelected(), expscModel);

			StringBuilder sblambdasr = new StringBuilder();
			int lrc = (expscModel.isLambdaRateFixed.get()) ? 1 : lrGrid.getColumnCount();
			for (int i = 0; i < lrc; i++) {
				if (i > 0) sblambdasr.append(" ");
				if (i < lambdasRList.size()) sblambdasr.append(Double.parseDouble(lambdasRList.get(i).getText()));
				else sblambdasr.append("0.0");
			}
			expscModel.lambdaRatesInput.get().setDimension(lrc);
			expscModel.lambdaRatesInput.get().valuesInput.setValue(sblambdasr.toString(), expscModel.lambdaRatesInput.get());
		}

		expscModel.isMuDecay.setValue(muDecayCheckBox.isSelected(), expscModel);
		expscModel.muRatesInput.get().isEstimatedInput.setValue(muREstCheckBox.isSelected(), expscModel.muRatesInput.get());
		if (muDecayCheckBox.isSelected()) {
			expscModel.isMuRateFixed.setValue(muRFixedCheckBox.isSelected(), expscModel);

			int mrc = (expscModel.isMuRateFixed.get()) ? 1 : mrGrid.getColumnCount();
			StringBuilder sbmusr = new StringBuilder();
			for (int i = 0; i < mrc; i++) {
				if (i > 0) sbmusr.append(" ");
				if (i < musRList.size()) sbmusr.append(Double.parseDouble(musRList.get(i).getText()));
				else sbmusr.append("0.0");
			}
			expscModel.muRatesInput.get().setDimension(mrc);
			expscModel.muRatesInput.get().valuesInput.setValue(sbmusr.toString(), expscModel.muRatesInput.get());
		}

		validateInput();
		refreshPanel();
	}

	/**
	 * Adds the lambda and mu fields.
	 */
	@Override
	protected void addLMPFields() {
		super.addLMPFields();

		HBox horBox = FXUtils.newHBox();
		lambdaDecayCheckBox.setSelected(expscModel.isLambdaDecay.get());
		horBox.getChildren().add(lambdaDecayCheckBox);
		pane.getChildren().add(horBox);

		// lambdaRates table
		if (lambdaDecayCheckBox.isSelected()) {
			horBox = FXUtils.newHBox();
			horBox = addInputLabel(horBox, expscModel.lambdaRatesInput);
			horBox.getChildren().add(lrGrid);
			lambdaREstCheckBox.setSelected(expscModel.lambdaRatesInput.get().isEstimatedInput.get());
			horBox.getChildren().add(lambdaREstCheckBox);
			pane.getChildren().add(horBox);

			horBox = FXUtils.newHBox();
			lambdaRFixedCheckBox.setSelected(expscModel.isLambdaRateFixed.get());
			horBox.getChildren().add(lambdaRFixedCheckBox);
			pane.getChildren().add(horBox);
		}

		horBox = FXUtils.newHBox();
		muDecayCheckBox.setSelected(expscModel.isMuDecay.get());
		horBox.getChildren().add(muDecayCheckBox);
		pane.getChildren().add(horBox);

		// muRates table
		if (muDecayCheckBox.isSelected()) {
			horBox = FXUtils.newHBox();
			horBox = addInputLabel(horBox, expscModel.muRatesInput);

			horBox.getChildren().add(mrGrid);
			muREstCheckBox.setSelected(expscModel.muRatesInput.get().isEstimatedInput.get());
			horBox.getChildren().add(muREstCheckBox);
			pane.getChildren().add(horBox);

			horBox = FXUtils.newHBox();
			muRFixedCheckBox.setSelected(expscModel.isMuRateFixed.get());
			horBox.getChildren().add(muRFixedCheckBox);
			pane.getChildren().add(horBox);
		}
	}

	/**
	 * Adds the event listeners for lambda, mu and nStates.
	 */
	@Override
	protected void addEventListeners() {
		super.addEventListeners();

		lambdaREstCheckBox.setOnAction(e -> {
			saveToStateChangeModel();
		});

		lambdaRFixedCheckBox.setOnAction(e -> {
			int dim = lambdaRFixedCheckBox.isSelected() ? 1 : nStatesSpinner.getValue();
			changeGridDimension(lrGrid, lambdasRList, dim);
			saveToStateChangeModel();
		});

		muREstCheckBox.setOnAction(e -> {
			saveToStateChangeModel();
		});

		muRFixedCheckBox.setOnAction(e -> {
			int dim = muRFixedCheckBox.isSelected() ? 1 : nStatesSpinner.getValue();
			changeGridDimension(mrGrid, musRList, dim);
			saveToStateChangeModel();
		});

		lambdaDecayCheckBox.setOnAction(e -> {
			if (lambdaDecayCheckBox.isSelected()) {
				lambdaREstCheckBox.setSelected(true); // reactivate priors and operators
				int dim = (lambdaRFixedCheckBox.isSelected()) ? 1 : nStatesSpinner.getValue();
				changeGridDimension(lrGrid, lambdasRList, dim);
			} else lambdaREstCheckBox.setSelected(false); // deactivate priors and operators

			saveToStateChangeModel();
		});

		muDecayCheckBox.setOnAction(e -> {
			if (muDecayCheckBox.isSelected()) {
				muREstCheckBox.setSelected(true); // reactivate priors and operators
				int dim = (muRFixedCheckBox.isSelected()) ? 1 : nStatesSpinner.getValue();
				changeGridDimension(mrGrid, musRList, dim);
			} else muREstCheckBox.setSelected(false); // deactivate priors and operators

			saveToStateChangeModel();
		});
	}

	/**
	 * Validate inputs.
	 */
	@Override
	public void validateInput() {
		try {
			expscModel.musInput.get().initAndValidate();
			expscModel.lambdasInput.get().initAndValidate();
			if(scModel.isSA()) expscModel.psisInput.get().initAndValidate();
			expscModel.muRatesInput.get().initAndValidate();
			expscModel.lambdaRatesInput.get().initAndValidate();
			expscModel.initAndValidate();
			if (m_validateLabel != null) {
				m_validateLabel.setVisible(false);
			}
		} catch (Exception e) {
			Log.err.println("Validation message: " + e.getMessage());
			if (m_validateLabel != null) {
				m_validateLabel.setTooltip(e.getMessage());
				m_validateLabel.setColor("red");
				m_validateLabel.setVisible(true);
			}
			notifyValidationListeners(ValidationStatus.IS_INVALID);
		}
		repaint();
	}
}
