/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.app.beauti;

import java.util.List;

import beastfx.app.inputeditor.BeautiDoc;
import beastfx.app.inputeditor.BeautiSubTemplate;
import beast.base.parser.PartitionContext;
import beastfx.app.inputeditor.InputEditor;
import beastfx.app.inputeditor.SmallLabel;
import beastfx.app.util.FXUtils;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import msbd.distributions.MultiRateTreeDistribution;
import beast.base.core.BEASTInterface;
import beast.base.core.Input;

/**
 * Shows and allows editing of an MRT Distribution in Beauti. Allows switching between subtemplates.
 * Adapted from TreeDistributionInputEditor
 */
public class MRTDistributionInputEditor extends InputEditor.Base {

	javafx.event.ActionEvent m_e;

	/**
	 * Instantiates a new MRT distribution input editor.
	 *
	 * @param doc the beauti doc
	 */
	public MRTDistributionInputEditor(BeautiDoc doc) {
		super(doc);
	}

	/**
	 * @return the class of objects that can be set by this input editor
	 */
	@Override
	public Class<?> type() {
		return MultiRateTreeDistribution.class;
	}

	/**
	 * Inits the input editor.
	 *
	 * @param input the input (parent object)
	 * @param beastObject the object edited (MRT distribution)
	 * @param listItemNr the list item nr
	 * @param bExpandOption the expand option
	 * @param bAddButtons the add buttons option
	 */
	@Override
	public void init(Input<?> input, BEASTInterface beastObject, int listItemNr, ExpandOption bExpandOption, boolean bAddButtons) {
		m_bAddButtons = bAddButtons;
		m_input = input;
		m_beastObject = beastObject;
		this.itemNr = listItemNr;

		pane = FXUtils.newHBox();
		MultiRateTreeDistribution distr = (MultiRateTreeDistribution) beastObject;
		String sText = distr.mrTreeInput.get().getID() + " Prior";

		Label label = new Label(sText);
		label.setPrefSize(LABEL_SIZE.getWidth(),LABEL_SIZE.getHeight());
		label.setMinSize(LABEL_SIZE.getWidth(), LABEL_SIZE.getHeight());
		pane.getChildren().add(label);

		List<BeautiSubTemplate> sAvailablePlugins = doc.getInputEditorFactory().getAvailableTemplates(m_input, m_beastObject, null, doc);

		ComboBox<BeautiSubTemplate> comboBox = new ComboBox<>();
		comboBox.getItems().addAll(sAvailablePlugins.toArray(new BeautiSubTemplate[]{}));
		comboBox.setId("MultiRateTreeDistribution");

		String sID = distr.getID();
		try {
			sID = sID.substring(0, sID.indexOf('.'));
		} catch (Exception e) {
			throw new RuntimeException("Improperly formatted ID: " + distr.getID());
		}
		for (BeautiSubTemplate template : sAvailablePlugins) {
			if (template.matchesName(sID)) {
				comboBox.setValue(template);
				comboBox.getSelectionModel().select(template);
			}
		}

		comboBox.setOnAction(e -> {
			m_e = e;
			@SuppressWarnings("unchecked")
			ComboBox<BeautiSubTemplate> currentComboBox = (ComboBox<BeautiSubTemplate>) m_e.getSource();
			@SuppressWarnings("unchecked")
			List<BEASTInterface> list = (List<BEASTInterface>) m_input.get();
			BeautiSubTemplate template = (BeautiSubTemplate) currentComboBox.getValue();
			PartitionContext partitionContext = doc.getContextFor(list.get(itemNr));
			try {
				InitModelConnector.latest = template.getMainID().substring(0, template.getMainID().indexOf('.'));
				template.createSubNet(partitionContext, list, itemNr, true);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			hardSync();
			refreshPanel();
		});
		
		pane.getChildren().add(comboBox);

		m_validateLabel = new SmallLabel("x", "red");
		m_validateLabel.setVisible(false);
		validateInput();
		
		pane.getChildren().add(m_validateLabel);
        getChildren().add(pane);
	}

}
