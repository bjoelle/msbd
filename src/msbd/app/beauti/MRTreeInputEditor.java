/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.app.beauti;

import java.awt.event.ActionEvent;
import java.util.List;

import beastfx.app.inputeditor.BeautiDoc;
import beastfx.app.inputeditor.BeautiSubTemplate;
import beastfx.app.inputeditor.TipDatesInputEditor;
import beastfx.app.util.FXUtils;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Tooltip;
import msbd.base.evolution.tree.MultiRateTree;
import beastfx.app.inputeditor.InputEditor;
import beast.base.core.BEASTInterface;
import beast.base.core.Input;

/**
 * Shows and allows editing of a MultiRateTree in Beauti. Allows fixing the starting tree.
 */
public class MRTreeInputEditor extends InputEditor.Base {
	ActionEvent m_e;
	BeautiSubTemplate current;
	CheckBox topEstCheckBox;
	private MultiRateTree tree;

	/**
	 * Instantiates a new MultiRateTree input editor.
	 *
	 * @param doc the beauti doc
	 */
	public MRTreeInputEditor(BeautiDoc doc) {
		super(doc);
	}

	/**
	 * @return the class of objects that can be edited by this input editor
	 */
	@Override
	public Class<?> type() {
		return MultiRateTree.class;
	}

	/**
	 * Inits the editor.
	 *
	 * @param input the input (tree or list of trees)
	 * @param beastObject the object (parent object)
	 * @param itemNr if a list, the index of the tree
	 * @param isExpandOption the expand option
	 * @param addButtons the add buttons option
	 */
	@Override
	public void init(Input<?> input, BEASTInterface beastObject, int itemNr, ExpandOption isExpandOption, boolean addButtons) {
		m_bAddButtons = addButtons;
		m_input = input;
		m_beastObject = beastObject;
		this.itemNr = itemNr;
		
		pane = FXUtils.newVBox();

		if (itemNr < 0) {
			tree = (MultiRateTree) m_input.get();
		} else {
			tree = (MultiRateTree) ((List<?>) m_input.get()).get(itemNr);
		}
		
		topEstCheckBox = new CheckBox(doc.beautiConfig.getInputLabel(tree, tree.estimateTopologyInput.getName()));
		topEstCheckBox.setId(m_input.getName() + ".isEstimatedTopology");
		topEstCheckBox.setSelected(tree.estimateTopologyInput.get());
		topEstCheckBox.setTooltip(new Tooltip(tree.estimateTopologyInput.getTipText()));		
		
		topEstCheckBox.setOnAction(e -> {
			tree.estimateTopologyInput.setValue(topEstCheckBox.isSelected(), tree);
		});
		
		pane.getChildren().add(topEstCheckBox);

		TipDatesInputEditor editor = new TipDatesInputEditor(doc);
		editor.init(m_input, tree, itemNr, isExpandOption, addButtons);
		pane.getChildren().add(editor.getComponent());
		
		getChildren().add(pane);
	}
	
}
