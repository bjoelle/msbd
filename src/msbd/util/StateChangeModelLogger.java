/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.util;

import java.io.PrintStream;

import beast.base.core.BEASTObject;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import msbd.evolution.tree.StateChangeModel;
import beast.base.core.Loggable;

/**
 * Log the parameters associated with each state of a StateChangeModel, up to maxStates.
 */
public class StateChangeModelLogger extends BEASTObject implements Loggable {

	public Input<StateChangeModel> stateChangeModelInput = new Input<>("stateChangeModel", "Rate shift model to log.", Validate.REQUIRED);

	public Input<Integer> maxStatesInput = new Input<>("maxStates", "Maximum number of states to log, if set to 0 will log all states. Default 0.", 0);
	public Input<Boolean> logSamplingInput = new Input<>("logSampling", "Whether to log parameters rho and (if not SA) sigma. Default false.", false);

	protected StateChangeModel stateChangeModel;
	protected int maxStates;
	protected boolean isMax = true;

	@Override
	public void initAndValidate() {
		stateChangeModel = stateChangeModelInput.get();
		maxStates = maxStatesInput.get();
		if (maxStates == 0) {
			maxStates = Integer.MAX_VALUE;
			isMax = false;
		}
	}

	@Override
	public void init(PrintStream out) {
		out.print("nstates" + "\t");
		out.print("nstar" + "\t");
		out.print("gamma" + "\t");
		if(logSamplingInput.get()) {
			out.print("rho" + "\t");
			if(!stateChangeModel.isSA()) out.print("sigma" + "\t");
		}

		int ncol = maxStates;
		if (!isMax) ncol = 1;
		for (int i = 0; i < ncol; i++) {
			out.print("lambda_" + i + "\t");
			out.print("mu_" + i + "\t");
			if(stateChangeModel.isSA()) out.print("psi_" + i + "\t");
		}

	}

	@Override
	public void log(long nSample, PrintStream out) {
		out.print(stateChangeModel.getNStates() + "\t");
		out.print(stateChangeModel.getNstar() + "\t");
		out.print(stateChangeModel.getGamma() + "\t");
		if(logSamplingInput.get()) {
			out.print(stateChangeModel.getRho() + "\t");
			if(!stateChangeModel.isSA()) out.print(stateChangeModel.getSigma() + "\t");
		}
		
		int n = stateChangeModel.getNStates();
		for (int i = 0; i < Math.min(n, maxStates); i++) {
			out.print(stateChangeModel.getLambda(i) + "\t");
			out.print(stateChangeModel.getMu(i) + "\t");
			if(stateChangeModel.isSA()) out.print(stateChangeModel.getPsi(i) + "\t");
		}
		if (isMax) {
			for (int i = 0; i < (maxStates - n); i++) {
				out.print("NaN" + "\t");
				out.print("NaN" + "\t");
				if(stateChangeModel.isSA()) out.print("NaN" + "\t");
			}
		}
	}

	@Override
	public void close(PrintStream out) {}

}
