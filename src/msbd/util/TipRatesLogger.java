/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.util;

import java.io.PrintStream;

import beast.base.core.BEASTObject;
import beast.base.core.Input;
import beast.base.core.Loggable;
import beast.base.core.Input.Validate;
import msbd.base.evolution.tree.MultiRateNode;
import msbd.base.evolution.tree.MultiRateTree;
import msbd.evolution.tree.ExpStateChangeModel;
import msbd.evolution.tree.StateChangeModel;

/**
 * Log the parameters associated with each tip of the tree (optionally the nodes as well). Nodes are identified by their
 * IDs.
 */
public class TipRatesLogger extends BEASTObject implements Loggable {

	public Input<StateChangeModel> stateChangeModelInput = new Input<>("stateChangeModel", "Rate shift model to log.", Validate.REQUIRED);

	public Input<MultiRateTree> multiRateTreeInput = new Input<>("multiRateTree", "Multi-rate tree to log.", Validate.REQUIRED);

	public Input<Boolean> nodeLogInput = new Input<>("nodeLog", "Should node rates also be logged, default false", false);

	private StateChangeModel scModel;
	private ExpStateChangeModel expModel;
	private MultiRateTree mrTree;
	private boolean nodeLog, isExp = false;

	@Override
	public void initAndValidate() {
		scModel = stateChangeModelInput.get();
		if (scModel instanceof ExpStateChangeModel) {
			expModel = (ExpStateChangeModel) scModel;
			isExp = true;
		}
		mrTree = multiRateTreeInput.get();
		nodeLog = nodeLogInput.get();
	}

	@Override
	public void init(PrintStream out) {
		String outName;
		if (mrTree.getID() == null || mrTree.getID().matches("\\s*")) outName = "mrTree";
		else outName = mrTree.getID();

		if (!nodeLog) {
			for (int i = 0; i < mrTree.getLeafNodeCount(); i++) {
				this.printHeaders(out, outName, i);
			}
		} else {
			for (int i = 0; i < mrTree.getNodeCount(); i++) {
				this.printHeaders(out, outName, i);
			}
		}
	}

	@Override
	public void log(long nSample, PrintStream out) {
		if (!nodeLog) {
			for (int i = 0; i < mrTree.getLeafNodeCount(); i++) {
				this.printData(out, i);
			}
		} else {
			for (int i = 0; i < mrTree.getNodeCount(); i++) {
				this.printData(out, i);
			}
		}
	}

	@Override
	public void close(PrintStream out) {}

	/**
	 * Prints the headers for one node.
	 *
	 * @param out the out stream
	 * @param outName the identifier of the tree
	 * @param i the index of the node
	 */
	private void printHeaders(PrintStream out, String outName, int i) {
		out.print(outName + ".node_" + mrTree.getNode(i).getID() + "_lambda" + "\t");
		out.print(outName + ".node_" + mrTree.getNode(i).getID() + "_mu" + "\t");
		if(scModel.isSA()) out.print(outName + ".node_" + mrTree.getNode(i).getID() + "_psi" + "\t");
		if (isExp) out.print(outName + ".node_" + mrTree.getNode(i).getID() + "_lambdaRate" + "\t");
		if (isExp) out.print(outName + ".node_" + mrTree.getNode(i).getID() + "_muRate" + "\t");
	}

	/**
	 * Prints the data for one node.
	 *
	 * @param out the out stream
	 * @param i the index of the node
	 */
	private void printData(PrintStream out, int i) {
		int c = ((MultiRateNode) mrTree.getNode(i)).getNodeState();
		out.print(scModel.getLambda(c) + "\t");
		out.print(scModel.getMu(c) + "\t");
		if(scModel.isSA()) out.print(scModel.getPsi(c) + "\t");
		if (isExp) out.print(expModel.getLambdaRate(c) + "\t");
		if (isExp) out.print(expModel.getMuRate(c) + "\t");
	}
}
