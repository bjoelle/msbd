/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.util;

import java.io.PrintStream;

import beast.base.inference.CalculationNode;
import msbd.base.evolution.tree.MultiRateNode;
import msbd.base.evolution.tree.MultiRateTree;
import beast.base.core.Function;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.core.Loggable;
import beast.base.evolution.tree.Node;

/**
 * Log the number of state changes in the tree.
 */
public class StateChangesCountLogger extends CalculationNode implements Loggable, Function {

	public Input<MultiRateTree> multiRateTreeInput = new Input<>("multiRateTree", "Multi-rate tree whose changes will be counted.",
			Validate.REQUIRED);

	private MultiRateTree mrTree;

	@Override
	public void initAndValidate() {
		mrTree = multiRateTreeInput.get();
	}

	@Override
	public void init(PrintStream out) {
		String idString = mrTree.getID();
		out.print(idString + "count_of_changes" + "\t");
	}

	@Override
	public void log(long nSample, PrintStream out) {		
		out.print(getCount() + "\t");
	}

	@Override
	public void close(PrintStream out) {}

	/**
	 * Gets the dimension.
	 *
	 * @return the dimension
	 */
	@Override
	public int getDimension() {
		return 1;
	}

	/**
	 * Gets the array value at specified index.
	 *
	 * @param dim the index
	 * @return the array value
	 */
	@Override
	public double getArrayValue(int dim) {
		return getCount();
	}
	
	/**
	 * Gets the total count of state changes.
	 *
	 * @return the count
	 */
	int getCount() {
		int count = 0;
		for (Node node : mrTree.getNodesAsArray()) {
			if (node.isRoot()) continue;
			count += ((MultiRateNode) node).getChangeCount();
		}
		return count;
	}

}
