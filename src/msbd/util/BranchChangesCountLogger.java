/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.util;

import java.io.PrintStream;

import beast.base.core.BEASTObject;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import msbd.base.evolution.tree.MultiRateNode;
import msbd.base.evolution.tree.MultiRateTree;
import beast.base.core.Loggable;

/**
 * Log the number of rate shifts on each branch of the tree.
 * <p>
 * Branches are identified by the ID of the node below the branch
 */
public class BranchChangesCountLogger extends BEASTObject implements Loggable {

	public Input<MultiRateTree> multiRateTreeInput = new Input<>("multiRateTree", "Multi-rate tree to log.", Validate.REQUIRED);

	private MultiRateTree mrTree;

	@Override
	public void initAndValidate() {
		mrTree = multiRateTreeInput.get();
	}

	@Override
	public void init(PrintStream out) {
		String outName;
		if (mrTree.getID() == null || mrTree.getID().matches("\\s*")) outName = "mrTree";
		else outName = mrTree.getID();

		for (int i = 0; i < mrTree.getNodeCount(); i++) {
			out.print(outName + ".node_" + mrTree.getNode(i).getID() + "_nshifts" + "\t");
		}
	}

	@Override
	public void log(long nSample, PrintStream out) {
		for (int i = 0; i < mrTree.getNodeCount(); i++) {
			out.print(((MultiRateNode) mrTree.getNode(i)).getChangeCount() + "\t");
		}
	}

	@Override
	public void close(PrintStream out) {}

}
