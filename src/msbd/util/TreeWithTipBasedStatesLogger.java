package msbd.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import beast.base.core.Function;
import beast.base.core.Loggable;
import beast.base.inference.parameter.IntegerParameter;
import msbd.base.evolution.tree.MultiRateNode;
import msbd.base.evolution.tree.MultiRateTree;
import beast.base.evolution.tree.Node;

/**
 * Logger for a multi-rate tree with node and tip states as metadata.
 */
public class TreeWithTipBasedStatesLogger extends MRTWithMetaDataLogger implements Loggable {

	/**
	 * Gets the tip-based rate metadata for specified tree.
	 *
	 * @param tree the tree
	 * @return the metadata
	 */
	@Override
	List<Function> getMetadata(MultiRateTree tree) {
		List<Function> metadata = new ArrayList<Function>();

		// add tip-based states to metadata
		Integer[] states = new Integer[tree.getNodeCount()];
		Map<Integer, Integer> stateNR = new HashMap<Integer, Integer>();
		int rootstate = ((MultiRateNode) tree.getRoot()).getNodeState();
		stateNR.put(rootstate, 0);

		for (Node n : tree.getNodesAsArray()) {
			int st = ((MultiRateNode) n).getNodeState();
			Integer newst = stateNR.get(st);
			if (newst == null) {
				newst = n.getNr();
				stateNR.put(st, newst);
			}
			states[n.getNr()] = newst;
		}

		IntegerParameter statesMD = new IntegerParameter(states);
		statesMD.setID("state");
		metadata.add(statesMD);
		
		return metadata;
	}

}
