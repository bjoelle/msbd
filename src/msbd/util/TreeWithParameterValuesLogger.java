package msbd.util;

import java.util.ArrayList;
import java.util.List;

import beast.base.core.Function;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.core.Loggable;
import beast.base.inference.parameter.RealParameter;
import msbd.base.evolution.tree.MultiRateNode;
import msbd.base.evolution.tree.MultiRateTree;
import msbd.evolution.tree.StateChangeModel;
import beast.base.evolution.tree.Node;

/**
 * Logger for a multi-rate tree with nodes and tips rates as metadata.
 */
public class TreeWithParameterValuesLogger extends MRTWithMetaDataLogger implements Loggable {

	public Input<StateChangeModel> stateChangeModelInput = new Input<>("stateChangeModel", "Model of transition between states.",
			Validate.REQUIRED);

	StateChangeModel scModel;

	@Override
	public void initAndValidate() {
		super.initAndValidate();
		scModel = stateChangeModelInput.get();
	}

	/**
	 * Gets the rate values metadata for specified tree.
	 *
	 * @param tree the tree
	 * @return the metadata
	 */
	@Override
	List<Function> getMetadata(MultiRateTree tree) {
		List<Function> metadata = new ArrayList<Function>();

		// add parameter values to metadata
		Double[] lambdas = new Double[tree.getNodeCount()];
		Double[] mus = new Double[tree.getNodeCount()];
		Double[] psis = new Double[tree.getNodeCount()];

		for (Node n : tree.getNodesAsArray()) {
			int st = ((MultiRateNode) n).getNodeState();
			lambdas[n.getNr()] = scModel.getLambda(st);
			mus[n.getNr()] = scModel.getMu(st);
			if(scModel.isSA()) psis[n.getNr()] = scModel.getPsi(st);
		}

		RealParameter lambdasMD = new RealParameter(lambdas);
		lambdasMD.setID("lambda");
		RealParameter musMD = new RealParameter(mus);
		musMD.setID("mu");
		metadata.add(lambdasMD);
		metadata.add(musMD);

		if(scModel.isSA()) {
			RealParameter psisMD = new RealParameter(psis);
			psisMD.setID("psi");
			metadata.add(psisMD);
		}

		return(metadata);
	}

}
