/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.util;

import java.io.PrintStream;

import msbd.evolution.tree.ExpStateChangeModel;

/**
 * Log the parameters associated with each state of an ExpStateChangeModel, up to maxStates.
 */
public class ExpStateChangeModelLogger extends StateChangeModelLogger {

	private ExpStateChangeModel expModel;
	private boolean lrdec, mrdec;

	@Override
	public void initAndValidate() {
		super.initAndValidate();

		if (!(stateChangeModel instanceof ExpStateChangeModel)) throw new IllegalArgumentException("Trying to use the exponential logger with a non-exponential model");
		expModel = (ExpStateChangeModel) stateChangeModel;
		lrdec = expModel.isLambdaDecay();
		mrdec = expModel.isMuDecay();
	}

	@Override
	public void init(PrintStream out) {
		super.init(out);

		int ncol = maxStates;
		if (!isMax) ncol = 1;
		for (int i = 0; i < ncol; i++) {
			if (lrdec) out.print("lambdaRate_" + i + "\t");
			if (mrdec) out.print("muRate_" + i + "\t");
		}
	}

	@Override
	public void log(long nSample, PrintStream out) {
		super.log(nSample, out);

		int n = expModel.getNStates();

		for (int i = 0; i < Math.min(n, maxStates); i++) {
			if (lrdec) out.print(expModel.getLambdaRate(i) + "\t");
			if (mrdec) out.print(expModel.getMuRate(i) + "\t");
		}
		if (isMax) {
			for (int i = 0; i < (maxStates - n); i++) {
				if (lrdec) out.print("NaN" + "\t");
				if (mrdec) out.print("NaN" + "\t");
			}
		}
	}
}
