package msbd.util;

import java.io.PrintStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import beast.base.core.BEASTObject;
import beast.base.core.Function;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.core.Loggable;
import beast.base.inference.StateNode;
import beast.base.inference.parameter.Parameter;
import beast.base.inference.parameter.RealParameter;
import msbd.base.evolution.tree.MultiRateTree;
import beast.base.evolution.branchratemodel.BranchRateModel;
import beast.base.evolution.tree.Node;

/**
 * Logger for a multi-rate tree with metadata.
 */
public abstract class MRTWithMetaDataLogger extends BEASTObject implements Loggable {

	final public Input<MultiRateTree> treeInput = new Input<>("tree", "MRtree to be logged", Validate.REQUIRED);

	final public Input<List<Function>> parameterInput = new Input<>("metadata", "meta data to be logged with the tree nodes",new ArrayList<>());
	final public Input<BranchRateModel.Base> clockModelInput = new Input<>("branchratemodel", "rate to be logged with branches of the tree");
	final public Input<Boolean> substitutionsInput = new Input<>("substitutions", "report branch lengths as substitutions (branch length times clock rate for the branch)", false);

	final public Input<Integer> decimalPlacesInput = new Input<>("dp", "the number of decimal places to use writing branch lengths, rates and real-valued metadata, use -1 for full precision (default = full precision)", -1);
	public Input<Boolean> logChangeNodesInput = new Input<>("logChangeNodes", "Whether to log change points on edges as single nodes, default true", true);

	boolean logChangeNodes;
	boolean substitutions = false;
	private DecimalFormat df;
	int nodeCount;

	@Override
	public void initAndValidate() {

		int dp = decimalPlacesInput.get();
		if (dp < 0) {
			df = null;
		} else {
			// just new DecimalFormat("#.######") (with dp time '#' after the decimal)
			df = new DecimalFormat("#."+new String(new char[dp]).replace('\0', '#'));
			df.setRoundingMode(RoundingMode.HALF_UP);
		}

		// without substitution model, reporting substitutions == reporting branch lengths 
		if (clockModelInput.get() != null) {
			substitutions = substitutionsInput.get();
		}

		logChangeNodes = logChangeNodesInput.get();
	}

	/**
	 * Translate to newick.
	 *
	 * @param node the current node
	 * @param metadataList the list of standard metadata
	 * @param MRTmetadataList the list of MRT-related metadata
	 * @param branchRateModel the branch rate model
	 * @return the Newick string
	 */
	protected String toNewick(Node node, List<Function> metadataList, List<Function> MRTmetadataList, BranchRateModel.Base branchRateModel) {
		StringBuffer buf = new StringBuffer();
		if (! node.isLeaf()) {
			buf.append("(");
			for(int i = 0; i < node.getChildCount(); i++) {			
				buf.append(toNewick(node.getChild(i), metadataList, MRTmetadataList, branchRateModel));
				if (i < node.getChildCount() - 1) buf.append(',');
			}
			buf.append(")");
		} else {
			buf.append(node.getNr() + 1);
		}

		int nodeNr = node.getNr();
		Node clockNode = node;
		if (logChangeNodes && nodeNr >= nodeCount) { // if node is single node indicating state change, we need the actual node for rate & metadata logging
			while (nodeNr >= nodeCount) {
				clockNode = clockNode.getChild(0);
				nodeNr = clockNode.getNr();
			}
			MultiRateTree tree = (MultiRateTree) treeInput.get().getCurrent();
			clockNode = tree.getNode(nodeNr);
		}

		buf.append("[&");

		if (metadataList.size() > 0) {
			for (Function metadata : metadataList) {
				buf.append(((BEASTObject)metadata).getID());
				buf.append('=');
				if (metadata instanceof Parameter<?>) {
					Parameter<?> p = (Parameter<?>) metadata;
					int dim = p.getMinorDimension1();
					if (dim > 1) {
						buf.append('{');
						for (int i = 0; i < dim; i++) {
							if (metadata instanceof RealParameter) {
								RealParameter rp = (RealParameter) metadata;
								appendDouble(buf, rp.getMatrixValue(nodeNr, i));
							} else {
								buf.append(p.getMatrixValue(nodeNr, i));
							}
							if (i < dim - 1) {
								buf.append(',');
							}
						}
						buf.append('}');
					} else {
						if (metadata instanceof RealParameter) {
							RealParameter rp = (RealParameter) metadata;
							appendDouble(buf, rp.getArrayValue(nodeNr));
						} else {
							buf.append(metadata.getArrayValue(nodeNr));
						}
					}
				} else {
					buf.append(metadata.getArrayValue(nodeNr));
				}
				if (metadataList.indexOf(metadata) < metadataList.size() - 1) {
					buf.append(",");
				}
			}
			buf.append(",");
		}

		if (branchRateModel != null) {
			buf.append("rate=");
			appendDouble(buf, branchRateModel.getRateForBranch(clockNode));
			buf.append(",");
		}

		for (Function metadata : MRTmetadataList) {
			buf.append(((BEASTObject)metadata).getID());
			buf.append('=');
			if (metadata instanceof RealParameter) {
				RealParameter rp = (RealParameter) metadata;
				appendDouble(buf, rp.getArrayValue(node.getNr()));
			} else {
				buf.append(metadata.getArrayValue(node.getNr()));
			}
			if (MRTmetadataList.indexOf(metadata) < MRTmetadataList.size() - 1) {
				buf.append(",");
			}
		}

		buf.append(']');

		buf.append(":");
		if (substitutions) {
			appendDouble(buf, node.getLength() * branchRateModel.getRateForBranch(clockNode));
		} else {
			appendDouble(buf, node.getLength());
		}
		return buf.toString();
	}

	/**
	 * Append double to buffer.
	 *
	 * @param buf the buffer
	 * @param d the double
	 */
	protected void appendDouble(StringBuffer buf, double d) {
		if (df == null) {
			buf.append(d);
		} else {
			buf.append(df.format(d));
		}
	}

	@Override
	public void init(PrintStream out) {
		treeInput.get().init(out);
	}

	@Override
	public void log(long sample, PrintStream out) {		
		MultiRateTree tree = (MultiRateTree) treeInput.get().getCurrent();
		if(logChangeNodes) {
			nodeCount = tree.getNodeCount();
			tree = (MultiRateTree) tree.getFlattenedTree();
		}

		List<Function> metadata = parameterInput.get();
		for (int i = 0; i < metadata.size(); i++) {
			if (metadata.get(i) instanceof StateNode) {
				metadata.set(i, ((StateNode) metadata.get(i)).getCurrent());
			}
		}

		List<Function> MRT_metadata = getMetadata(tree);	
		BranchRateModel.Base branchRateModel = clockModelInput.get();
		// write out the log tree with meta data
		out.print("tree STATE_" + sample + " = ");
		tree.getRoot().sort();

		out.print(toNewick(tree.getRoot(), metadata, MRT_metadata, branchRateModel));
		out.print(";");
	}

	/**
	 * Gets the MRT-specific metadata.
	 *
	 * @param tree the tree
	 * @return the metadata
	 */
	abstract List<Function> getMetadata(MultiRateTree tree);

	@Override
	public void close(PrintStream out) {
		treeInput.get().close(out);
	}
}
