/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.util;

import java.io.PrintStream;

import beast.base.inference.CalculationNode;
import msbd.base.evolution.tree.MultiRateTree;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.core.Loggable;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.TreeUtils;

/**
 * Log the colless index and gamma statistic of the sampled multi-rate tree.
 */
public class TreeStatsLogger extends CalculationNode implements Loggable {
	public Input<MultiRateTree> multiRateTreeInput = new Input<>("multiRateTree", "Multi-rate tree to log.", Validate.REQUIRED);

	MultiRateTree mrT;

	@Override
	public void initAndValidate() {
		mrT = multiRateTreeInput.get();
	}

	@Override
	public void init(PrintStream out) {
		out.print("CollessIndex" + "\t" + "GammaStat" + "\t");
	}

	@Override
	public void log(long sample, PrintStream out) {
		out.print(calcCollessIndex() + "\t" + calcGammaStatistic() + "\t");
	}

	@Override
	public void close(PrintStream out) {}

	/**
	 * Calculate the colless index.
	 *
	 * @return the colless index
	 */
	double calcCollessIndex() { // adapted from beast-mcmc
		double C = 0;

		int n = mrT.getNodeCount();
		for (int i = 0; i < n; i++) {
			Node node = mrT.getNode(i);
			if (node.isLeaf()) continue;
			int r = node.getLeft().getLeafNodeCount();
			int s = node.getRight().getLeafNodeCount();
			C += Math.abs(r - s);
		}

		n = mrT.getLeafNodeCount();
		return C;
	}

	/**
	 * Calculate the gamma statistic.
	 *
	 * @return the gamma statistic
	 */
	double calcGammaStatistic() { // adapted from beast-mcmc
		int n = mrT.getLeafNodeCount();
		double[] g = TreeUtils.getIntervals(mrT);
		double T = mrT.getTotalLength(); // total branch length

		double sum = 0.0;
		for (int i = 2; i < n; i++) {
			for (int k = 2; k <= i; k++) {
				sum += k * g[k - 2];
			}
		}
		double gamma = ((sum / (n - 2.0)) - (T / 2.0)) / (T * Math.sqrt(1.0 / (12.0 * (n - 2.0))));
		return gamma;
	}

}
