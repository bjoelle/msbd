/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.base.evolution.tree;

import java.util.List;

import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.evolution.tree.Node;
import beast.base.inference.StateNode;
import beast.base.inference.StateNodeInitialiser;
import beast.base.inference.parameter.IntegerParameter;
import beast.base.util.Randomizer;

/**
 * This generates a random colouring on a multi-rate tree (not very useful in current form).
 */
public class RandomMultiRateTree extends MultiRateTree implements StateNodeInitialiser {
	public Input<IntegerParameter> nStatesInput = new Input<>("nStates", "Maximum number of states on the generated tree", Validate.REQUIRED);

	private int nStates;

	@Override
	public void initAndValidate() {
		super.initAndValidate();

		nStates = nStatesInput.get().getValue();
		// Randomly fill leaf state array:
		for (int i = 0; i < getLeafNodeCount(); i++)
			((MultiRateNode) getNode(i)).setNodeState(Randomizer.nextInt(nStates));

		generateColouring(getRoot());

		if (!isValid()) throw new RuntimeException("Inconsistent state assignment.");
	}

	@Override
	public void initStateNodes() {}

	/**
	 * Generate a random colouring of the sub-tree consistent with all children and nStates.
	 *
	 * @param node the starting node
	 */
	public void generateColouring(Node node) {

		if (node.getNodeCount() >= 3) {
			Node left = node.getChild(0);
			Node right = node.getChild(1);

			if (left.getNodeCount() >= 3) generateColouring(left);
			if (right.getNodeCount() >= 3) generateColouring(right);

			int leftCol = ((MultiRateNode) left).getFinalState();
			int rightCol = ((MultiRateNode) right).getFinalState();

			if (leftCol == rightCol) ((MultiRateNode) node).setNodeState(leftCol);

			else {

				Node nodeToKeep, other;
				if (Randomizer.nextBoolean()) {
					nodeToKeep = left;
					other = right;
				} else {
					nodeToKeep = right;
					other = left;
				}

				((MultiRateNode) node).setNodeState(((MultiRateNode) nodeToKeep).getNodeState());

				double changeTime = Randomizer.nextDouble() * (node.getHeight() - other.getHeight()) + other.getHeight();
				((MultiRateNode) other).addChange(((MultiRateNode) node).getNodeState(), changeTime);
			}
		}
	}

	/**
	 * Gets the initialised state nodes.
	 *
	 * @param stateNodeList the state node list
	 * @return the initialised state nodes
	 */
	@Override
	public void getInitialisedStateNodes(List<StateNode> stateNodeList) {
		stateNodeList.add(this);
	}
}
