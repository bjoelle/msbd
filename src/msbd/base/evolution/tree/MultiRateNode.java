/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.base.evolution.tree;

import java.util.ArrayList;
import java.util.List;

import beast.base.evolution.tree.Node;

/**
 * Node for a multi-rate tree.
 */
public class MultiRateNode extends Node {

	// state metadata:
	int nStateChanges = 0;
	List<Integer> changeStates = new ArrayList<Integer>();
	List<Double> changeTimes = new ArrayList<Double>();
	int nodeState = 0;

	double pvalues[] = new double[1];

	/**
	 * Gets the time range around a change (preceding change/node to following change/node, going towards the root).
	 *
	 * @param i the index of the change
	 * @return a vector containing the times below (index 0) and above the change
	 */
	public double[] getTimeRangeAround(int i) {
		double[] times = new double[2];
		if (i == 0) times[0] = getHeight();
		else times[0] = getChangeTime(i - 1);
		if (i == getChangeCount() - 1) times[1] = getParent().getHeight();
		else times[1] = getChangeTime(i + 1);
		return times;
	}

	/**
	 * Gets the time range below a change (preceding change/node to this change/node, going towards the root).
	 *
	 * @param i the index of the change
	 * @return a vector containing the time below the change (index 0) and the time of the change
	 */
	public double[] getTimeRangeBelow(int i) {
		double[] times = new double[2];
		if (i == 0) times[0] = getHeight();
		else times[0] = getChangeTime(i - 1);
		if (i == getChangeCount()) times[1] = getParent().getHeight();
		else times[1] = getChangeTime(i);
		return times;
	}

	/**
	 * Retrieve the total number of changes on the branch above this node.
	 *
	 * @return change count
	 */
	public int getChangeCount() {
		return nStateChanges;
	}

	/**
	 * Obtain the destination state (going towards the root) of the change specified by idx on the branch between this
	 * node and its parent.
	 *
	 * @param idx the index of the change
	 * @return change state
	 */
	public int getChangeState(int idx) {
		return changeStates.get(idx);
	}

	/**
	 * Obtain the time of the change specified by idx on the branch between this node and its parent.
	 *
	 * @param idx the index of the change
	 * @return time of change
	 */
	public double getChangeTime(int idx) {
		return changeTimes.get(idx);
	}

	/**
	 * Retrieve the final state on branch above this node (=state of the parent of the node).
	 *
	 * @return final state
	 */
	public int getFinalState() {
		if (nStateChanges > 0) return changeStates.get(nStateChanges - 1);
		else return nodeState;
	}

	/**
	 * Retrieve time of final state change on branch above this node, or the height of the node if no changes exist.
	 *
	 * @return final state change time
	 */
	public double getFinalChangeTime() {
		if (nStateChanges > 0) return changeTimes.get(nStateChanges - 1);
		else return getHeight();
	}

	/**
	 * Obtain the state value at this node. (Used for caching or for specifying state changes at nodes.)
	 *
	 * @return state value at this node
	 */
	public int getNodeState() {
		return nodeState;
	}

	/**
	 * Sets state of node.
	 *
	 * @param nodeState New node state.
	 */
	public void setNodeState(int nodeState) {
		startEditing();
		this.nodeState = nodeState;
	}

	/**
	 * Add a new state change to branch above node.
	 *
	 * @param newState Destination (towards the root) state of change
	 * @param time Time at which change occurs.
	 */
	public void addChange(int newState, double time) {
		startEditing();
		changeStates.add(newState);
		changeTimes.add(time);
		nStateChanges += 1;
	}

	/**
	 * Remove all state changes from branch above node.
	 */
	public void clearChanges() {
		startEditing();
		changeStates.clear();
		changeTimes.clear();
		nStateChanges = 0;
	}

	/**
	 * Change time at which state change idx occurs.
	 *
	 * @param idx Index of state-change to modify.
	 * @param newTime New time of state-change.
	 */
	public void setChangeTime(int idx, double newTime) {
		startEditing();
		changeTimes.set(idx, newTime);
	}

	/**
	 * Change destination state of state change idx.
	 *
	 * @param idx Index of state-change to modify.
	 * @param newState New destination state of state-change.
	 */
	public void setChangeState(int idx, int newState) {
		startEditing();
		changeStates.set(idx, newState);
	}

	/**
	 * Truncate state change lists so that a maximum of newNChanges remain. Does nothing if newNChanges>= the current
	 * value.
	 *
	 * @param newNChanges new change count.
	 */
	public void truncateChanges(int newNChanges) {
		startEditing();

		while (nStateChanges > newNChanges) {
			changeStates.remove(nStateChanges - 1);
			changeTimes.remove(nStateChanges - 1);
			nStateChanges -= 1;
		}
	}

	/**
	 * Insert a new state change at index idx to state newState at the specified time.
	 *
	 * @param idx the index
	 * @param newState the new state
	 * @param newTime the new time
	 */
	public void insertChange(int idx, int newState, double newTime) {
		startEditing();

		if (idx > nStateChanges) throw new IllegalArgumentException("Index to insertChange() out of range.");

		changeTimes.add(idx, newTime);
		changeStates.add(idx, newState);
		nStateChanges += 1;
	}

	/**
	 * Remove the state change at index idx from the branch above node.
	 *
	 * @param idx the index
	 */
	public void removeChange(int idx) {
		startEditing();

		if (idx >= nStateChanges) throw new IllegalArgumentException("Index to removeChange() out of range.");

		changeTimes.remove(idx);
		changeStates.remove(idx);
		nStateChanges -= 1;

	}

	// Methods used to manage precalculated p values for exponential decay

	/**
	 * Gets the p-value either at the node (i=0) or at shift number i-1.
	 *
	 * @param i the index
	 * @return the p-value
	 */
	public double getPval(int i) {
		if (i >= pvalues.length) return -1;
		return pvalues[i];
	}

	/**
	 * Sets the p-value either at the node (i=0) or at shift number i-1. Reinitializes the p-values array if its length
	 * is wrong.
	 *
	 * @param i the index
	 * @param x the new value
	 */
	public void setPval(int i, double x) {
		if (pvalues.length != changeStates.size() + 1) {
			pvalues = new double[changeStates.size() + 1];
		}
		pvalues[i] = x;
	}

	/**
	 * Shallow copy.
	 *
	 * @return shallow copy of node
	 */
	public MultiRateNode shallowCopy() {
		MultiRateNode node = new MultiRateNode();
		node.height = height;
		node.parent = parent;
		node.children.addAll(children);

		node.nStateChanges = nStateChanges;
		node.changeTimes.addAll(changeTimes);
		node.changeStates.addAll(changeStates);
		node.nodeState = nodeState;

		node.labelNr = labelNr;
		node.metaDataString = metaDataString;
		node.ID = ID;

		return node;
	}

	/**
	 * ************************** Methods ported from Node * **************************.
	 **/

	/**
	 * @return (deep) copy of node
	 */
	@Override
	public MultiRateNode copy() {
		MultiRateNode node = new MultiRateNode();
		node.height = height;
		node.labelNr = labelNr;
		node.metaDataString = metaDataString;
		node.parent = null;
		node.ID = ID;
		node.nStateChanges = nStateChanges;
		node.changeTimes.addAll(changeTimes);
		node.changeStates.addAll(changeStates);
		node.nodeState = nodeState;

		for (Node child : getChildren()) {
			node.addChild(child.copy());
		}

		return node;
	}

	/**
	 * assign values from a tree in array representation.
	 *
	 * @param nodes the nodes
	 * @param node the node
	 */
	@Override
	public void assignFrom(Node[] nodes, Node node) {
		height = node.getHeight();
		labelNr = node.getNr();
		metaDataString = node.metaDataString;
		parent = null;
		ID = node.getID();

		MultiRateNode mtNode = (MultiRateNode) node;
		nStateChanges = mtNode.nStateChanges;
		changeTimes.clear();
		changeTimes.addAll(mtNode.changeTimes);
		changeStates.clear();
		changeStates.addAll(mtNode.changeStates);
		nodeState = mtNode.nodeState;

		removeAllChildren(false);
		for (Node child : node.getChildren()) {
			addChild(nodes[child.getNr()]);
			getChild(getChildCount() - 1).assignFrom(nodes, child);
			getChild(getChildCount() - 1).setParent(this);
		}
	}

}
