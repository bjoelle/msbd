/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.base.evolution.tree;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.math.MathException;

import beast.base.core.BEASTInterface;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.inference.StateNode;
import beast.base.inference.StateNodeInitialiser;
import beast.base.inference.parameter.IntegerParameter;
import beast.base.core.Log;
import beast.base.evolution.alignment.TaxonSet;
import beast.base.evolution.tree.MRCAPrior;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.TraitSet;
import beast.base.inference.distribution.ParametricDistribution;
import beast.base.util.Randomizer;

/**
 * Initializes a multi-rate tree from a coalescent model using the parameters from the given birth-death model.
 */
public class MultiRateTreeFromBDModel extends MultiRateTree implements StateNodeInitialiser {

	public Input<Double> lambdaInput = new Input<>("lambda", "Birth rate to use in simulator.", Validate.REQUIRED);

	public Input<IntegerParameter> nLeavesInput = new Input<>("nLeaves", "Number of leaf nodes.");

	public Input<String> outputFileNameInput = new Input<>("outputFileName", "Optional name of file to write simulated tree to.");

	public Input<Double> mrcaAgeInput = new Input<>("mrcaAge", "Scale resulting tree so that the MRCA has the chosen age.");


	protected Double lambda;
	protected int nLeaves;
	private List<String> leafNames;

	// taxonSets and m_bounds are indexed together

	// taxon sets of clades that has a constraint of calibrations. Monophyletic constraints may be nested, and are sorted by the code to be at a
	// higher index, i.e iterating from zero up does post-order (descendants before parent).
	List<Set<String>> taxonSets;

	// hard bound for the set, if any
	List<Bound> m_bounds;

	List<Integer>[] children;
	int nConstraints;

	// auxiliary variables for tree generation
	Set<String> usedTaxa = new TreeSet<String>();
	int nextNodeNr = 0;

	/**
	 * Instantiates a new multi-rate tree from birth-death model.
	 */
	public MultiRateTreeFromBDModel() {}

	@Override
	public void initAndValidate() {

		super.initAndValidate();

		// Obtain required parameters from inputs:
		lambda = lambdaInput.get();

		// Obtain leaf names from explicit input or alignment:
		leafNames = new ArrayList<>();
		if (this.m_taxonset.get() != null) {
			nLeaves = this.m_taxonset.get().getTaxonCount();
			leafNames = this.m_taxonset.get().asStringList();
		} else {
			nLeaves = nLeavesInput.get().getValue();
			for (int i = 0; i < nLeaves; i++)
				leafNames.add(String.valueOf(i));
		}

		// Set leaf times if specified:
		List<TraitSet> traits = new ArrayList<>();
		traits.addAll(m_traitList.get());
		if (m_initial.get() != null) {
			traits.addAll(m_initial.get().m_traitList.get());
		}
		for (TraitSet trait : traits) {
			if (trait.getTraitName().equals(TraitSet.DATE_TRAIT) || trait.getTraitName().equals(TraitSet.DATE_FORWARD_TRAIT)) {
				timeTraitSet = trait;
				break;
			}
		}

		// find MRCA constraints on tree
		taxonSets = new ArrayList<>();
		m_bounds = new ArrayList<>();
		List<MRCAPrior> calibrations = new ArrayList<>();
		List<String> taxonSetIDs = new ArrayList<>();

		for (final Object beastObject : getOutputs()) {
			if (beastObject instanceof MRCAPrior && !calibrations.contains(beastObject) ) {
				calibrations.add((MRCAPrior) beastObject);
			}
		}
		if (m_initial.get() != null) {
			for (final Object beastObject : m_initial.get().getOutputs()) {
				if (beastObject instanceof MRCAPrior && !calibrations.contains(beastObject)) {
					calibrations.add((MRCAPrior) beastObject);
				}
			}
		}

		for (final MRCAPrior prior : calibrations) {
			final TaxonSet taxonSet = prior.taxonsetInput.get();
			if (taxonSet != null && !prior.onlyUseTipsInput.get()) {
				final Set<String> setTaxa = new LinkedHashSet<>();
				if (taxonSet.asStringList() == null) {
					taxonSet.initAndValidate();
				}
				for (final String taxonID : taxonSet.asStringList()) {
					if (!leafNames.contains(taxonID)) {
						throw new IllegalArgumentException("Taxon <" + taxonID + "> could not be found in list of taxa. Choose one of " + leafNames);
					}
					setTaxa.add(taxonID);
				}
				final ParametricDistribution distr = prior.distInput.get();
				final Bound bounds = new Bound();
				if (distr != null) {
					List<BEASTInterface> beastObjects = new ArrayList<>();
					distr.getPredecessors(beastObjects);
					for (int i = beastObjects.size() - 1; i >= 0 ; i--) {
						beastObjects.get(i).initAndValidate();
					}
					try {
						double tLow = distr.inverseCumulativeProbability(0.0);
						double tHi = distr.inverseCumulativeProbability(1.0);
						bounds.lower = getDate(tLow);
						bounds.upper = getDate(tHi);
						if (bounds.lower > bounds.upper && tLow < tHi) {
							// can happen with date-forward trait -- bounds need to be swapped
							double tmp = bounds.lower;
							bounds.lower = bounds.upper;
							bounds.upper = tmp;
						}
					} catch (MathException e) {
						Log.warning.println("In MultiRateTreeFromBDModel, bound on MRCAPrior could not be set " + e.getMessage());
					}
				}

				if (prior.isMonophyleticInput.get() || !Double.isInfinite(bounds.lower) || !Double.isInfinite(bounds.upper)) {
					taxonSets.add(setTaxa);
					m_bounds.add(bounds);
					taxonSetIDs.add(prior.getID());
				}
			}
		}        

		// from RandomTree - assume all calibration constraints are monophyletic
		// from RandomTree - TODO: verify that this is a reasonable assumption
		nConstraints = taxonSets.size();

		// sort constraints such that if taxon set i is subset of taxon set j, then i < j
		for (int i = 0; i < nConstraints; i++) {
			for (int j = i + 1; j < nConstraints; j++) {

				Set<String> intersection = new LinkedHashSet<>(taxonSets.get(i));
				intersection.retainAll(taxonSets.get(j));

				if (intersection.size() > 0) {
					final boolean isSubset = taxonSets.get(i).containsAll(taxonSets.get(j));
					final boolean isSubset2 = taxonSets.get(j).containsAll(taxonSets.get(i));
					// sanity check: make sure either
					// o taxonset1 is subset of taxonset2 OR
					// o taxonset1 is superset of taxonset2 OR
					// o taxonset1 does not intersect taxonset2
					if (!(isSubset || isSubset2)) {
						throw new IllegalArgumentException("333: Don't know how to generate a Random Tree for taxon sets that intersect, " +
								"but are not inclusive. Taxonset " + taxonSetIDs.get(i) + " and " + taxonSetIDs.get(j));
					}
					// swap i & j if b1 subset of b2
					if (isSubset) {
						swap(taxonSets, i, j);
						swap(m_bounds, i, j);
						swap(taxonSetIDs, i, j);
					}
				}
			}
		}

		// build tree of mono constraints such that j is parent of i if i is a subset of j but i+1,i+2,...,j-1 are not.
		// The last one, standing for the virtual "root" of all monophyletic clades is not associated with an actual clade
		final int[] parent = new int[nConstraints];
		children = new ArrayList[nConstraints + 1];
		for (int i = 0; i <= nConstraints; i++) {
			children[i] = new ArrayList<>();
		}
		for (int i = 0; i < nConstraints; i++) {
			int j = i + 1;
			while (j < nConstraints && !taxonSets.get(j).containsAll(taxonSets.get(i))) j++;
			parent[i] = j;
			children[j].add(i);
		}

		// make sure upper bounds of a child does not exceed the upper bound of its parent
		for (int i = nConstraints - 1; i >= 0 ;--i) {
			if (parent[i] < nConstraints) {
				if (m_bounds.get(i).upper > m_bounds.get(parent[i]).upper) {
					m_bounds.get(i).upper = m_bounds.get(parent[i]).upper - 1e-100;
				}
			}
		}

		// set up last constraint with everything
		Set<String> all_taxa = new TreeSet<String>();
		all_taxa.addAll(leafNames);
		taxonSets.add(all_taxa);
		m_bounds.add(new Bound());

		// Construct tree
		this.root = simulateTree();

		HashMap<String,Integer> taxonToNR = new HashMap<>();
		String[] taxa = getTaxaNames();
		for(int k = 0; k < taxa.length; ++k) {
			taxonToNR.put(taxa[k], k);
		}

		setNodesNrs(root, 0, taxonToNR);
		this.root.setParent(null);
		this.nodeCount = this.root.getNodeCount();
		this.internalNodeCount = this.root.getInternalNodeCount();
		this.leafNodeCount = this.root.getLeafNodeCount();

		initArrays();

		// Write tree to disk if requested
		if (outputFileNameInput.get() != null) {
			try (PrintStream pstream = new PrintStream(outputFileNameInput.get())) {
				pstream.println("#nexus\nbegin trees;");
				pstream.println("tree TREE_1 = " + toString() + ";");
				pstream.println("end;");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		initStateNodes();
	}

	/**
	 * Simulates the tree.
	 *
	 * @return the root node
	 */
	private Node simulateTree() {

		// Initialise node creation counter:
		nextNodeNr = 0;
		usedTaxa = new TreeSet<String>();

		MultiRateNode rootNode = simulateSubTree(nConstraints);

		if (mrcaAgeInput.get() != null) {
			rootNode.setHeight(mrcaAgeInput.get());
		}

		// Return sole remaining active node as root:
		return rootNode;
	}

	/**
	 * Simulate sub tree for subset of taxa covered by specified constraint.
	 *
	 * @param constraint the index of the constraint
	 * @return the root node of resulting tree
	 */
	private MultiRateNode simulateSubTree(int constraint) {
		List<MultiRateNode> subTrees = new ArrayList<>();
		for(int subc : children[constraint]) {
			subTrees.add(simulateSubTree(subc));
		}

		for(String taxa : taxonSets.get(constraint)) {
			if(!usedTaxa.contains(taxa)) {
				MultiRateNode node = new MultiRateNode();
				node.setNr(nextNodeNr);
				node.setID(taxa);
				
				List<TraitSet> traitSets;
				if (m_initial.get() != null) traitSets = m_initial.get().m_traitList.get();
                else traitSets = m_traitList.get();                
				for (TraitSet traitSet : traitSets) {
					node.setMetaData(traitSet.getTraitName(), traitSet.getValue(node.getID()));
				}

				nextNodeNr++;
				subTrees.add(node);
				usedTaxa.add(taxa);
			}
		}

		Double maxHeight = m_bounds.get(constraint).upper;
		Double minHeight = m_bounds.get(constraint).lower;

		MultiRateNode rootNode;
		if(subTrees.size() == 1) rootNode = subTrees.get(0);
		else rootNode = simulateMerge(subTrees, minHeight, maxHeight);

		return rootNode;
	}

	/**
	 * Simulate merge of list of subtrees.
	 *
	 * @param subTrees the subtrees
	 * @param minHeight the min height of the merge
	 * @param maxHeight the max height of the merge
	 * @return the root node of resulting tree
	 */
	private MultiRateNode simulateMerge(List<MultiRateNode> subTrees, Double minHeight, Double maxHeight) {

		List<MultiRateNode> activeNodes = new ArrayList<>();
		double time = Double.POSITIVE_INFINITY;
		for(MultiRateNode node : subTrees) {
			if(node.getHeight() < time) {
				time = node.getHeight() + 1e-8;
			}
		}
		while(activeNodes.size() + subTrees.size() > 1) {
			for(MultiRateNode node : subTrees) {
				if(node.getHeight() < time) {
					activeNodes.add(node);
				}
			}
			for(MultiRateNode node : activeNodes) subTrees.remove(node);

			double specProp = activeNodes.size() * lambda;
			double nextTime;
			if (specProp > 0) nextTime = Randomizer.nextExponential(specProp);
			else nextTime = time * 0.1;

			if(time + nextTime > maxHeight) time = (time + maxHeight)/2;
			else if(activeNodes.size() + subTrees.size() == 2 && time < minHeight) time = minHeight + 1e-8;
			else time += nextTime;

			if(activeNodes.size() > 1) {
				int n1, n2 = Randomizer.nextInt(activeNodes.size());
				do n1 = Randomizer.nextInt(activeNodes.size());
				while(n1 == n2);

				MultiRateNode node = joinNodes(activeNodes.get(n1), activeNodes.get(n2), time);
				activeNodes.add(node);
				activeNodes.remove(Math.max(n1, n2));
				activeNodes.remove(Math.min(n1, n2));
			}
		}

		return activeNodes.get(0);
	}

	/**
	 * Creates a node with specified children and height.
	 *
	 * @param child1 the child 1
	 * @param child2 the child 2
	 * @param time the height of the new node
	 * @return the parent node
	 */
	private MultiRateNode joinNodes(MultiRateNode child1, MultiRateNode child2, double time) {

		// Create new parent node with appropriate ID and time:
		MultiRateNode parent = new MultiRateNode();
		parent.setNr(nextNodeNr);
		parent.setID(String.valueOf(nextNodeNr));
		parent.setHeight(time);
		nextNodeNr++;

		// Connect new parent to children:
		parent.addChild(child1);
		parent.addChild(child2);
		child2.setParent(parent);
		child1.setParent(parent);

		// Ensure new parent is set to correct colour:
		parent.setNodeState(0);

		return parent;
	}

	@Override
	public void initStateNodes() {
		if (m_initial.get() != null) {
			m_initial.get().assignFromWithoutID(this);
		}
	}

	/**
	 * Gets the initialised state nodes.
	 *
	 * @param stateNodeList the state node list
	 * @return the initialised state nodes
	 */
	@Override
	public void getInitialisedStateNodes(List<StateNode> stateNodeList) {
		if (m_initial.get() != null) {
			stateNodeList.add(m_initial.get());
		}
	}

	/**
	 * Swap two items in list.
	 *
	 * @param list the list
	 * @param i the first index
	 * @param j the second index
	 */
	// everything below is from RandomTree
	@SuppressWarnings({"rawtypes", "unchecked"})
	private void swap(final List list, final int i, final int j) {
		final Object tmp = list.get(i);
		list.set(i, list.get(j));
		list.set(j, tmp);
	}

	/**
	 * Class storing upper and lower height values for a constraint.
	 */
	class Bound {
		Double upper = Double.POSITIVE_INFINITY;
		Double lower = Double.NEGATIVE_INFINITY;

		@Override
		public String toString() {
			return "[" + lower + "," + upper + "]";
		}
	}

	/**
	 * Sets the proper nodes nrs for a subtree.
	 *
	 * @param node the root node of subtree
	 * @param nextInternalNr the next nr to use for internal nodes
	 * @return the updated next internal nr
	 */
	private int setNodesNrs(final Node node, int nextInternalNr, Map<String,Integer> initial) {
		if( node.isLeaf() )  {
			node.setNr(initial.get(node.getID()));
		} else {
			for (final Node child : node.getChildren()) {
				nextInternalNr = setNodesNrs(child, nextInternalNr, initial);
			}
			node.setNr(nLeaves + nextInternalNr);
			nextInternalNr += 1;
		}
		return nextInternalNr;
	}

	@Override
	public String[] getTaxaNames() {
		if (m_sTaxaNames == null) {
			final List<String> taxa = m_taxonset.get().asStringList();
			m_sTaxaNames = taxa.toArray(new String[taxa.size()]);
		}
		return m_sTaxaNames;
	}

}
