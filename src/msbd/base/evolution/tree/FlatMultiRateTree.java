/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.base.evolution.tree;

import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.evolution.tree.Tree;

/**
 * Class that converts a MultiRateTree to a standard BEAST tree object, on which single-child nodes are used to
 * represent each colour change.
 */
@Description("A standard BEAST tree representation of a MultiRateTree object.")
public class FlatMultiRateTree extends Tree {

	public Input<MultiRateTree> multiRateTreeInput = new Input<MultiRateTree>("multiRate", "Multi-Rate tree to flatten.");

	protected MultiRateTree multiRateTree;

	/**
	 * Instantiates a new flat multi-rate tree.
	 */
	public FlatMultiRateTree() {};

	@Override
	public void initAndValidate() {
		multiRateTree = multiRateTreeInput.get();
		setRoot(multiRateTree.getFlattenedTree().getRoot());
		initArrays();
	}

}
