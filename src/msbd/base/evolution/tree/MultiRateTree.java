/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package msbd.base.evolution.tree;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import beast.base.core.Input;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.Tree;
import beast.base.evolution.tree.TreeParser;
import beast.base.inference.StateNode;
import beast.base.inference.StateNodeInitialiser;
import com.google.common.collect.Lists;

/**
 * The MultiRateTree encodes a tree structure and the associated colouring.
 */
public class MultiRateTree extends Tree {

	// inputs used by Beauti for activating operators
	public Input<Boolean> estimateTopologyInput = new Input<>("estimateTopology",
			"Whether to estimate the topology of the tree. If false, only estimate the colouring.", true);

	// Will be used to read/write in newick format
	private String stateLabel = "State";

	/**
	 * Instantiates a new multi-rate tree.
	 */
	public MultiRateTree() {}

	/**
	 * Instantiates a new multi-rate tree.
	 *
	 * @param rootNode the root node
	 */
	public MultiRateTree(Node rootNode) {

		if (!(rootNode instanceof MultiRateNode)) throw new IllegalArgumentException("Attempted to instantiate " + "multi-Rate tree with regular root node.");
		setRoot(rootNode);
		initArrays();
	}

	@Override
	public void initAndValidate() {
		if (this.estimateTopologyInput.get()) this.isEstimatedInput.setValue(true, this);

		if (m_initial.get() != null && !(this instanceof StateNodeInitialiser)) {

			if (!(m_initial.get() instanceof MultiRateTree)) {
				throw new IllegalArgumentException("Attempted to initialise " + "multi-Rate tree with regular tree object.");
			}
		}

		if (nodeCount < 0) {
			if (m_taxonset.get() != null) {
				// make a caterpillar
				List<String> sTaxa = m_taxonset.get().asStringList();
				Node left = new MultiRateNode();
				left.setNr(0);
				left.setHeight(0);
				left.setID(sTaxa.get(0));
				for (int i = 1; i < sTaxa.size(); i++) {
					Node right = new MultiRateNode();
					right.setNr(i);
					right.setHeight(0);
					right.setID(sTaxa.get(i));
					Node parent = new MultiRateNode();
					parent.setNr(sTaxa.size() + i - 1);
					parent.setHeight(i);
					left.setParent(parent);
					parent.addChild(left);
					right.setParent(parent);
					parent.addChild(right);
					left = parent;
				}
				root = left;
				leafNodeCount = sTaxa.size();
				nodeCount = leafNodeCount * 2 - 1;
				internalNodeCount = leafNodeCount - 1;
			} else {
				// make dummy tree with a single root node
				root = new MultiRateNode();
				root.setNr(0);
				root.setTree(this);
				nodeCount = 1;
				internalNodeCount = 0;
				leafNodeCount = 1;
			}
		}

		if (nodeCount >= 0) {
			initArrays();
		}

		processTraits(m_traitList.get());

		// Ensure tree is compatible with traits.
		if (hasDateTrait()) adjustTreeNodeHeights(root);

		if (!this.isValid()) throw new IllegalStateException("Can't initialize tree with invalid colouring");
	}

	/**
	 * Inits the node arrays.
	 */
	@Override
	public final void initArrays() {
		// initialise tree-as-array representation + its stored variant
		m_nodes = new MultiRateNode[nodeCount];
		listNodes((MultiRateNode) root, (MultiRateNode[]) m_nodes);
		m_storedNodes = new MultiRateNode[nodeCount];
		Node copy = root.copy();
		listNodes((MultiRateNode) copy, (MultiRateNode[]) m_storedNodes);
	}

	/**
	 * Convert a multi-Rate tree to an array representation.
	 *
	 * @param node Root of sub-tree to convert.
	 * @param nodes Array to populate with tree nodes.
	 */
	private void listNodes(Node node, Node[] nodes) {
		nodes[node.getNr()] = node;
		node.setTree(this);
		for (Node child : node.getChildren()) {
			listNodes(child, nodes);
		}
	}

	/**
	 * Deep copy, returns a completely new multi-rate tree.
	 *
	 * @return a deep copy of this multi-rate tree
	 */
	@Override
	public MultiRateTree copy() {
		MultiRateTree tree = new MultiRateTree();
		tree.ID = ID;
		tree.index = index;
		tree.root = root.copy();
		tree.nodeCount = nodeCount;
		tree.internalNodeCount = internalNodeCount;
		tree.leafNodeCount = leafNodeCount;
		tree.setStateLabel(stateLabel);
		return tree;
	}

	/**
	 * Copy all values from an existing multi-rate tree.
	 *
	 * @param other the existing tree
	 */
	@Override
	public void assignFrom(StateNode other) {
		MultiRateTree mtTree = (MultiRateTree) other;

		MultiRateNode[] mtNodes = new MultiRateNode[mtTree.getNodeCount()];
		for (int i = 0; i < mtTree.getNodeCount(); i++)
			mtNodes[i] = new MultiRateNode();

		ID = mtTree.ID;
		root = mtNodes[mtTree.root.getNr()];
		root.assignFrom(mtNodes, mtTree.root);
		root.setParent(null);

		nodeCount = mtTree.nodeCount;
		internalNodeCount = mtTree.internalNodeCount;
		leafNodeCount = mtTree.leafNodeCount;
		initArrays();
	}

	/**
	 * Copy all values aside from IDs from an existing multi-rate tree.
	 *
	 * @param other the existing tree
	 */
	@Override
	public void assignFromFragile(StateNode other) {
		MultiRateTree mtTree = (MultiRateTree) other;
		if (m_nodes == null) {
			initArrays();
		}
		root = m_nodes[mtTree.root.getNr()];
		Node[] otherNodes = mtTree.m_nodes;
		int iRoot = root.getNr();
		assignFromFragileHelper(0, iRoot, otherNodes);
		root.setHeight(otherNodes[iRoot].getHeight());
		root.setParent(null);

		MultiRateNode mtRoot = (MultiRateNode) root;
		mtRoot.nodeState = ((MultiRateNode) (otherNodes[iRoot])).nodeState;
		mtRoot.changeTimes.clear();
		mtRoot.changeStates.clear();
		mtRoot.nStateChanges = 0;

		root.removeAllChildren(false);
		for (Node child : otherNodes[iRoot].getChildren()) {
			root.addChild(m_nodes[child.getNr()]);
		}

		assignFromFragileHelper(iRoot + 1, nodeCount, otherNodes);
	}

	/**
	 * helper to assignFromFragile *.
	 *
	 * @param iStart the start index
	 * @param iEnd the end index
	 * @param otherNodes the other nodes
	 */
	private void assignFromFragileHelper(int iStart, int iEnd, Node[] otherNodes) {
		for (int i = iStart; i < iEnd; i++) {
			MultiRateNode sink = (MultiRateNode) m_nodes[i];
			MultiRateNode src = (MultiRateNode) otherNodes[i];
			sink.setHeight(src.getHeight());
			sink.setParent(m_nodes[src.getParent().getNr()]);

			sink.nStateChanges = src.nStateChanges;
			sink.changeTimes.clear();
			sink.changeTimes.addAll(src.changeTimes);
			sink.changeStates.clear();
			sink.changeStates.addAll(src.changeStates);
			sink.nodeState = src.nodeState;

			sink.removeAllChildren(false);;
			for (Node child : src.getChildren()) {
				sink.addChild(m_nodes[child.getNr()]);
			}
		}
	}

	/**
	 * Check whether the colouring and timing of tree are coherent.
	 * 
	 * @return true if states and times are "valid"
	 */
	public boolean isValid() {
		return timesAreValid(root) && statesAreValid(root);
	}

	/**
	 * Check if times are valid on edges descending from a node.
	 *
	 * @param node the node
	 * @return true, if times are valid
	 */
	private boolean timesAreValid(Node node) {
		for (Node child : node.getChildren()) {
			double lastHeight = node.getHeight();
			for (int idx = ((MultiRateNode) child).getChangeCount() - 1; idx >= 0; idx--) {
				double thisHeight = ((MultiRateNode) child).getChangeTime(idx);
				if (thisHeight > lastHeight) return false;
				lastHeight = thisHeight;
			}
			if (child.getHeight() > lastHeight) return false;

			if (!timesAreValid(child)) return false;
		}

		return true;
	}

	/**
	 * Check if states are valid on edges descending from a node.
	 *
	 * @param node the node
	 * @return true, if states are valid
	 */
	private boolean statesAreValid(Node node) {
		for (Node child : node.getChildren()) {
			if (((MultiRateNode) node).getNodeState() != ((MultiRateNode) child).getFinalState()) return false;

			if (!statesAreValid(child)) return false;
		}

		return true;
	}

	/**
	 * Generates a new tree in which the colours along the branches are indicated by the traits of single-child nodes.
	 *
	 * This method is useful for interfacing trees coloured externally using a ColouredTree instance with methods
	 * designed to act on trees coloured using single-child nodes and their metadata fields.
	 *
	 * Caveat: assumes more than one node exists on tree (i.e. leaf != root)
	 *
	 * @return Flattened tree.
	 */
	public Tree getFlattenedTree() {

		// Create new tree to modify. Note that copy() doesn't
		// initialise the node array lists, so initArrays() must
		// be called manually.
		Tree flatTree = copy();
		flatTree.initArrays();

		int nextNodeNr = getNodeCount();
		Node colourChangeNode;

		for (Node node : getNodesAsArray()) {

			MultiRateNode mtNode = (MultiRateNode) node;

			int nodeNum = node.getNr();

			if (node.isRoot()) {
				flatTree.getNode(nodeNum).setMetaData(getStateLabel(), ((MultiRateNode) (node.getChild(0))).getFinalState());
				continue;
			}

			Node startNode = flatTree.getNode(nodeNum);

			startNode.setMetaData(getStateLabel(), ((MultiRateNode) node).getNodeState());
			startNode.metaDataString = String.format("%s=%d", getStateLabel(), mtNode.getNodeState());

			Node endNode = startNode.getParent();

			endNode.setMetaData(getStateLabel(), ((MultiRateNode) node.getParent()).getNodeState());
			endNode.metaDataString = String.format("%s=%d", getStateLabel(), ((MultiRateNode) node.getParent()).getNodeState());

			Node branchNode = startNode;
			for (int i = 0; i < mtNode.getChangeCount(); i++) {

				// Create and label new node:
				colourChangeNode = new MultiRateNode();
				colourChangeNode.setNr(nextNodeNr);
				colourChangeNode.setID(String.valueOf(nextNodeNr));
				nextNodeNr += 1;
				flatTree.setNodeCount(flatTree.getNodeCount() + 1);

				// Connect to child and parent:
				branchNode.setParent(colourChangeNode);
				colourChangeNode.addChild(branchNode);

				// Ensure height and colour trait are set:
				colourChangeNode.setHeight(mtNode.getChangeTime(i));
				colourChangeNode.setMetaData(getStateLabel(), mtNode.getChangeState(i));
				colourChangeNode.metaDataString = String.format("%s=%d", getStateLabel(), mtNode.getChangeState(i));

				// Update branchNode:
				branchNode = colourChangeNode;
			}

			// Ensure final branchNode is connected to the original parent:
			branchNode.setParent(endNode);
			endNode.removeChild(startNode);
			endNode.addChild(branchNode);
		}
		
		// update arrays again with new nodes
		flatTree.initArrays();
		return flatTree;
	}

	/**
	 * Initialise colours and tree topology from Tree object in which colour changes are marked by single-child nodes
	 * and colours are stored in meta-data tags. Node numbers of non-singleton nodes in flat tree are preserved.
	 *
	 * @param flatTree the flat tree
	 * @param keepIDs whether to keep the node ids from the flat tree
	 */
	public void initFromFlatTree(Tree flatTree, boolean keepIDs) {

		// Build new coloured tree:

		List<Node> activeFlatTreeNodes = new ArrayList<>();
		List<Node> nextActiveFlatTreeNodes = new ArrayList<>();
		List<MultiRateNode> activeTreeNodes = new ArrayList<>();
		List<MultiRateNode> nextActiveTreeNodes = new ArrayList<>();

		// Populate active node lists with root:
		activeFlatTreeNodes.add(flatTree.getRoot());
		MultiRateNode newRoot = new MultiRateNode();
		activeTreeNodes.add(newRoot);

		while (!activeFlatTreeNodes.isEmpty()) {

			nextActiveFlatTreeNodes.clear();
			nextActiveTreeNodes.clear();

			for (int idx = 0; idx < activeFlatTreeNodes.size(); idx++) {
				Node flatTreeNode = activeFlatTreeNodes.get(idx);
				MultiRateNode treeNode = activeTreeNodes.get(idx);

				List<Integer> colours = new ArrayList<>();
				List<Double> times = new ArrayList<>();

				while (flatTreeNode.getChildCount() == 1) {
					// dirty hack :)
					// this is needed because beast writes to Newick by adding "&" to the metadata
					int col;
					Object test = flatTreeNode.getMetaData(getStateLabel());
					// not handling the case 'no metadata' here because there are no colour changes in that case
					if (test == null) col = (int) Math.round((Double) flatTreeNode.getMetaData("&" + getStateLabel()));
					else col = (int) Math.round((Double) test);

					// this is to handle manually inputed trees which may or may not be clean
					if (colours.isEmpty() || col != colours.get(colours.size() - 1)) {
						colours.add(col);
						times.add(flatTreeNode.getHeight());
					}

					flatTreeNode = flatTreeNode.getChild(0);
				}

				// Order changes from youngest to oldest:
				colours = Lists.reverse(colours);
				times = Lists.reverse(times);

				switch (flatTreeNode.getChildCount()) {
				case 0:
					// Leaf at base of branch
					treeNode.setNr(flatTreeNode.getNr());
					
					if (keepIDs) {
						treeNode.setID(String.valueOf(flatTreeNode.getID()));
					} else {
						treeNode.setID(String.valueOf(treeNode.getNr()));
					}
					break;

				case 2:
					// Non-leaf at base of branch
					for (Node child : flatTreeNode.getChildren()) {
						nextActiveFlatTreeNodes.add(child);
					}

					MultiRateNode daughter = new MultiRateNode();
					MultiRateNode son = new MultiRateNode();
					treeNode.addChild(daughter);
					treeNode.addChild(son);

					if (keepIDs) {
						treeNode.setID(String.valueOf(flatTreeNode.getID()));
					}

					nextActiveTreeNodes.add(daughter);
					nextActiveTreeNodes.add(son);

					break;
				}

				// Add state changes to multi-rate tree branch:
				for (int i = 0; i < colours.size(); i++)
					treeNode.addChange(colours.get(i), times.get(i));

				// Set node state at base of multi-rate tree branch:
				int nodeState;
				Object test = flatTreeNode.getMetaData(getStateLabel());
				if (test == null) {
					try {
						nodeState = (int) Math.round((Double) flatTreeNode.getMetaData("&" + getStateLabel()));
					} catch (NullPointerException e) {
						nodeState = 0; // there is no metadata, defaulting to 0
					}
				} else nodeState = (int) Math.round((Double) test);
				treeNode.setNodeState(nodeState);
				if (!colours.isEmpty() && nodeState == treeNode.getChangeState(0)) treeNode.removeChange(0);

				// Set node height:
				treeNode.setHeight(flatTreeNode.getHeight());
			}

			// Replace old active node lists with new:
			activeFlatTreeNodes.clear();
			activeFlatTreeNodes.addAll(nextActiveFlatTreeNodes);

			activeTreeNodes.clear();
			activeTreeNodes.addAll(nextActiveTreeNodes);

		}

		// Number internal nodes:
		numberInternalNodes(newRoot, newRoot.getAllLeafNodes().size(), keepIDs);

		MultiRateTree tmp = new MultiRateTree(newRoot);
		// Assign tree topology:
		assignFromWithoutID(tmp);
		
		leafNodeCount = newRoot.getLeafNodeCount();
        nodeCount = leafNodeCount * 2 - 1;
        internalNodeCount = leafNodeCount - 1;
		initArrays();

	}

	/**
	 * Helper method used by initFromFlattenedTree to assign sensible node numbers to each internal node. This is a
	 * post-order traversal, meaning the root is given the largest number.
	 *
	 * @param node the node to number
	 * @param nextNr the next node number to be assigned
	 * @param keepIDs whether to keep the node ids from the flat tree
	 * @return the next node number
	 */
	private int numberInternalNodes(Node node, int nextNr, boolean keepIDs) {
		if (node.isLeaf()) return nextNr;

		for (Node child : node.getChildren())
			nextNr = numberInternalNodes(child, nextNr, keepIDs);

		node.setNr(nextNr);
		if (!keepIDs) node.setID(String.valueOf(nextNr));

		return nextNr + 1;
	}

	/**
	 * Obtain total number of state changes along nodes on tree.
	 * 
	 * @return total change count
	 */
	public int getTotalNumberOfChanges() {
		int count = 0;

		for (Node node : m_nodes) {
			if (node.isRoot()) continue;

			if (((MultiRateNode) node).getChangeCount() > 0) {
				count += ((MultiRateNode) node).getChangeCount();
			}

		}

		return count;
	}

	/**
	 * Return string representation of multi-rate tree. We use reflection here to determine whether this is being called
	 * as part of writing the state file.
	 *
	 * @return Multi-Rate tree string in Newick format.
	 */
	@Override
	public String toString() {

		// Behaves differently if writing a state file
		StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		if (ste[2].getMethodName().equals("toXML")) {
			// Use toShortNewick to generate Newick string without taxon labels
			return getFlattenedTree().getRoot().toShortNewick(true);
		} else {
			return getFlattenedTree().getRoot().toNewick(false);
		}
	}

	// ///////////////////////////////////////////////
	// StateNode implementation //
	// ///////////////////////////////////////////////

	@Override
	protected void store() {
		storedRoot = m_storedNodes[root.getNr()];
		int iRoot = root.getNr();

		storeNodes(0, iRoot);

		storedRoot.setHeight(m_nodes[iRoot].getHeight());
		storedRoot.setParent(null);

		storedRoot.removeAllChildren(false);
		for (Node child : root.getChildren()) {
			Node c = m_storedNodes[child.getNr()];
			c.setParent(storedRoot);
			storedRoot.addChild(c);
		}

		MultiRateNode mtStoredRoot = (MultiRateNode) storedRoot;
		mtStoredRoot.changeTimes.clear();
		mtStoredRoot.changeTimes.addAll(((MultiRateNode) m_nodes[iRoot]).changeTimes);

		mtStoredRoot.changeStates.clear();
		mtStoredRoot.changeStates.addAll(((MultiRateNode) m_nodes[iRoot]).changeStates);

		mtStoredRoot.nStateChanges = ((MultiRateNode) m_nodes[iRoot]).nStateChanges;
		mtStoredRoot.nodeState = ((MultiRateNode) m_nodes[iRoot]).nodeState;

		storeNodes(iRoot + 1, nodeCount);
	}

	/**
	 * helper to store *.
	 *
	 * @param iStart the start index
	 * @param iEnd the end index
	 */
	private void storeNodes(int iStart, int iEnd) {
		for (int i = iStart; i < iEnd; i++) {
			MultiRateNode sink = (MultiRateNode) m_storedNodes[i];
			MultiRateNode src = (MultiRateNode) m_nodes[i];
			sink.setHeight(src.getHeight());
			sink.setParent(m_storedNodes[src.getParent().getNr()]);
			
			final List<Node> children = sink.getChildren();
            final List<Node> srcChildren = src.getChildren();
            
            if( children.size() == srcChildren.size() ) {
                // shave some more time by avoiding list clear and add
                for (int k = 0; k < children.size(); ++k) {
                    // don't call addChild, which calls  setParent(..., true);
                    final Node c = m_storedNodes[srcChildren.get(k).getNr()];
                    c.setParent(sink);
                    sink.setChild(k, c);
                }
             } else {
                 children.clear();
                 for (final Node srcChild : srcChildren) {
                     // don't call addChild, which calls  setParent(..., true);
                     final Node c = m_storedNodes[srcChild.getNr()];
                     sink.addChild(c);
                 }
             }

			sink.changeTimes.clear();
			sink.changeTimes.addAll(src.changeTimes);

			sink.changeStates.clear();
			sink.changeStates.addAll(src.changeStates);

			sink.nStateChanges = src.nStateChanges;
			sink.nodeState = src.nodeState;
		}
	}

	// ///////////////////////////////////////////////
	// Methods implementing the Loggable interface //
	// ///////////////////////////////////////////////

	@Override
	public void init(PrintStream printStream) {

		printStream.println("#NEXUS\n");
		printStream.println("Begin taxa;");
		printStream.println("\tDimensions ntax=" + getLeafNodeCount() + ";");
		printStream.println("\t\tTaxlabels");
		for (int i = 0; i < getLeafNodeCount(); i++)
			printStream.println("\t\t\t" + getNodesAsArray()[i].getID());
		printStream.println("\t\t\t;");
		printStream.println("End;");

		printStream.println("Begin trees; \n");
		printStream.println("\tTranslate");
        for (int i = 0; i<getLeafNodeCount(); i++) {
            printStream.print("\t\t\t"+(getNodesAsArray()[i].getNr()+1)
                    +" "+getNodesAsArray()[i].getID());
            if (i<getLeafNodeCount()-1)
                printStream.print(",");
            printStream.print("\n");
        }
        printStream.print("\t\t\t;");
	}

	@Override
	public void log(long i, PrintStream printStream) {
		printStream.print("tree STATE_" + i + " = ");
		printStream.print(toString());
		printStream.print(";");

	}
	
	/**
	 * Gets the total length.
	 *
	 * @return the total length
	 */
	public double getTotalLength() {
		double length = 0.0;
		for (Node node : this.m_nodes) {
			if (node.isRoot()) continue;
			length += node.getLength();
		}
		return length;
	}

	/**
	 * Gets the state label.
	 *
	 * @return the state label
	 */
	public String getStateLabel() {
		return stateLabel;
	}

	/**
	 * Sets the state label.
	 *
	 * @param stateLabel the new state label
	 */
	public void setStateLabel(String stateLabel) {
		this.stateLabel = stateLabel;
	}

	// ///////////////////////////////////////////////
	// Serialization and deserialization for state //
	// ///////////////////////////////////////////////

	/**
	 * reconstruct tree from XML fragment in the form of a DOM node *.
	 *
	 * @param node the DOM node
	 */
	@Override
	public void fromXML(org.w3c.dom.Node node) {
		try {
			String sNewick = node.getTextContent();// .replace("&", "");

			TreeParser parser = new TreeParser();
			parser.initByName("IsLabelledNewick", false, "offset", 0, "adjustTipHeights", false, "singlechild", true, "newick", sNewick);
			// parser.m_nThreshold.setValue(1e-10, parser);
			// parser.m_nOffset.setValue(0, parser);

			initFromFlatTree(parser, true);

			initArrays();
		} catch (Exception ex) {
			Logger.getLogger(MultiRateTree.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	@Override
	public int scale(double scale) {
		throw new UnsupportedOperationException();
	}
}
