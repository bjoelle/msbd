/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package models;

import org.junit.Assert;
import org.junit.Test;

import beast.base.inference.parameter.RealParameter;
import msbd.base.inference.parameter.ExtendableRealParameter;
import msbd.evolution.tree.ExpStateChangeModel;
import msbd.evolution.tree.StateChangeModel;

/**
 * Tests that model validation works.
 */
public class ModelValidationTests {

	/**
	 * Test states mismatch in std model.
	 */
	@Test(expected = RuntimeException.class)
	public void testStatesMismatchStd() {
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0 2.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus);
	}

	/**
	 * Test states mismatch in exp model.
	 */
	@Test(expected = RuntimeException.class)
	public void testStatesMismatchExp() {
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0 2.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0 0.1");
		ExtendableRealParameter lambdars = new ExtendableRealParameter();
		lambdars.initByName("value", "2.0 32.0 1.5");
		ExpStateChangeModel expModel = new ExpStateChangeModel();
		expModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "lambdaRates", lambdars);
	}

	/**
	 * Test states mismatch in exp model 2.
	 */
	@Test(expected = RuntimeException.class)
	public void testStatesMismatchExp2() {
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0 2.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0 0.1");
		ExtendableRealParameter murs = new ExtendableRealParameter();
		murs.initByName("value", "2.0 32.0 1.5");
		ExpStateChangeModel expModel = new ExpStateChangeModel();
		expModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "muRates", murs);
	}

	/**
	 * Test states mismatch in exp model 3.
	 */
	@Test(expected = RuntimeException.class)
	public void testStatesMismatchExp3() {
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0 2.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0 0.5");
		ExtendableRealParameter lambdars = new ExtendableRealParameter();
		lambdars.initByName("value", "2.0 32.0 1.5");
		ExtendableRealParameter murs = new ExtendableRealParameter();
		murs.initByName("value", "2.0 2.0 11.5");
		ExpStateChangeModel expModel = new ExpStateChangeModel();
		expModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "lambdaRates", lambdars, "muRates", murs);
	}

	/**
	 * Test sampling exceptions.
	 */
	@Test(expected = Exception.class)
	public void testSamplingExceptions() {
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "rho", 1.5);
	}

	/**
	 * Test sampling exceptions 2.
	 */
	@Test(expected = Exception.class)
	public void testSamplingExceptions2() {
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "rho", -0.1);
	}

	/**
	 * Test sampling exceptions 3.
	 */
	@Test(expected = Exception.class)
	public void testSamplingExceptions3() {
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "sigma", 1.5);
	}

	/**
	 * Test sampling exceptions 4.
	 */
	@Test(expected = Exception.class)
	public void testSamplingExceptions4() {
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "sigma", -0.2);
	}

	/**
	 * Test negative rates (gamma).
	 */
	@Test(expected = Exception.class)
	public void testNegativeRates() {
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "-0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus);
	}

	/**
	 * Test negative rates 2 (lambda).
	 */
	@Test(expected = Exception.class)
	public void testNegativeRates2() {
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "-1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus);
	}

	/**
	 * Test negative rates 3 (mu).
	 */
	@Test(expected = Exception.class)
	public void testNegativeRates3() {
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "-1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus);
	}
	
	/**
	 * Test negative rates 4 (psi).
	 */
	@Test(expected = Exception.class)
	public void testNegativeRates4() {
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		RealParameter psi = new RealParameter();
		psi.initByName("value", "-0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "psi", psi);
	}

	/**
	 * Test multiple gammas.
	 */
	@Test
	public void testMultipleGammas() {
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5 1.0");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus);

		Assert.assertEquals(0.5, stateChangeModel.getGamma(), 0);
		Assert.assertEquals(1, stateChangeModel.gammaInput.get().getDimension());
	}

	/**
	 * Test overflow in setValues.
	 */
	@Test(expected = Exception.class)
	public void testOverflowSetValues() {
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "isLambdaFixed", true);

		Double[] val = { 1.5, 2.0 };
		stateChangeModel.setLambdas(val);
	}

	/**
	 * Test overflow in setValues 2.
	 */
	@Test(expected = Exception.class)
	public void testOverflowSetValues2() {
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "isMuFixed", true);

		Double[] val = { 1.5, 2.0 };
		stateChangeModel.setMus(val);
	}
}
