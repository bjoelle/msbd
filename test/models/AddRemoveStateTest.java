/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package models;

import org.junit.Assert;
import org.junit.Test;

import beast.base.inference.State;
import beast.base.inference.parameter.RealParameter;
import msbd.base.inference.parameter.ExtendableRealParameter;
import msbd.evolution.tree.StateChangeModel;

/**
 * Test add and remove states functions.
 */
public class AddRemoveStateTest {

	/**
	 * Test.
	 */
	@Test
	public void test() {
		// Assemble migration model:
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0 2.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0 0.5");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus);

		// Set up state:
		State state = new State();
		state.initByName("stateNode", lambdas, "stateNode", mus);
		state.initialise();

		stateChangeModel.addState(0.5, 0);
		Assert.assertEquals(0.5, stateChangeModel.getLambda(2), 1e-8);
		Assert.assertEquals(3, stateChangeModel.getNStates());
		stateChangeModel.removeState(0);
		Assert.assertEquals(2.0, stateChangeModel.getLambda(0), 1e-8);
		Assert.assertEquals(0.0, stateChangeModel.getMu(1), 1e-8);
		Assert.assertEquals(2, stateChangeModel.getNStates());
		stateChangeModel.addState(1.0, 1.0);
		stateChangeModel.addState(1.5, 1.0);
		stateChangeModel.removeState(0);
		Assert.assertEquals(1.0, stateChangeModel.getLambda(1), 1e-8);
	}
}
