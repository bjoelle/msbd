/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package trees;

import org.junit.Assert;
import org.junit.Test;

import msbd.base.evolution.tree.MultiRateNode;
import msbd.base.evolution.tree.MultiRateTreeFromNewick;

/**
 * Tests for correct reading & writing of multi-states trees.
 */
public class ReadWriteTreeTest {

	/**
	 * Test reading a tree.
	 */
	@Test
	public void ReadTree() {
		String newickStr = "((4[&State=0]:0.150558013,5[&State=0]:0.150558013)7[&State=0]:1.154521107,(((2[&State=1]:0.2610234442,3[&State=1]:0.2610234442)9[&State=1]:0.02397655557)"
				+ "10[&State=0]:0.006852994,1[&State=0]:0.2918529938)8[&State=0]:1.013226126)6[&State=0];";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", true, "isLabelledNewick", true);

		Assert.assertEquals("4", mtTree.getNode(0).getID());
		Assert.assertEquals(1, ((MultiRateNode) mtTree.getNode(6)).getChangeCount());
		Assert.assertEquals(0, ((MultiRateNode) mtTree.getNode(5)).getNodeState());
	}

	/**
	 * Test writing a tree.
	 */
	@Test
	public void WriteTree() {
		String newickStr = "((4[&State=0]:0.150558013,5[&State=0]:0.150558013)7[&State=0]:1.154521107,(((2[&State=1]:0.2610234442,3[&State=1]:0.2610234442)9[&State=1]:0.02397655557)"
				+ "10[&State=0]:0.006852994,1[&State=0]:0.2918529938)8[&State=0]:1.013226126)6[&State=0];";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", true, "isLabelledNewick", true);

		String newS = mtTree.toString();
		MultiRateTreeFromNewick mt2 = new MultiRateTreeFromNewick();
		mt2.initByName("newick", newS, "adjustTipHeights", true, "isLabelledNewick", true);

		Assert.assertEquals(mtTree.getNode(0).getID(), mt2.getNode(0).getID());
		Assert.assertEquals(((MultiRateNode) mtTree.getNode(6)).getChangeCount(), ((MultiRateNode) mt2.getNode(6)).getChangeCount());
		Assert.assertEquals(((MultiRateNode) mtTree.getNode(5)).getNodeState(), ((MultiRateNode) mt2.getNode(5)).getNodeState());
	}

	/**
	 * Test reading an invalid tree.
	 */
	@Test(expected = RuntimeException.class)
	public void ReadInvalidTree() {
		String newickStr = "((4[&State=0]:0.150558013,5[&State=0]:0.150558013)7[&State=0]:1.154521107,((((2[&State=1]:0.2610234442,3[&State=1]:0.2610234442)9[&State=1]:0.02397655557)"
				+ "10[&State=0]:0.003852994)11[&State=1]:0.003,1[&State=0]:0.2918529938)8[&State=0]:1.013226126)6[&State=0];";

		MultiRateTreeFromNewick mrTree = new MultiRateTreeFromNewick();
		mrTree.initByName("newick", newickStr, "adjustTipHeights", true);
	}

	/**
	 * Test reading a tree with redundant changes.
	 */
	@Test
	public void ReadRedundantChanges() {
		String newickStr = "((4[&State=0]:0.150558013,5[&State=0]:0.150558013)7[&State=0]:1.154521107,((((2[&State=1]:0.2610234442,3[&State=1]:0.2610234442)9[&State=1]:0.02397655557)"
				+ "10[&State=0]:0.003852994)11[&State=0]:0.003,1[&State=0]:0.2918529938)8[&State=0]:1.013226126)6[&State=0];";

		MultiRateTreeFromNewick mrTree = new MultiRateTreeFromNewick();
		mrTree.initByName("newick", newickStr, "adjustTipHeights", true);
		Assert.assertEquals(1, mrTree.getTotalNumberOfChanges());
	}

	/**
	 * Test reading a tree with SA.
	 */
	@Test
	public void ReadSATree() {
		String newickStr = "((3 : 1.5, 4 : 0) : 1 , (1 : 2, 2 : 1) : 3);";
		
		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", false);
		Assert.assertEquals(1, mtTree.getDirectAncestorNodeCount());
	}
}
