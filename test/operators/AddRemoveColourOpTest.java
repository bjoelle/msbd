/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package operators;


import org.junit.Assert;
import org.junit.Test;

import beast.base.inference.State;
import beast.base.inference.parameter.RealParameter;
import beast.base.inference.distribution.Uniform;
import beast.base.util.Randomizer;
import msbd.base.evolution.tree.MultiRateNode;
import msbd.base.evolution.tree.MultiRateTreeFromNewick;
import msbd.base.inference.parameter.ExtendableRealParameter;
import msbd.distributions.StdBirthDeathDensity;
import msbd.evolution.operator.AddRemoveColourWPropagation;
import msbd.evolution.tree.StateChangeModel;

/**
 * Test the AddRemoveState operator.
 */
// TODO add a check that node numbers are correct when removing a state in the middle of the table
public class AddRemoveColourOpTest {

	/**
	 * Test HR calculation.
	 */
	@Test
	public void testHR() {
		Randomizer.setSeed(2);
		String newickStr = "((4:0.150558013,5:0.150558013)7:1.154521107,((2:0.2610234442,3:0.2610234442)9:0.03082954957,1:0.2918529938)8:1.013226126)6;";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", true);

		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "1.0");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus);

		Uniform draw = new Uniform();
		draw.initByName("upper", 5.0);
		AddRemoveColourWPropagation operator = new AddRemoveColourWPropagation();
		operator.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mtTree, "lambdaDistribution", draw, "muDistribution", draw,
				"weight", 1.0);

		StdBirthDeathDensity likelihood = new StdBirthDeathDensity();
		likelihood.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mtTree, "checkValidity", true);

		State state = new State();
		state.initByName("stateNode", mtTree, "stateNode", lambdas, "stateNode", mus, "stateNode", gamma);
		state.initialise();

		double logp1 = likelihood.calculateLogP();
		
		double HRadd = operator.proposal();
		double HRrem = operator.proposal();
		Assert.assertEquals(HRadd, -HRrem, 1e-6);
		
		double logp2 = likelihood.calculateLogP();
		Assert.assertEquals(logp1, logp2, 1e-6);
	}

	/**
	 * Test adding move.
	 */
	@Test
	public void test_add() {
		Randomizer.setSeed(2);
		// Assemble test MultiRateTree:
		String newickStr = "((4[&State=0]:0.150558013,5[&State=0]:0.150558013)7[&State=0]:1.154521107,(((2[&State=1]:0.2610234442,3[&State=1]:0.2610234442)9[&State=1]:0.02397655557)"
				+ "10[&State=0]:0.006852994,1[&State=0]:0.2918529938)8[&State=0]:1.013226126)6[&State=0];";

		MultiRateTreeFromNewick mrTree = new MultiRateTreeFromNewick();
		mrTree.initByName("newick", newickStr, "adjustTipHeights", true);

		// Assemble rate shift model:
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0 10.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "0.0 1.0");

		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus);

		// Set up operator:
		Uniform draw = new Uniform();
		draw.initByName("upper", 5.0);
		AddRemoveColourWPropagation operator = new AddRemoveColourWPropagation();
		operator.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mrTree, "lambdaDistribution", draw, "muDistribution", draw,
				"weight", 1.0);

		State state = new State();
		state.initByName("stateNode", mrTree, "stateNode", lambdas, "stateNode", mus, "stateNode", gamma);
		state.initialise();

		operator.proposal();
		Assert.assertEquals(3, stateChangeModel.getNStates());
		Assert.assertEquals(2, ((MultiRateNode) mrTree.getNode(3)).getNodeState());
	}

	/**
	 * Test removing move.
	 */
	@Test
	public void test_remove() {
		Randomizer.setSeed(1);
		// Assemble test MultiRateTree:
		String newickStr = "((4[&State=0]:0.150558013,5[&State=0]:0.150558013)7[&State=0]:1.154521107,(((2[&State=1]:0.2610234442,3[&State=1]:0.2610234442)9[&State=1]:0.02397655557)"
				+ "10[&State=0]:0.006852994,1[&State=0]:0.2918529938)8[&State=0]:1.013226126)6[&State=0];";

		MultiRateTreeFromNewick mrTree = new MultiRateTreeFromNewick();
		mrTree.initByName("newick", newickStr, "adjustTipHeights", true);

		// Assemble rate shift model:
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0 10.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "0.0 1.0");

		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus);

		// Set up operator:
		Uniform draw = new Uniform();
		draw.initByName("upper", 5.0);
		AddRemoveColourWPropagation operator = new AddRemoveColourWPropagation();
		operator.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mrTree, "lambdaDistribution", draw, "muDistribution", draw,
				"weight", 1.0);

		State state = new State();
		state.initByName("stateNode", mrTree, "stateNode", lambdas, "stateNode", mus, "stateNode", gamma);
		state.initialise();

		operator.proposal();

		Assert.assertEquals(1, stateChangeModel.getNStates());
		Assert.assertEquals(0, ((MultiRateNode) mrTree.getNode(2)).getNodeState());
	}
}
