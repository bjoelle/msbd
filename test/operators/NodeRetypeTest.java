/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package operators;

import org.junit.Assert;
import org.junit.Test;

import beast.base.inference.State;
import beast.base.inference.parameter.IntegerParameter;
import beast.base.inference.parameter.RealParameter;
import beast.base.util.Randomizer;
import msbd.base.evolution.tree.MultiRateNode;
import msbd.base.evolution.tree.MultiRateTreeFromNewick;
import msbd.base.inference.parameter.ExtendableRealParameter;
import msbd.evolution.operator.NodeRetype;
import msbd.evolution.tree.StateChangeModel;
/**
 * Test the NodeRetype operator.
 */
public class NodeRetypeTest {

	/**
	 * Test operator. From the MultiTypeTree package.
	 */
	@Test
	public void test() {

		Randomizer.setSeed(1);
		// Assemble initial MultiRateTree
		String newickStr = "((1:50,2:50)4:150,3:200)5;";

		MultiRateTreeFromNewick mrTree = new MultiRateTreeFromNewick();
		mrTree.initByName("newick", newickStr);

		// Assemble migration model:
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0 2.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0 0.5");
		IntegerParameter nstar = new IntegerParameter();
		nstar.initByName("value","2");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "nstar", nstar);

		// Set up state:
		State state = new State();
		state.initByName("stateNode", mrTree);

		NodeRetype op = new NodeRetype();
		op.initByName("multiRateTree", mrTree, "stateChangeModel", stateChangeModel, "weight", 1.0);

		double[] expQ = { -0.5, 0.5, 0.5, -0.5 };
		Assert.assertArrayEquals(expQ, op.getQ(false).toArray(), 1e-5);

		op.proposal();

		Assert.assertEquals(1, ((MultiRateNode) mrTree.getNode(1)).getNodeState());
		Assert.assertEquals(25, ((MultiRateNode) mrTree.getNode(1)).getChangeCount());
	}
}
