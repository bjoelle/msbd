package operators;

import org.junit.Assert;
import org.junit.Test;

import beast.base.inference.State;
import beast.base.inference.parameter.RealParameter;
import beast.base.util.Randomizer;
import msbd.base.evolution.tree.MultiRateTreeFromNewick;
import msbd.base.inference.parameter.ExtendableRealParameter;
import msbd.distributions.StdBirthDeathDensity;
import msbd.evolution.operator.MRTLeafToSAJump;
import msbd.evolution.tree.StateChangeModel;
/**
 * Test class for the MRT SA <-> leaf operator.
 */
public class MRTSALeafOpTest {
	
	/**
	 * Test HR calculation.
	 */
	@Test
	public void testHR() {
		String newickStr = "((4:0.150558013,5:0.150558013)7:1.154521107,((2:0.2610234442,3:0.2610234442)9:0.03082954957,1:0.02)8:1.013226126)6;";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", false);

		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "1.0");
		RealParameter sigma = new RealParameter();
		sigma.initByName("value", "1.0");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "sigma", sigma);

		MRTLeafToSAJump operator = new MRTLeafToSAJump();
		operator.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mtTree, "weight", 1.0);

		StdBirthDeathDensity likelihood = new StdBirthDeathDensity();
		likelihood.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mtTree, "checkValidity", true);

		State state = new State();
		state.initByName("stateNode", mtTree, "stateNode", lambdas, "stateNode", mus, "stateNode", gamma, "stateNode", sigma);
		state.initialise();
		
		Randomizer.setSeed(2);

		Assert.assertEquals(0, mtTree.getDirectAncestorNodeCount());		
		double HRadd = operator.proposal();
		Assert.assertEquals(1, mtTree.getDirectAncestorNodeCount());
		double HRrem = operator.proposal();
		Assert.assertEquals(0, mtTree.getDirectAncestorNodeCount());
		
		Assert.assertEquals(HRadd, -HRrem, 1e-6);
	}
}
