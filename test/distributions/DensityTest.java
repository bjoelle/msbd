/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package distributions;

import org.junit.Assert;
import org.junit.Test;

import beast.base.inference.parameter.IntegerParameter;
import beast.base.inference.parameter.RealParameter;
import msbd.base.evolution.tree.MultiRateNode;
import msbd.base.evolution.tree.MultiRateTree;
import msbd.base.evolution.tree.MultiRateTreeFromNewick;
import msbd.base.inference.parameter.ExtendableRealParameter;
import msbd.distributions.ExpBirthDeathDensity;
import msbd.distributions.StdBirthDeathDensity;
import msbd.evolution.tree.ExpStateChangeModel;
import msbd.evolution.tree.StateChangeModel;

/**
 * Test likelihood calculations.
 */
public class DensityTest {

	/**
	 * Test initialization with invalid tree.
	 */
	@Test
	public void testInvalidTree() {
		String newickStr = "((4[&State=0]:0.150558013,5[&State=0]:0.150558013)7[&State=0]:1.154521107,(((2[&State=1]:0.2610234442,3[&State=1]:0.2610234442)9[&State=1]:0.02397655557)"
				+ "10[&State=0]:0.006852994,1[&State=0]:0.2918529938)8[&State=0]:1.013226126)6[&State=0];";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", true);
		MultiRateTree mt2 = mtTree, mt3 = mtTree, mt4 = mtTree;

		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus);

		((MultiRateNode) mtTree.getNode(5)).addChange(1, 0.3);
		StdBirthDeathDensity likelihood = new StdBirthDeathDensity();
		likelihood.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mtTree, "checkValidity", true);
		Assert.assertEquals(Double.NEGATIVE_INFINITY, likelihood.calculateLogP(), 1e-10);

		((MultiRateNode) mt2.getNode(5)).removeChange(0);
		StdBirthDeathDensity likelihood2 = new StdBirthDeathDensity();
		likelihood2.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mt2, "checkValidity", true);
		Assert.assertEquals(Double.NEGATIVE_INFINITY, likelihood2.calculateLogP(), 1e-10);

		((MultiRateNode) mt3.getNode(5)).setChangeTime(0, 0.5);
		StdBirthDeathDensity likelihood3 = new StdBirthDeathDensity();
		likelihood3.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mt3, "checkValidity", true);
		Assert.assertEquals(Double.NEGATIVE_INFINITY, likelihood3.calculateLogP(), 1e-10);
		
		mt4.getNode(5).setHeight(0.2);
		StdBirthDeathDensity likelihood4 = new StdBirthDeathDensity();
		likelihood4.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mt4);
		Assert.assertEquals(Double.NEGATIVE_INFINITY, likelihood4.calculateLogP(), 1e-10);
	}

	/**
	 * Test one state std likelihood, happy case.
	 */
	@Test
	public void testOneStateStdHappyCase() {
		// Assemble test tree:
		String newickStr = "((4:0.150558013,5:0.150558013)7:1.154521107,((2:0.2610234442,3:0.2610234442)9:0.03082954957,1:0.2918529938)8:1.013226126)6;";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", true);

		// Assemble rate shift model:
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus);

		// Set up likelihood instance:
		StdBirthDeathDensity likelihood = new StdBirthDeathDensity();
		likelihood.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mtTree);

		double expected = -6.53629442939 + 4*Math.log(2); // Calculated by R + correction for oriented trees
		double result = likelihood.calculateLogP();

		Assert.assertEquals(expected, result, 1e-5);
	}

	/**
	 * Test one state exp likelihood, happy case.
	 */
	@Test
	public void testOneStateExpHappyCase() {
		// Assemble test tree:
		String newickStr = "((4:0.150558013,5:0.150558013)7:1.154521107,((2:0.2610234442,3:0.2610234442)9:0.03082954957,1:0.2918529938)8:1.013226126)6;";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", true);

		// Assemble rate shift model:
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		ExtendableRealParameter lambdars = new ExtendableRealParameter();
		lambdars.initByName("value", "0.0");
		ExtendableRealParameter murs = new ExtendableRealParameter();
		murs.initByName("value", "0.0");
		ExpStateChangeModel expModel = new ExpStateChangeModel();
		expModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "lambdaRates", lambdars, "muRates", murs);

		// Set up likelihood instance:
		ExpBirthDeathDensity explik = new ExpBirthDeathDensity();
		explik.initByName("stateChangeModel", expModel, "multiRateTree", mtTree, "stepsize", 0.1);

		double expected = -6.53629442939 + 4*Math.log(2); // Calculated by R + correction for oriented trees
		double expResult = explik.calculateLogP();
		Assert.assertEquals(expected, expResult, 1e-5);

		// Assemble rate shift model:
		lambdars = new ExtendableRealParameter();
		lambdars.initByName("value", "0.5");
		murs = new ExtendableRealParameter();
		murs.initByName("value", "0.0");
		expModel = new ExpStateChangeModel();
		expModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "lambdaRates", lambdars, "muRates", murs);

		// Set up likelihood instance:
		explik = new ExpBirthDeathDensity();
		explik.initByName("stateChangeModel", expModel, "multiRateTree", mtTree, "stepsize", 0.1);

		expected = -7.467351 + 4*Math.log(2); // Calculated by R + correction for oriented trees
		expResult = explik.calculateLogP();
		Assert.assertEquals(expected, expResult, 1e-4);
	}

	/**
	 * Test two states std likelihood, happy case.
	 */
	@Test
	public void testTwoStatesStdHappyCase() {
		// Assemble test MultiRateTree
		String newickStr = "((4[&State=0]:0.150558013,5[&State=0]:0.150558013)7[&State=0]:1.154521107,(((2[&State=1]:0.2610234442,3[&State=1]:0.2610234442)9[&State=1]:0.02397655557)"
				+ "10[&State=0]:0.006852994,1[&State=0]:0.2918529938)8[&State=0]:1.013226126)6[&State=0];";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", true);

		// Assemble rate shift model:
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0 10.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "0.0 1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus);

		// Set up likelihood instance:
		StdBirthDeathDensity likelihood = new StdBirthDeathDensity();
		likelihood.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mtTree);

		double expected = -8.136331023568 + 4*Math.log(2); // Calculated by R + correction for oriented trees
		double result = likelihood.calculateLogP();

		Assert.assertEquals(expected, result, 1e-5);
	}
	
	/**
	 * Test three states std likelihood, happy case.
	 */
	@Test
	public void testThreeStatesStdHappyCase() {
		// Assemble test MultiRateTree
		String newickStr = "((((4[&State=0]:0.1)11[&State=2]:0.01)12[&State=0]:0.040558013,5[&State=0]:0.150558013)7[&State=0]:1.154521107,(((2[&State=1]:0.2610234442,3[&State=1]:0.2610234442)9[&State=1]:0.02397655557)"
				+ "10[&State=0]:0.006852994,1[&State=0]:0.2918529938)8[&State=0]:1.013226126)6[&State=0];";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", true);

		// Assemble rate shift model:
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0 10.0 1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "0.0 1.0 0.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "nstar", new IntegerParameter(new Integer[] {4}));

		// Set up likelihood instance:
		StdBirthDeathDensity likelihood = new StdBirthDeathDensity();
		likelihood.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mtTree);

		double expected = -12.81846 + Math.log(6) + 4*Math.log(2); // Calculated by R + unseen states correction + correction for oriented trees
		double result = likelihood.calculateLogP();

		Assert.assertEquals(expected, result, 1e-5);
	}

	/**
	 * Test two states exp likelihood, happy case.
	 */
	@Test
	public void testTwoStatesExpHappyCase() {
		// Assemble test MultiRateTree
		String newickStr = "((4[&State=0]:0.150558013,5[&State=0]:0.150558013)7[&State=0]:1.154521107,(((2[&State=1]:0.2610234442,3[&State=1]:0.2610234442)9[&State=1]:0.02397655557)"
				+ "10[&State=0]:0.006852994,1[&State=0]:0.2918529938)8[&State=0]:1.013226126)6[&State=0];";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", true);

		// Assemble rate shift model:
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0 10.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "0.0 1.0");
		ExtendableRealParameter lambdars = new ExtendableRealParameter();
		lambdars.initByName("value", "0.1 0.5");
		ExtendableRealParameter murs = new ExtendableRealParameter();
		murs.initByName("value", "0.0 0.0");
		ExpStateChangeModel expModel = new ExpStateChangeModel();
		expModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "lambdaRates", lambdars, "muRates", murs);

		// Set up likelihood instance:
		ExpBirthDeathDensity explik = new ExpBirthDeathDensity();
		explik.initByName("stateChangeModel", expModel, "multiRateTree", mtTree, "stepsize", 0.1);

		double expected = -7.81227 + 4*Math.log(2); // Calculated by R + correction for oriented trees
		double expResult = explik.calculateLogP();
		Assert.assertEquals(expected, expResult, 1e-4);
	}
}
