/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package distributions;

import org.junit.Assert;
import org.junit.Test;

import beast.base.inference.parameter.RealParameter;
import msbd.base.evolution.tree.MultiRateTreeFromNewick;
import msbd.base.inference.parameter.ExtendableRealParameter;
import msbd.distributions.ExpBirthDeathDensity;
import msbd.distributions.StdBirthDeathDensity;
import msbd.evolution.tree.ExpStateChangeModel;
import msbd.evolution.tree.StateChangeModel;

/**
 * Tests for the origin feature.
 */
public class OriginTest {

	/**
	 * Test likelihood with invalid origin.
	 */
	@Test
	public void testInvalidOrigin() {
		String newickStr = "((4:0.150558013,5:0.150558013)7:1.154521107,((2:0.2610234442,3:0.2610234442)9:0.03082954957,1:0.2918529938)8:1.013226126)6;";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", true);

		// Assemble rate shift model:
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus);

		RealParameter origin = new RealParameter();
		origin.initByName("value", "0.5");
		// Set up likelihood instance:
		StdBirthDeathDensity likelihood = new StdBirthDeathDensity();
		likelihood.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mtTree, "origin", origin);
		Assert.assertEquals(Double.NEGATIVE_INFINITY, likelihood.calculateLogP(), 1e-10);
	}

	/**
	 * Test std likelihood with origin.
	 */
	@Test
	public void testStdLikelihood() {
		String newickStr = "((4:0.150558013,5:0.150558013)7:1.154521107,((2:0.2610234442,3:0.2610234442)9:0.03082954957,1:0.2918529938)8:1.013226126)6;";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", true);

		// Assemble rate shift model:
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus);

		RealParameter origin = new RealParameter();
		origin.initByName("value", "2.0");
		// Set up likelihood instance:
		StdBirthDeathDensity likelihood = new StdBirthDeathDensity();
		likelihood.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mtTree, "origin", origin);

		double expected = -7.625499 + (mtTree.getLeafNodeCount() - mtTree.getDirectAncestorNodeCount() - 1)*Math.log(2);
		Assert.assertEquals(expected, likelihood.calculateLogP(), 1e-5);
	}

	/**
	 * Test exp likelihood with origin.
	 */
	@Test
	public void testExpLikelihood() {
		String newickStr = "((4:0.150558013,5:0.150558013)7:1.154521107,((2:0.2610234442,3:0.2610234442)9:0.03082954957,1:0.2918529938)8:1.013226126)6;";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", true);

		// Assemble rate shift model:
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		ExtendableRealParameter lambdars = new ExtendableRealParameter();
		lambdars.initByName("value", "0.5");
		ExtendableRealParameter murs = new ExtendableRealParameter();
		murs.initByName("value", "0.0");
		ExpStateChangeModel expModel = new ExpStateChangeModel();
		expModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "lambdaRates", lambdars, "muRates", murs);

		RealParameter origin = new RealParameter();
		origin.initByName("value", "2.0");
		// Set up likelihood instance:
		ExpBirthDeathDensity explik = new ExpBirthDeathDensity();
		explik.initByName("stateChangeModel", expModel, "multiRateTree", mtTree, "stepsize", 0.1, "origin", origin);

		double expected = -9.582703 + (mtTree.getLeafNodeCount() - mtTree.getDirectAncestorNodeCount() - 1)*Math.log(2);
		Assert.assertEquals(expected, explik.calculateLogP(), 1e-4);
	}
}
