package distributions;

import org.junit.Assert;
import org.junit.Test;

import beast.base.inference.parameter.RealParameter;
import msbd.base.evolution.tree.MultiRateTreeFromNewick;
import msbd.base.inference.parameter.ExtendableRealParameter;
import msbd.distributions.StdBirthDeathDensity;
import msbd.evolution.tree.StateChangeModel;

/**
 * Test class for matching the likelihood of MSBD with SAs to the original SA likelihood.
 */
public class SADensityTest {
	
	/**
	 * Test an invalid model with no psi.
	 */
	@Test(expected = RuntimeException.class)
	public void testInvalidModelNoPsi() {
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.0");
		StateChangeModel stateChangeModel = new StateChangeModel();
		
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "isSA", true);
	}
	
	/**
	 * Test a simple likelihood calculation (no SA).
	 */
	@Test
	public void testLikelihoodCalculationSimple() {

		/**Tree tree;
		try {
			tree = new ZeroBranchSATreeParser("((3 : 1.5, 4 : 0.5) : 1 , (1 : 2, 2 : 1) : 3);", false, true, 0);
		} catch (Exception e) {
			return;
		}

		SABirthDeathModel sabdm = new SABirthDeathModel();
		sabdm.setInputValue("tree", tree);
		sabdm.setInputValue("origin", new RealParameter("10."));
		sabdm.setInputValue("removalProbability", "0");
		sabdm.setInputValue("rho", "1.0");

		sabdm.setInputValue("birthRate", new RealParameter("2.25"));
		sabdm.setInputValue("deathRate", new RealParameter("1.05"));
		sabdm.setInputValue("samplingRate", new RealParameter("0.45"));

		sabdm.initAndValidate();
		double saLogP = sabdm.calculateLogP(); **/
		double saLogP = -31.83810965033502; // calculated using SA in the above code
				
		String newickStr = "((3 : 1.5, 4 : 0.5) : 1 , (1 : 2, 2 : 1) : 3);";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", false);

		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.0");
		ExtendableRealParameter psi = new ExtendableRealParameter();
		psi.initByName("value", "0.45");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "2.25");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.05");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "psis", psi, "isSA", true);
		
		RealParameter origin = new RealParameter();
		origin.initByName("value", "10.0");
		StdBirthDeathDensity likelihood = new StdBirthDeathDensity();
		likelihood.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mtTree, "checkValidity", true, "origin", origin);
		
		Assert.assertEquals(saLogP, likelihood.calculateLogP(), 1e-6);
	}
	
	/**
	 * Test a likelihood calculation with SAs.
	 */
	@Test
	public void testLikelihoodCalculationSA() {

		/** Tree tree;
		try {
			tree = new ZeroBranchSATreeParser("((3 : 1.5, 4 : 0) : 1 , (1 : 2, 2 : 1) : 3);", false, true, 0);
		} catch (Exception e) {
			return;
		}

		SABirthDeathModel sabdm = new SABirthDeathModel();
		sabdm.setInputValue("tree", tree);
		sabdm.setInputValue("origin", new RealParameter("10."));
		sabdm.setInputValue("removalProbability", "0");
		sabdm.setInputValue("rho", "1.0");

		sabdm.setInputValue("birthRate", new RealParameter("2.25"));
		sabdm.setInputValue("deathRate", new RealParameter("1.05"));
		sabdm.setInputValue("samplingRate", new RealParameter("0.45"));

		sabdm.initAndValidate();
		Assert.assertEquals(1, tree.getDirectAncestorNodeCount());
		
		double saLogP = sabdm.calculateLogP(); **/
		double saLogP = -31.235139142137662; // calculated using SA in the above code
				
		String newickStr = "((3 : 1.5, 4 : 0) : 1 , (1 : 2, 2 : 1) : 3);";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", false);
		Assert.assertEquals(1, mtTree.getDirectAncestorNodeCount());

		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.0");
		ExtendableRealParameter psi = new ExtendableRealParameter();
		psi.initByName("value", "0.45");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "2.25");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "1.05");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "psis", psi, "isSA", true);
		
		RealParameter origin = new RealParameter();
		origin.initByName("value", "10.0");
		StdBirthDeathDensity likelihood = new StdBirthDeathDensity();
		likelihood.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mtTree, "checkValidity", true, "origin", origin);
		
		Assert.assertEquals(saLogP, likelihood.calculateLogP(), 1e-6);
	}
}
