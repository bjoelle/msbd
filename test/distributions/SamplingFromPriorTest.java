/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package distributions;

import java.io.File;

import org.junit.Assert;

import test.beast.beast2vs1.trace.LogFileTraces;
import test.beast.beast2vs1.trace.TraceStatistics;
import beagle.BeagleFlag;
import beast.base.inference.Logger;
import beast.base.util.Randomizer;
import beast.base.parser.XMLParser;

/**
 * Test distributions obtained from Sampling From Prior and compare to expected.
 */
public class SamplingFromPriorTest {

	protected String XML_FILE;
	protected double[] tolerances = new double[6];
	int SEED = 1265;
	String DIR = System.getProperty("user.dir") + "/test/";

	/**
	 * Analyze log file traces and compare results to expected results using the set tolerances.
	 *
	 * @param traces the log file traces
	 */
	protected void analyzeTraces(LogFileTraces traces) {
		// all expected values are from R forward simulation (10k samples)

		int i = traces.getTraceIndex("CollessIndex");
		TraceStatistics stats = traces.analyseTrace(i);
		double expectedColMed = 154;
		Assert.assertEquals(expectedColMed, stats.getMedian(), tolerances[0]);
		double expectedColUpCpd = 272;
		Assert.assertEquals(expectedColUpCpd, stats.getCpdUpper(), tolerances[1]);
		double expectedColDownCpd = 89;
		Assert.assertEquals(expectedColDownCpd, stats.getCpdLower(), tolerances[2]);

		i = traces.getTraceIndex("GammaStat");
		stats = traces.analyseTrace(i);
		double expectedGamMed = 0.4255241;
		Assert.assertEquals(expectedGamMed, stats.getMedian(), tolerances[3]);
		double expectedGamUpCpd = 3.7184690;
		Assert.assertEquals(expectedGamUpCpd, stats.getCpdUpper(), tolerances[4]);
		double expectedGamDownCpd = -1.8354140;
		Assert.assertEquals(expectedGamDownCpd, stats.getCpdLower(), tolerances[5]);
	}

	/**
	 * Runs beast.
	 *
	 * @return the log file traces
	 * @throws Exception if anything fails
	 */
	protected LogFileTraces runBeast() throws Exception {
		Randomizer.setSeed(SEED);
		Logger.FILE_MODE = Logger.LogFileMode.overwrite;

		long beagleFlags = BeagleFlag.PROCESSOR_CPU.getMask() | BeagleFlag.VECTOR_SSE.getMask();
		System.setProperty("beagle.preferred.flags", Long.toString(beagleFlags));

		String fileName = DIR + XML_FILE;

		XMLParser parser = new XMLParser();
		beast.base.inference.Runnable runable = parser.parseFile(new File(fileName));
		runable.setStateFile(DIR + "tmp.state", false);
		runable.run();

		String logFile = DIR + "sampleFromPrior.log";
		File file = new File(logFile);
		LogFileTraces traces = new LogFileTraces(logFile, file);
		traces.loadTraces();
		long burnin = traces.getMaxState() / 10;
		traces.setBurnIn(burnin);

		return traces;
	}

}
