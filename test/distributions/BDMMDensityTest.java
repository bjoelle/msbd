package distributions;

import org.junit.Assert;
import org.junit.Test;

import beast.base.inference.parameter.IntegerParameter;
import beast.base.inference.parameter.RealParameter;
import msbd.base.evolution.tree.MultiRateTreeFromNewick;
import msbd.base.inference.parameter.ExtendableRealParameter;
import msbd.distributions.StdBirthDeathDensity;
import msbd.evolution.tree.StateChangeModel;

public class BDMMDensityTest {
	/**
	 * Test two states std likelihood with SA, identical states.
	 */
	@Test
	public void testTwoIdenticalStatesStdSA() {
		// Assemble test MultiRateTree
		String newickStr = "((4[&State=0]:0.150558013,5[&State=0]:0.150558013)7[&State=0]:1.154521107,(((2[&State=1]:0.2610234442,3[&State=1]:0.2610234442)9[&State=1]:0.02397655557)"
				+ "10[&State=0]:0.006852994,1[&State=0]:0)8[&State=0]:1.013226126)6[&State=0];";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", false);

		// Assemble rate shift model:
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		RealParameter rho = new RealParameter();
		rho.initByName("value", "1.0");
		IntegerParameter nstar = new IntegerParameter();
		nstar.initByName("value", "2");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0 1.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "0.0 0.0"); // compared with BDMM so needs mu=0 for approx
		ExtendableRealParameter psi = new ExtendableRealParameter();
		psi.initByName("value", "0.45 0.45");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "psis", psi, "isSA", true, 
				"rho", rho, "nstar", nstar);

		// Set up likelihood instance:
		StdBirthDeathDensity likelihood = new StdBirthDeathDensity();
		likelihood.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mtTree);

		double expected = -5.997752874116868 - Math.log(0.5); // Calculated by BDMM + correction for frequency of root state
		double result = likelihood.calculateLogP();

		Assert.assertEquals(expected, result, 1e-5);
	}

	/**
	 * Test two states std likelihood with no SA, different states.
	 */
	@Test
	public void testTwoStatesStdNoSA() {
		// Assemble test MultiRateTree
		String newickStr = "((4[&State=0]:0.15,5[&State=0]:0.15)7[&State=0]:1.15,(((2[&State=1]:0.26,3[&State=1]:0.26)9[&State=1]:0.02)"
				+ "10[&State=0]:0.02,1[&State=0]:0.3)8[&State=0]:1.0)6[&State=0];";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", false);

		// Assemble rate shift model:
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		RealParameter rho = new RealParameter();
		rho.initByName("value", "1.0");
		IntegerParameter nstar = new IntegerParameter();
		nstar.initByName("value", "2");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0 10.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "0.0 0.0"); // compared with BDMM so needs mu=0 for approx
		ExtendableRealParameter psi = new ExtendableRealParameter();
		psi.initByName("value", "0.45 0.65");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "psis", psi, "isSA", true, 
				"rho", rho, "nstar", nstar);

		// Set up likelihood instance:
		StdBirthDeathDensity likelihood = new StdBirthDeathDensity();
		likelihood.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mtTree);

		double expected = -7.733620339392267 - Math.log(0.5); // Calculated by BDMM + correction for frequency of root state
		double result = likelihood.calculateLogP();

		Assert.assertEquals(expected, result, 1e-5);
	}

	/**
	 * Test two states std likelihood with SA, different states.
	 */
	@Test
	public void testTwoStatesStdSA() {
		// Assemble test MultiRateTree
		String newickStr = "((4[&State=0]:0.0,5[&State=0]:0.15)7[&State=0]:1.15,(((2[&State=1]:0.26,3[&State=1]:0.26)9[&State=1]:0.02)"
				+ "10[&State=0]:0.02,1[&State=0]:0.3)8[&State=0]:1.0)6[&State=0];";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", false);

		// Assemble rate shift model:
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		RealParameter rho = new RealParameter();
		rho.initByName("value", "1.0");
		IntegerParameter nstar = new IntegerParameter();
		nstar.initByName("value", "2");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0 10.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "0.0 0.0"); // compared with BDMM so needs mu=0 for approx
		ExtendableRealParameter psi = new ExtendableRealParameter();
		psi.initByName("value", "0.45 0.65");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "psis", psi, "isSA", true, 
				"rho", rho, "nstar", nstar);

		// Set up likelihood instance:
		StdBirthDeathDensity likelihood = new StdBirthDeathDensity();
		likelihood.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mtTree);

		double expected = -8.932775208071082 - Math.log(0.5); // Calculated by BDMM + correction for frequency of root state
		double result = likelihood.calculateLogP();

		Assert.assertEquals(expected, result, 1e-5);
	}


	/**
	 * Test two states std likelihood without the problem SA, different states.
	 */
	@Test
	public void testTwoStatesStdSA2() {
		// Assemble test MultiRateTree
		String newickStr = "((4[&State=0]:0.0,1[&State=0]:0.15)7[&State=0]:1.15,((2[&State=1]:0.26,3[&State=1]:0.26)6[&State=1]:0.02)"
				+ "8[&State=0]:1.02)5[&State=0];";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", false);

		// Assemble rate shift model:
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		RealParameter rho = new RealParameter();
		rho.initByName("value", "1.0");
		IntegerParameter nstar = new IntegerParameter();
		nstar.initByName("value", "2");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0 10.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "0.0 0.0"); // compared with BDMM so needs mu=0 for approx
		ExtendableRealParameter psi = new ExtendableRealParameter();
		psi.initByName("value", "0.45 0.65");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "psis", psi, "isSA", true, 
				"rho", rho, "nstar", nstar);

		// Set up likelihood instance:
		StdBirthDeathDensity likelihood = new StdBirthDeathDensity();
		likelihood.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mtTree);

		double expected = -9.04092238849239 - Math.log(0.5); // Calculated by BDMM + correction for frequency of root state
		double result = likelihood.calculateLogP();

		Assert.assertEquals(expected, result, 1e-5);
	}

	/**
	 * Test two states std likelihood with added problem SA, different states.
	 */
	@Test(expected = AssertionError.class)
	public void testTwoStatesStdSA3() {
		// Assemble test MultiRateTree
		String newickStr = "((4[&State=0]:0.0,5[&State=0]:0.15)7[&State=0]:1.15,(((2[&State=1]:0.26,3[&State=1]:0.26)9[&State=1]:0.02)"
				+ "10[&State=0]:0.02,1[&State=0]:0.0)8[&State=0]:1.0)6[&State=0];";

		MultiRateTreeFromNewick mtTree = new MultiRateTreeFromNewick();
		mtTree.initByName("newick", newickStr, "adjustTipHeights", false);

		// Assemble rate shift model:
		RealParameter gamma = new RealParameter();
		gamma.initByName("value", "0.5");
		RealParameter rho = new RealParameter();
		rho.initByName("value", "1.0");
		IntegerParameter nstar = new IntegerParameter();
		nstar.initByName("value", "2");
		ExtendableRealParameter lambdas = new ExtendableRealParameter();
		lambdas.initByName("value", "1.0 10.0");
		ExtendableRealParameter mus = new ExtendableRealParameter();
		mus.initByName("value", "0.0 0.0"); // compared with BDMM so needs mu=0 for approx
		ExtendableRealParameter psi = new ExtendableRealParameter();
		psi.initByName("value", "0.45 0.65");
		StateChangeModel stateChangeModel = new StateChangeModel();
		stateChangeModel.initByName("gamma", gamma, "lambdas", lambdas, "mus", mus, "psis", psi, "isSA", true, 
				"rho", rho, "nstar", nstar);

		// Set up likelihood instance:
		StdBirthDeathDensity likelihood = new StdBirthDeathDensity();
		likelihood.initByName("stateChangeModel", stateChangeModel, "multiRateTree", mtTree);

		double expected = -9.655430083534036 - Math.log(0.5); // Calculated by BDMM + correction for frequency of root state
		double result = likelihood.calculateLogP();

		Assert.assertEquals(expected, result, 1e-5); // currently bug in BDMM for that calculation
	}

}
