MSBD
=============

This is a BEAST 2 package which allows for the inference of a multi-state birth-death prior on phylogenetic trees, with state-specific birth and death rates.  
The multi-states birth-death model allows the inference of the number of states, the position of states on the phylogeny, the state-specific birth and death rates and the overall rate of state change from serially-sampled sequence data. 


NEWS
=============
 * v1.2.0 Added generation of random multi-rate trees from birth rate, accounting for MRCA priors.
 * v1.3.0 Added handling of SA trees with state-dependent sampling rate.
 * v2.0.0 Ported package to BEAST 2.7.
 * v2.0.1 Fixed several BEAUti issues related to SAs and morphological data.
 * v2.0.2 Fixed additional BEAUti issues realted to the initial tree, added dependency on MM package.
 * v2.0.3 Changed the default operator setup to improve performance.


License
-------

This software is free (as in freedom).  With the exception of the
libraries on which it depends, it is made available under the terms of
the GNU General Public Licence version 3, which is contained in this
directory in the file named COPYING.

The following libraries are bundled with MSBD:

* Google Guava (http://code.google.com/p/guava-libraries/)
* jblas (http://mikiobraun.github.io/jblas/)

That software is distributed under the licences provided in the
LICENCE.* files included in this archive.
